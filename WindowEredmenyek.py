from PIL.Image import Image
from WidgetEloszlasAbra import WidgetAbra
import pandas as pd
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtGui import QFont
from datetime import datetime
from pathlib import Path

class Ui_WindowEredmenyek(object):

    df_export = pd.DataFrame()
    text = ""

    def eredmenyekMentese(self):
        textEdit_szoveg = self.plainTextEditDialogus.toPlainText()
        try:
            most = datetime.now()
            ido_most = most.strftime("%H_%M_%S")
            if self.lineEditFajlNev.text() == "":
                filepath_csv = Path('eredmenyek/klaszteranalizis_' + ido_most + '.csv')
                filepath_txt = Path('eredmenyek/klaszteranalizis_' + ido_most + '.txt')
                filepath_png = Path('eredmenyek/klaszteranalizis_' + ido_most + '.png')
            else:
                szoveg = self.lineEditFajlNev.text()
                filepath_csv = Path('eredmenyek/'+ szoveg + "_" + ido_most + '.csv')
                filepath_txt = Path('eredmenyek/'+ szoveg + "_" + ido_most + '.txt')
                filepath_png = Path('eredmenyek/'+ szoveg + "_" + ido_most + '.png')

            Ui_WindowEredmenyek.df_export.to_csv(filepath_csv, index=None)

            with open(filepath_txt, 'w') as file:
                file.write(textEdit_szoveg)

            self.widgetAbra.canvas.axes.figure.savefig(filepath_png)

            uzenet = "Az eredmények sikeresen exportálva lettek az 'Eredmények' ablakba."
            QtWidgets.QMessageBox.information(self.window, "Tábla exportálása", uzenet)
        except Exception as e:
            QtWidgets.QMessageBox.warning(self.window, "Exportálás", "Hiba az eredménytábla exportálásakor! Lehetséges, hogy helytelen útvonalat adott meg. Próbálja újra!")
            print(e)

    def setupUi(self, WindowEredmenyek):
        WindowEredmenyek.setObjectName("WindowEredmenyek")
        WindowEredmenyek.resize(1072, 884)
        self.window = WindowEredmenyek
        self.centralwidget = QtWidgets.QWidget(self.window)
        self.centralwidget.setObjectName("centralwidget")
        self.tableViewKlaszteradatok = QtWidgets.QTableView(self.centralwidget)
        self.tableViewKlaszteradatok.setGeometry(QtCore.QRect(520, 10, 541, 401))
        self.tableViewKlaszteradatok.setObjectName("tableViewKlaszteradatok")
        self.plainTextEditDialogus = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEditDialogus.setGeometry(QtCore.QRect(10, 420, 1051, 401))
        self.plainTextEditDialogus.setReadOnly(True)
        self.plainTextEditDialogus.setWordWrapMode(0)
        font = QFont("Monospace")
        font.setStyleHint(QFont.TypeWriter)
        self.plainTextEditDialogus.setFont(font)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.plainTextEditDialogus.sizePolicy().hasHeightForWidth())
        self.plainTextEditDialogus.setSizePolicy(sizePolicy)
        self.plainTextEditDialogus.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.plainTextEditDialogus.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.plainTextEditDialogus.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByKeyboard|QtCore.Qt.LinksAccessibleByMouse|QtCore.Qt.TextBrowserInteraction|QtCore.Qt.TextSelectableByKeyboard|QtCore.Qt.TextSelectableByMouse)
        self.plainTextEditDialogus.setObjectName("plainTextEditDialogus")
        self.pushButtonOK = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonOK.setGeometry(QtCore.QRect(220, 830, 371, 31))
        self.pushButtonOK.setObjectName("pushButtonOK")
        self.pushButtonOK.clicked.connect(lambda: self.window.close())
        self.pushButtonExportalas = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonExportalas.setGeometry(QtCore.QRect(610, 830, 241, 31))
        self.pushButtonExportalas.setObjectName("pushButtonExportalas")
        self.pushButtonExportalas.clicked.connect(lambda: self.eredmenyekMentese())
        self.widgetAbra = WidgetAbra(self.centralwidget)
        self.widgetAbra.setGeometry(QtCore.QRect(10, 10, 501, 401))
        self.widgetAbra.setObjectName("MplWidget")
        self.lineEditFajlNev = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditFajlNev.setGeometry(QtCore.QRect(860, 830, 201, 31))
        self.lineEditFajlNev.setText("")
        self.lineEditFajlNev.setObjectName("lineEdit")
        self.window.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(self.window)
        self.statusbar.setObjectName("statusbar")
        self.window.setStatusBar(self.statusbar)
        self.retranslateUi(self.window)
        QtCore.QMetaObject.connectSlotsByName(self.window)

    def retranslateUi(self, WindowEredmenyek):
        _translate = QtCore.QCoreApplication.translate
        WindowEredmenyek.setWindowTitle(_translate("WindowEredmenyek", "Eredmények"))
        self.pushButtonOK.setText(_translate("WindowEredmenyek", "Befejezés"))
        self.pushButtonExportalas.setText(_translate("WindowEredmenyek", "Exportálás"))
        self.lineEditFajlNev.setPlaceholderText(_translate("WindowEredmenyek", "Adja meg az eredményfájl nevét!"))



