import os
import numpy as np
import pandas as pd
import squarify
from PIL import Image
import matplotlib.pyplot as plt
import plotly.graph_objects as go
import plotly.figure_factory as ff
from PyQt5.QtWidgets import QWidget, QMessageBox
from PyQt5 import QtCore, QtGui, QtWidgets
import openpyxl
import kaleido


class Ui_Grafikus(QWidget):
    global df
    global basepath

    def setupUi(self, Ui_Grafikus):
        Ui_Grafikus.setObjectName("Ui_Grafikus")
        Ui_Grafikus.resize(600, 600)

        font = QtGui.QFont()
        font.setPointSize(11)

        self.centralwidget = QtWidgets.QWidget(Ui_Grafikus)
        Ui_Grafikus.setCentralWidget(self.centralwidget)

        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(20, 20, 560, 560))
        self.groupBox.setFont(font)

        # sourcedata
        self.label_sourcedata = QtWidgets.QLabel("Bemeneti adatállomány:", self.groupBox)
        self.label_sourcedata.setGeometry(QtCore.QRect(10, 40, 200, 21))
        self.label_sourcedata.setAlignment(QtCore.Qt.AlignRight
                                           | QtCore.Qt.AlignTrailing
                                           | QtCore.Qt.AlignVCenter)
        self.comboBox_sourcedata = QtWidgets.QComboBox(self.groupBox)
        self.comboBox_sourcedata.setGeometry(QtCore.QRect(250, 40, 250, 25))
        sourcedata_defaultstring = "Válassz egy adatállományt"
        self.comboBox_sourcedata.addItem(sourcedata_defaultstring)
        all_folder_file = os.listdir('./SourceData')
        for element in all_folder_file:
            self.comboBox_sourcedata.addItem(element)

        # visualisation options
        self.label_visualopt = QtWidgets.QLabel("Vizualizálás módja:", self.groupBox)
        self.label_visualopt.setGeometry(QtCore.QRect(10, 70, 200, 23))
        self.label_visualopt.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.comboBox_visualopt = QtWidgets.QComboBox(self.groupBox)
        self.comboBox_visualopt.setGeometry(QtCore.QRect(250, 70, 250, 25))
        possible_vis = ['pontdiagram', 'oszlopdiagram', 'fatérképdiagram', 'hisztogram', 'kördiagram']
        for element in possible_vis:
            self.comboBox_visualopt.addItem(element)

        # visualisation style
        self.label_visualstyle = QtWidgets.QLabel("Vizualizálás stílusa:", self.groupBox)
        self.label_visualstyle.setGeometry(QtCore.QRect(10, 100, 200, 23))
        self.label_visualstyle.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.comboBox_visualstyle = QtWidgets.QComboBox(self.groupBox)
        self.comboBox_visualstyle.setGeometry(QtCore.QRect(250, 100, 250, 25))
        style = ['alapértelmezett', 'szürkeárnyalatos']
        for element in style:
            self.comboBox_visualstyle.addItem(element)

        # opportunities button
        self.button_opportunities = QtWidgets.QPushButton("Lehetőségek megtekintése", self.centralwidget)
        self.button_opportunities.setGeometry(QtCore.QRect(320, 160, 200, 31))
        self.button_opportunities.setFont(font)
        self.button_opportunities.setDefault(True)
        self.button_opportunities.clicked.connect(lambda: self.second_menu(self.comboBox_sourcedata.currentText(),
                                                                           self.comboBox_visualopt.currentText()))

        # parameters title
        self.label_parameters_title = QtWidgets.QLabel("A diagram paraméterei", self.groupBox)
        self.label_parameters_title.setGeometry(QtCore.QRect(0, 180, 350, 46))
        self.label_parameters_title.setAlignment(
            QtCore.Qt.AlignVCenter | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_parameters_title.hide()

        # diagram title
        self.label_diagramtitle = QtWidgets.QLabel("A diagram címe:", self.groupBox)
        self.label_diagramtitle.setGeometry(QtCore.QRect(10, 230, 200, 25))
        self.label_diagramtitle.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_diagramtitle.hide()
        self.textfield_diagramtitle = QtWidgets.QLineEdit(self.groupBox)
        self.textfield_diagramtitle.setGeometry(QtCore.QRect(250, 230, 250, 25))
        self.textfield_diagramtitle.hide()

        # first dimension
        self.label_first_dimension = QtWidgets.QLabel("X tengely:", self.groupBox)
        self.label_first_dimension.setGeometry(QtCore.QRect(10, 280, 200, 25))
        self.label_first_dimension.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_first_dimension.hide()
        self.comboBox_first_dimension = QtWidgets.QComboBox(self.groupBox)
        self.comboBox_first_dimension.setGeometry(QtCore.QRect(250, 280, 250, 25))
        self.comboBox_first_dimension.hide()
        self.label_first_dimension_title = QtWidgets.QLabel("X tengely címe:", self.groupBox)
        self.label_first_dimension_title.setGeometry(QtCore.QRect(10, 310, 200, 25))
        self.label_first_dimension_title.setAlignment(
            QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_first_dimension_title.hide()
        self.textfield_first_dimension_title = QtWidgets.QLineEdit(self.groupBox)
        self.textfield_first_dimension_title.setGeometry(QtCore.QRect(250, 310, 250, 25))
        self.textfield_first_dimension_title.hide()

        # second dimension
        self.label_second_dimension = QtWidgets.QLabel("Y tengely:", self.groupBox)
        self.label_second_dimension.setGeometry(QtCore.QRect(10, 350, 200, 25))
        self.label_second_dimension.setAlignment(
            QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_second_dimension.hide()
        self.comboBox_second_dimension = QtWidgets.QComboBox(self.groupBox)
        self.comboBox_second_dimension.setGeometry(QtCore.QRect(250, 350, 250, 25))
        self.comboBox_second_dimension.hide()
        self.label_second_dimension_title = QtWidgets.QLabel("Y tengely címe:", self.groupBox)
        self.label_second_dimension_title.setGeometry(QtCore.QRect(10, 380, 200, 25))
        self.label_second_dimension_title.setAlignment(
            QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_second_dimension_title.hide()
        self.textfield_second_dimension_title = QtWidgets.QLineEdit(self.groupBox)
        self.textfield_second_dimension_title.setGeometry(QtCore.QRect(250, 380, 250, 25))
        self.textfield_second_dimension_title.hide()

        # third dimension
        self.label_third_dimension = QtWidgets.QLabel("Z tengely:", self.groupBox)
        self.label_third_dimension.setGeometry(QtCore.QRect(10, 420, 200, 25))
        self.label_third_dimension.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_third_dimension.hide()
        self.comboBox_third_dimension = QtWidgets.QComboBox(self.groupBox)
        self.comboBox_third_dimension.setGeometry(QtCore.QRect(250, 420, 250, 25))
        self.comboBox_third_dimension.hide()
        self.label_third_dimension_title = QtWidgets.QLabel("Z tengely címe:", self.groupBox)
        self.label_third_dimension_title.setGeometry(QtCore.QRect(10, 450, 200, 25))
        self.label_third_dimension_title.setAlignment(
            QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_third_dimension_title.hide()
        self.textfield_third_dimension_title = QtWidgets.QLineEdit(self.groupBox)
        self.textfield_third_dimension_title.setGeometry(QtCore.QRect(250, 450, 250, 25))
        self.textfield_third_dimension_title.hide()

        self.pushButton_megse = QtWidgets.QPushButton("Mégse", self.centralwidget)
        self.pushButton_megse.setGeometry(QtCore.QRect(460, 530, 91, 31))
        self.pushButton_megse.setFont(font)
        self.pushButton_megse.setDefault(True)
        self.pushButton_megse.clicked.connect(lambda: Ui_Grafikus.close())

        self.pushButton_futtat = QtWidgets.QPushButton("Futtatás", self.centralwidget)
        self.pushButton_futtat.setGeometry(QtCore.QRect(360, 530, 91, 31))
        self.pushButton_futtat.setFont(font)
        self.pushButton_futtat.setDefault(True)
        self.pushButton_futtat.clicked.connect(lambda: self.create_visual(self.comboBox_sourcedata.currentText(),
                                                                          self.comboBox_visualopt.currentText(),
                                                                          self.comboBox_visualstyle.currentText(),
                                                                          self.textfield_diagramtitle.text(),
                                                                          self.comboBox_first_dimension.currentText(),
                                                                          self.comboBox_second_dimension.currentText(),
                                                                          self.comboBox_third_dimension.currentText(),
                                                                          self.textfield_first_dimension_title.text(),
                                                                          self.textfield_second_dimension_title.text(),
                                                                          self.textfield_third_dimension_title.text()))

        self.pushButton_futtat.hide()

        self.retranslateUi(Ui_Grafikus)
        QtCore.QMetaObject.connectSlotsByName(Ui_Grafikus)

    def retranslateUi(self, Ui_Grafikus):
        _translate = QtCore.QCoreApplication.translate

        Ui_Grafikus.setWindowTitle(_translate("Ui_Grafikus", "Grafikus megjelenítés elemi statisztikákkal"))
        self.groupBox.setTitle(_translate("Ui_Grafikus", "Vizualizáció módja"))

    def second_menu(self, sourcedatastring, possibility):
        if sourcedatastring == 'Válassz egy adatállományt':
            self.label_parameters_title.hide()

            self.label_diagramtitle.hide()
            self.textfield_diagramtitle.hide()

            self.label_first_dimension.hide()
            self.comboBox_first_dimension.hide()
            self.label_first_dimension_title.hide()
            self.textfield_first_dimension_title.hide()

            self.label_second_dimension.hide()
            self.comboBox_second_dimension.hide()
            self.label_second_dimension_title.hide()
            self.textfield_second_dimension_title.hide()

            self.label_third_dimension.hide()
            self.comboBox_third_dimension.hide()
            self.label_third_dimension_title.hide()
            self.textfield_third_dimension_title.hide()

            show_popup(title='Hiba történt!', description='Válassz egy bemeneti adatállományt!')

            return

        extension = sourcedatastring.split('.')[-1]

        if extension == 'xlsx':
            df_columns = pd.read_excel(f'SourceData/{sourcedatastring}', nrows=0)
        elif extension == 'csv':
            df_columns = pd.read_csv(f'SourceData/{sourcedatastring}', nrows=0)
        else:
            show_popup(title='Hiba történt!', description='Csak excel és csv állományt adhat meg!')

            return

        self.comboBox_first_dimension.clear()
        self.comboBox_second_dimension.clear()
        self.comboBox_third_dimension.clear()

        for element in df_columns:
            self.comboBox_first_dimension.addItem(element)
            self.comboBox_second_dimension.addItem(element)
            self.comboBox_third_dimension.addItem(element)

        if possibility in ['hisztogram']:
            self.label_diagramtitle.show()
            self.textfield_diagramtitle.show()

            self.label_parameters_title.show()

            self.label_first_dimension.show()
            self.comboBox_first_dimension.show()
            self.label_first_dimension_title.show()
            self.textfield_first_dimension_title.show()

            self.label_second_dimension.hide()
            self.comboBox_second_dimension.hide()
            self.label_second_dimension_title.hide()
            self.textfield_second_dimension_title.hide()

            self.label_third_dimension.hide()
            self.comboBox_third_dimension.hide()
            self.label_third_dimension_title.hide()
            self.textfield_third_dimension_title.hide()
        elif possibility in ['kördiagram', 'vonaldiagram', 'pontdiagram', 'oszlopdiagram', 'halmozott oszlopdiagram',
                             'csoportosított oszlopdiagram', 'hőtérkép', 'fatérképdiagram']:
            self.label_diagramtitle.show()
            self.textfield_diagramtitle.show()

            self.label_parameters_title.show()

            self.label_first_dimension.show()
            self.comboBox_first_dimension.show()
            self.label_first_dimension_title.show()
            self.textfield_first_dimension_title.show()

            self.label_second_dimension.show()
            self.comboBox_second_dimension.show()
            self.label_second_dimension_title.show()
            self.textfield_second_dimension_title.show()

            self.label_third_dimension.hide()
            self.comboBox_third_dimension.hide()
            self.label_third_dimension_title.hide()
            self.textfield_third_dimension_title.hide()
        elif possibility in ['buborékdiagram']:
            self.label_diagramtitle.show()
            self.textfield_diagramtitle.show()

            self.label_parameters_title.show()

            self.label_first_dimension.show()
            self.comboBox_first_dimension.show()
            self.label_first_dimension_title.show()
            self.textfield_first_dimension_title.show()

            self.label_second_dimension.show()
            self.comboBox_second_dimension.show()
            self.label_second_dimension_title.show()
            self.textfield_second_dimension_title.show()

            self.label_third_dimension.show()
            self.comboBox_third_dimension.show()
            self.label_third_dimension_title.show()
            self.textfield_third_dimension_title.show()

        self.button_opportunities.setText('Lehetőségek frissítése')
        self.pushButton_futtat.show()
        self.comboBox_first_dimension.show()
        self.label_parameters_title.show()

    def create_visual(self, sourcedatastring, visualisation, style_string, title, axis_first, axis_second, axis_third,
                      first_label, second_label, third_label):
        if title == "":
            show_popup(title='Hiba történt!', description='Adj címet a diagramnak!')
            return

        df = self.get_df(sourcedatastring)

        if style_string == 'alapértelmezett':
            style = 'bmh'
        else:
            style = 'grayscale'
        try:
            if visualisation == 'kördiagram':
                self.create_pie_diagram(df, style, axis_first, axis_second, title, first_label, second_label)
            elif visualisation == 'hisztogram':
                self.create_histogram(df, style, axis_first, title, first_label)
            elif visualisation == 'vonaldiagram':
                self.create_line_diagram(df, style, axis_first, axis_second, title, first_label, second_label)
            elif visualisation == 'pontdiagram':
                self.create_scatter_diagram(df, style, axis_first, axis_second, title, first_label, second_label)
            elif visualisation == 'oszlopdiagram':
                self.create_simple_bar_diagram(df, style, axis_first, axis_second, title, first_label, second_label)
            elif visualisation == 'fatérképdiagram':
                self.create_boxplot_diagram(df, style, axis_first, axis_second, title, first_label, second_label)
        except Exception as err:
            show_popup(title='Hiba történt!', description='Hiba történt a diagram előállítása során!')
            return

        im = Image.open(rf"eredmenyek\{title}.png")
        im.show()

    def create_pie_diagram(self, df, style, axis_first, axis_second, title, first_label, second_label):
        plt.close()
        plt.style.use(f'{style}')

        plt.pie(df[axis_second], labels=df[axis_first])

        plt.title(f'{title}', fontsize=24)
        plt.xlabel(f'{first_label}', fontsize=15)
        plt.ylabel(f'{second_label}', fontsize=15)

        plt.tight_layout()

        plt.savefig(f"eredmenyek/{title}.png")

    def create_histogram(self, df, style, axis_first, title, first_label):
        if first_label == "":
            first_label = axis_first

        if style == 'bmh':
            style = 'plotly_white'
        else:
            style = 'plotly_dark'

        plt.close()
        fig = ff.create_distplot([df[axis_first]], [first_label])
        fig2 = ff.create_distplot([df[axis_first]], [first_label], curve_type='normal')
        normal_x = fig2.data[1]['x']
        normal_y = fig2.data[1]['y']
        fig.add_traces(go.Scatter(x=normal_x, y=normal_y, mode='lines', name='Normális eloszlás',
                                  line=dict(color='red', width=1.5)
                                  ))
        fig.update_layout(title_text=f'{title}', template=style)
        fig.write_image(f"eredmenyek/{title}.png")

    def create_line_diagram(self, df, style, axis_first, axis_second, title, first_label, second_label):
        plt.close()
        plt.style.use(f'{style}')
        data = pd.DataFrame({
            'Group': df[axis_first],
            'Value': df[axis_second]
        })
        x_index = np.arange(len(df[axis_first]))
        ordered_data = data.sort_values(by=['Value'])

        plt.plot(ordered_data.Group, ordered_data.Value)
        plt.xticks(ticks=x_index, labels=ordered_data.Group, fontsize=9, rotation=90)

        plt.title(f'{title}', fontsize=24)
        plt.xlabel(f'{first_label}', fontsize=15)
        plt.ylabel(f'{second_label}', fontsize=15)
        plt.tight_layout()

        plt.savefig(f"eredmenyek/{title}.png")

    def create_simple_bar_diagram(self, df, style, axis_first, axis_second, title, first_label, second_label):
        plt.close()
        plt.style.use(f'{style}')
        data = pd.DataFrame({
            'Group': df[axis_first],
            'Value': df[axis_second]
        })
        x_index = np.arange(len(df[axis_first]))
        ordered_data = data.sort_values(by=['Value'])
        plt.bar(ordered_data.Group, ordered_data.Value)
        plt.xticks(ticks=x_index, fontsize=9, rotation=90)
        plt.title(f'{title}', fontsize=24)
        plt.xlabel(f'{first_label}', fontsize=15)
        plt.ylabel(f'{second_label}', fontsize=15)
        plt.tight_layout()
        plt.savefig(f"eredmenyek/{title}.png")

    def create_scatter_diagram(self, df, style, axis_first, axis_second, title, first_label, second_label):
        plt.close()
        plt.style.use(f'{style}')

        data = pd.DataFrame({
            'Group': df[axis_first],
            'Value': df[axis_second]
        })
        ordered_data = data.sort_values(by=['Value'])

        plt.plot('Group', 'Value', data=ordered_data, linestyle='none', marker='o')

        if title == "":
            title = "Pontdiagram"
        if first_label == "":
            first_label = axis_first
        if second_label == "":
            second_label = axis_second

        plt.title(f'{title}', fontsize=24)
        plt.xlabel(f'{first_label}', fontsize=15)
        plt.ylabel(f'{second_label}', fontsize=15)
        plt.xticks(rotation=90)
        plt.tight_layout()

        plt.savefig(f"eredmenyek/{title}.png")

    def create_boxplot_diagram(self, df, style, axis_first, axis_second, title, first_label, second_label):
        plt.close()
        plt.style.use(f'{style}')
        data = pd.DataFrame({
            'Group': df[axis_first],
            'Value': df[axis_second]
        })
        ordered_data = data.sort_values(by=['Value'])

        squarify.plot(label=ordered_data.Group, sizes=ordered_data.Value, alpha=.8)
        plt.axis('off')
        plt.title(f'{title}', fontsize=24)
        plt.tight_layout()

        plt.savefig(f"eredmenyek/{title}.png")

    def get_df(self, sourcedatastring):
        df = None
        formatum = sourcedatastring.split('.')[-1]
        if formatum == 'csv':
            df = pd.read_csv(f"SourceData\{sourcedatastring}")
        elif formatum == 'xlsx':
            df = pd.read_excel(f"SourceData\{sourcedatastring}")
        return df


def show_popup(title, description):
    msg = QMessageBox()
    msg.setWindowTitle(title)
    msg.setText(description)
    msg.setIcon(QMessageBox.Information)
    msg.setStandardButtons(QMessageBox.Ok)
    x = msg.exec_()
