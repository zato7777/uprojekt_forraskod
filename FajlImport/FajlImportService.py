import numpy as np
import pandas as pd
from numpy import nan

from FajlImport.FajlImportRepository import FajlImportRepository


class FajlImportService(object):
    def __init__(self):
        self.fajl_import_repository = FajlImportRepository()

    def createTableFromFajl(self, adatallomany_neve, df):
        column_info = {}
        column_info['id'] = 'INT AUTO_INCREMENT PRIMARY KEY'
        for oszlop_nev, oszlop_adattipus in zip(df.columns, df.dtypes):
            if str(oszlop_adattipus) == 'object':
                mysql_adattipus = 'TEXT'
            elif 'int' in str(oszlop_adattipus):
                mysql_adattipus = 'INT'
            elif 'float' in str(oszlop_adattipus):
                mysql_adattipus = 'FLOAT'
            else:
                mysql_adattipus = 'TEXT'

            #kéne vizsgálni mi van akkor, ha id az oszlop neve

            column_info[oszlop_nev] = mysql_adattipus

        return self.fajl_import_repository.createTableFromFajl(adatallomany_neve, column_info)

    def fillUpTable(self, adatallomany_neve, df):
        replaced_df = df.replace({np.nan: None})
        df_values = replaced_df.values.tolist()
        return self.fajl_import_repository.fillUpTable(adatallomany_neve, df.columns, df_values)

    def get_fajl_rekordszam(self, adatallomany_neve):
        rekordszam, message = self.fajl_import_repository.get_fajl_rekordszam(adatallomany_neve)
        return rekordszam, message

    def get_fajl_mezoszam(self, adatallomany_neve):
        mezoszam, message = self.fajl_import_repository.get_fajl_mezoszam(adatallomany_neve)
        return mezoszam, message

    def rekord_list(self, adatallomany_neve):
        rekord_list, message = self.fajl_import_repository.rekord_list(adatallomany_neve)
        rekord_list_str = []

        for rekord in rekord_list:
            sor = [str(ertek) for ertek in rekord]
            rekord_list_str.append(sor)

        return rekord_list_str, message

    def get_file_header_names(self, adatallomany_neve):
        header_names, message = self.fajl_import_repository.get_file_header_names(adatallomany_neve)
        header_names_string_list = []
        for header_name in header_names:
            header_names_string_list.append(header_name[0])
        return header_names_string_list, message

    def delete(self, nev):
        return self.fajl_import_repository.delete(nev)

    def create_rekord(self, selected_adatallomany_nev, rekord_ertekek):
        message = self.fajl_import_repository.create_rekord(selected_adatallomany_nev, rekord_ertekek)
        return message

    def update_rekord(self, selected_adatallomany_nev, selected_rekord_id, rekord_ertekek):
        message = self.fajl_import_repository.update_rekord(selected_adatallomany_nev, selected_rekord_id,
                                                         rekord_ertekek)
        return message

    def delete_rekord(self, selected_adatallomany_nev, selected_rekord_id):
        return self.fajl_import_repository.delete_rekord(selected_adatallomany_nev, selected_rekord_id)

    def getOneById(self, selected_adatallomany_nev, selected_rekord_id):
        rekord, message = self.fajl_import_repository.getOneById(selected_adatallomany_nev, selected_rekord_id)
        return rekord, message

