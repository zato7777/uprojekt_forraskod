import datetime
import os

from PyQt5 import QtCore, QtGui, QtWidgets

from Adatszotar.Controller.AdatallomanyController import AdatallomanyController
from Adatszotar.Controller.MezoController import MezoController
from Adatszotar.Controller.RekordleirasController import RekordleirasController
from FajlImport.FajlImportController import FajlImportController


class UiFajlImport(object):
    def __init__(self, fajl=None, fajl_nev=None, kivalasztott_fajl_url=None, df=None, rekordleiras_nev=None, adatallomany_nev=None, adatallomany_projekt=None,
                 adatallomany_titkositasi_kod=None, adatallomany_tulajdonos=None, adatallomany_verzioszam=None, generalt_rekordleiras=None):
        super().__init__()
        self.kivalasztott_fajl_url = kivalasztott_fajl_url
        self.generalt_rekordleiras = generalt_rekordleiras
        self.adatallomany_controller = AdatallomanyController()
        self.rekordleiras_controller = RekordleirasController()
        self.fajl_import_controller = FajlImportController()
        self.df = df
        self.fajl = fajl
        self.fajl_nev = fajl_nev
        self.rekordleiras_nev = rekordleiras_nev
        self.adatallomany_nev = adatallomany_nev
        self.adatallomany_projekt = adatallomany_projekt
        self.adatallomany_titkositasi_kod = adatallomany_titkositasi_kod
        self.adatallomany_tulajdonos = adatallomany_tulajdonos
        self.adatallomany_verzioszam = adatallomany_verzioszam

    def setupUi(self, UiFajlImport):
        UiFajlImport.setObjectName("UiFajlImport")
        UiFajlImport.resize(900, 600)
        UiFajlImport.setMinimumSize(900, 600)
        UiFajlImport.setMaximumSize(900, 600)
        self.window = UiFajlImport
        self.centralwidget = QtWidgets.QWidget(self.window)
        self.centralwidget.setObjectName("centralwidget")

        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(80, 40, 200, 20))
        self.label.setObjectName("label")
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)

        
        self.lineAdd_Fajl = QtWidgets.QLineEdit(self.centralwidget)
        self.lineAdd_Fajl.setGeometry(QtCore.QRect(300, 40, 180, 30))
        self.lineAdd_Fajl.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.lineAdd_Fajl.setReadOnly(True)
        self.lineAdd_Fajl.setObjectName("lineAdd_Fajl")

        self.pushButton_Tallozas = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Tallozas.setGeometry(QtCore.QRect(520, 40, 120, 30))
        self.pushButton_Tallozas.setObjectName("pushButton_Tallozas")
        self.pushButton_Tallozas.setFont(font)
        self.pushButton_Tallozas.clicked.connect(lambda: self.fajlEllenorzese())

        self.label2 = QtWidgets.QLabel(self.centralwidget)
        self.label2.setGeometry(QtCore.QRect(80, 100, 200, 20))
        self.label2.setObjectName("label")
        self.label2.setFont(font)
        
        #ez lesz a legördülő lista
        self.comboBox_rekordleiras = QtWidgets.QComboBox(self.centralwidget)

        rekordleiras_list, message = self.rekordleiras_controller.rekordleiras_list()
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        else:
            for i in range(0, len(rekordleiras_list)):
                self.comboBox_rekordleiras.addItem("")

        self.comboBox_rekordleiras.setGeometry(QtCore.QRect(300, 100, 180, 30))
        self.comboBox_rekordleiras.setObjectName("comboBox_rekordleiras")
        self.comboBox_rekordleiras.setFont(font)

        
        #ez pedig a generálás gomb(automatikusan generál egy rekordleirást)
        self.pushButton_Generate = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Generate.setGeometry(QtCore.QRect(520, 100, 120, 30))
        self.pushButton_Generate.setObjectName("pushButton_Generate")
        self.pushButton_Generate.setFont(font)
        self.pushButton_Generate.clicked.connect(lambda: self.openrekordleirasGeneralas())

        self.label3 = QtWidgets.QLabel(self.centralwidget)
        self.label3.setGeometry(QtCore.QRect(80, 160, 500, 20))
        self.label3.setObjectName("label")
        self.label3.setFont(font)

        self.lineEdit_adatallomany_nev = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_adatallomany_nev.setGeometry(QtCore.QRect(600, 160, 200, 30))
        self.lineEdit_adatallomany_nev.setObjectName("lineEdit_adatallomany_nev")
        self.lineEdit_adatallomany_nev.setFont(font)

        self.label4 = QtWidgets.QLabel(self.centralwidget)
        self.label4.setGeometry(QtCore.QRect(80, 220, 500, 20))
        self.label4.setObjectName("label")
        self.label4.setFont(font)

        self.lineEdit_adatallomany_projekt = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_adatallomany_projekt.setGeometry(QtCore.QRect(600, 220, 200, 30))
        self.lineEdit_adatallomany_projekt.setObjectName("lineEdit_adatallomany_projekt")
        self.lineEdit_adatallomany_projekt.setFont(font)

        self.label5 = QtWidgets.QLabel(self.centralwidget)
        self.label5.setGeometry(QtCore.QRect(80, 280, 500, 20))
        self.label5.setObjectName("label")
        self.label5.setFont(font)

        self.lineEdit_adatallomany_titkositasi_kod = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_adatallomany_titkositasi_kod.setGeometry(QtCore.QRect(600, 280, 200, 30))
        self.lineEdit_adatallomany_titkositasi_kod.setObjectName("lineEdit_adatallomany_titkositasi_kod")
        self.lineEdit_adatallomany_titkositasi_kod.setFont(font)

        self.label6 = QtWidgets.QLabel(self.centralwidget)
        self.label6.setGeometry(QtCore.QRect(80, 340, 500, 20))
        self.label6.setObjectName("label")
        self.label6.setFont(font)

        self.lineEdit_adatallomany_tulajdonos = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_adatallomany_tulajdonos.setGeometry(QtCore.QRect(600, 340, 200, 30))
        self.lineEdit_adatallomany_tulajdonos.setObjectName("lineEdit_adatallomany_tulajdonos")
        self.lineEdit_adatallomany_tulajdonos.setFont(font)

        self.label7 = QtWidgets.QLabel(self.centralwidget)
        self.label7.setGeometry(QtCore.QRect(80, 400, 500, 20))
        self.label7.setObjectName("label")
        self.label7.setFont(font)

        self.lineEdit_adatallomany_verzioszam = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_adatallomany_verzioszam.setGeometry(QtCore.QRect(600, 400, 200, 30))
        self.lineEdit_adatallomany_verzioszam.setObjectName("lineEdit_adatallomany_verzioszam")
        self.lineEdit_adatallomany_verzioszam.setFont(font)

        self.pushButton_Mentes = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Mentes.setGeometry(QtCore.QRect(250, 500, 100, 30))
        self.pushButton_Mentes.setObjectName("pushButton_Importalas")
        self.pushButton_Mentes.setFont(font)
        self.pushButton_Mentes.clicked.connect(lambda: self.fajlMentese())

        self.pushButton_Megse = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Megse.setGeometry(QtCore.QRect(450, 500, 100, 30))
        self.pushButton_Megse.setObjectName("pushButton_Megse")
        self.pushButton_Megse.setFont(font)
        self.pushButton_Megse.clicked.connect(lambda: UiFajlImport.close())

        self.window.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(self.window)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 500, 21))
        self.menubar.setObjectName("menubar")
        self.window.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(self.window)
        self.statusbar.setObjectName("statusbar")
        self.window.setStatusBar(self.statusbar)
        self.retranslateUi(self.window)
        QtCore.QMetaObject.connectSlotsByName(self.window)

    def retranslateUi(self, UiFajlImport):
        _translate = QtCore.QCoreApplication.translate
        UiFajlImport.setWindowTitle(_translate("UiFajlImport", "Fajl import"))
        self.label.setText(_translate("UiFajlImport", "Adatállomány kiválasztása:"))
        self.pushButton_Tallozas.setText(_translate("UiFajlImport", "Tallózás"))
        self.label2.setText(_translate("UiFajlImport", "Rekordleírás kiválasztása:"))

        rekordleiras_list, message = self.rekordleiras_controller.rekordleiras_list()
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        else:
            for i in range(0, len(rekordleiras_list)):
                self.comboBox_rekordleiras.setItemText(i, _translate("UiFajlImport", rekordleiras_list[i].nev))

        if self.generalt_rekordleiras is not None:
            self.comboBox_rekordleiras.setCurrentText(self.generalt_rekordleiras)
        self.pushButton_Generate.setText(_translate("UiFajlImport", "Generálás"))
        self.label3.setText(_translate("UiFajlImport", "Adja meg az adatállomány nevét:"))
        self.label4.setText(_translate("UiFajlImport", "Adja meg, hogy az adatállomány melyik projekthez tartozik:"))
        self.label5.setText(_translate("UiFajlImport", "Adja meg az adatállomány titkosítási kódját:"))
        self.label6.setText(_translate("UiFajlImport", "Adja meg az adatállomány tulajdonosát:"))
        self.label7.setText(_translate("UiFajlImport", "Adja meg az adatállomány verziószámát:"))
        self.pushButton_Mentes.setText(_translate("UiFajlImport", "Futtatás"))
        self.pushButton_Megse.setText(_translate("UiFajlImport", "Mégse"))

        font = QtGui.QFont()
        font.setPointSize(12)

        if self.fajl_nev is not None:
            item = self.lineAdd_Fajl
            item.setText(_translate("UiFajlImport", self.fajl_nev))
            item.setFont(font)
        if self.rekordleiras_nev is not None and self.generalt_rekordleiras is None:
            item = self.comboBox_rekordleiras
            item.setCurrentText(_translate("UiFajlImport", self.rekordleiras_nev))
            item.setFont(font)
        if self.adatallomany_nev is not None:
            item = self.lineEdit_adatallomany_nev
            item.setText(_translate("UiFajlImport", self.adatallomany_nev))
            item.setFont(font)
        if self.adatallomany_projekt is not None:
            item = self.lineEdit_adatallomany_projekt
            item.setText(_translate("UiFajlImport", self.adatallomany_projekt))
            item.setFont(font)
        if self.adatallomany_titkositasi_kod is not None:
            item = self.lineEdit_adatallomany_titkositasi_kod
            item.setText(_translate("UiFajlImport", self.adatallomany_titkositasi_kod))
            item.setFont(font)
        if self.adatallomany_tulajdonos is not None:
            item = self.lineEdit_adatallomany_tulajdonos
            item.setText(_translate("UiFajlImport", self.adatallomany_tulajdonos))
            item.setFont(font)
        if self.adatallomany_verzioszam is not None:
            item = self.lineEdit_adatallomany_verzioszam
            item.setText(_translate("UiFajlImport", self.adatallomany_verzioszam))
            item.setFont(font)

    def fajlEllenorzese(self):
        try:
            self.fajl = QtWidgets.QFileDialog.getOpenFileUrl(caption="Adatállomány importálása",
                                                        filter="Adatállomány (*.csv *.sas7bdat *.xlsx)",
                                                        initialFilter="Adatállomány (*.csv *.sas7bdat *.xlsx)")

            self.kivalasztott_fajl_url = self.fajl[0].toString()
            self.fajl_nev = self.fajl[0].fileName()
            self.lineAdd_Fajl.setText(self.fajl_nev)
            font = QtGui.QFont()
            font.setPointSize(12)
            self.lineAdd_Fajl.setFont(font)
            self.df = self.fajl_import_controller.adatallomanyt_betolt(self.kivalasztott_fajl_url)

        except Exception as e:
            QtWidgets.QMessageBox.warning(self.window, "Adatállomány importálása",
                                          "Hiba az adatállomány importálása közben! Próbálja újra!" + repr(e))


    def fajlMentese(self):
        if self.lineAdd_Fajl.text() == "":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", "Válasszon ki adatállományt!")
            return

        if self.lineEdit_adatallomany_nev.text() == "":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", "Adja meg az adatállomány nevét!")
            return

        adatallomany_list, message = self.adatallomany_controller.adatszotar_list()
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)

        for adatallomany in adatallomany_list:
            if self.lineEdit_adatallomany_nev.text() == adatallomany.nev:
                QtWidgets.QMessageBox.warning(self.window, "Hiba", "Nem lehet két azonos nevű adatállományt létrehozni!")
                return

        message = self.fajl_import_controller.fajl_mentese(self.lineEdit_adatallomany_nev.text(), self.df)
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
            return
        else:
            rekordszam, message = self.fajl_import_controller.get_fajl_rekordszam(self.lineEdit_adatallomany_nev.text())
            if message != "sikeres":
                QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
                return
            else:
                self.adatallomanytLetrehoz(rekordszam)

    #létrehoz egy rekodot az adatállományok táblában
    def adatallomanytLetrehoz(self, rekordszam):
        rendes_url = self.kivalasztott_fajl_url[8:]
        letrehozas_datuma = datetime.datetime.fromtimestamp(os.path.getctime(rendes_url))
        message = self.adatallomany_controller.adatallomanyt_letrehoz(rendes_url, letrehozas_datuma,
                                                                      self.lineEdit_adatallomany_nev.text(),
                                                                      self.lineEdit_adatallomany_projekt.text(),
                                                                      rekordszam,
                                                                      self.lineEdit_adatallomany_titkositasi_kod.text(),
                                                                      self.lineEdit_adatallomany_tulajdonos.text(),
                                                                      self.lineEdit_adatallomany_verzioszam.text(),
                                                                      self.comboBox_rekordleiras.currentText())
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        else:
            QtWidgets.QMessageBox.information(self.window, "Info", "Sikeres fájl importálás!")
            self.window.close()

    #generál egy rekordleírást a betöltött adatállomány alapján
    def openrekordleirasGeneralas(self):
        self.adatallomany_nev = self.lineEdit_adatallomany_nev.text()
        self.adatallomany_projekt = self.lineEdit_adatallomany_projekt.text()
        self.adatallomany_titkositasi_kod = self.lineEdit_adatallomany_titkositasi_kod.text()
        self.adatallomany_tulajdonos = self.lineEdit_adatallomany_tulajdonos.text()
        self.adatallomany_verzioszam = self.lineEdit_adatallomany_verzioszam.text()
        if self.fajl is None or self.fajl_nev is None or self.kivalasztott_fajl_url is None or self.df is None:
            QtWidgets.QMessageBox.warning(self.window, "Hiba", "Ahhoz, hogy rekordleírás generálódjon előbb ki kell"
                                                               " választani az importálni kívánt fájlt!")
            self.window.close()
            self.window = QtWidgets.QMainWindow()
            self.ui = UiFajlImport(self.fajl, self.fajl_nev, self.kivalasztott_fajl_url, self.df, self.rekordleiras_nev,
                                   self.adatallomany_nev, self.adatallomany_projekt, self.adatallomany_titkositasi_kod,
                                   self.adatallomany_tulajdonos, self.adatallomany_verzioszam, self.generalt_rekordleiras)
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            self.window.close()
            self.window = QtWidgets.QMainWindow()
            self.ui = UiRekordleirasGeneralas([self.fajl, self.lineAdd_Fajl.text(), self.kivalasztott_fajl_url,
                                              self.df, self.comboBox_rekordleiras.currentText(),
                                              self.lineEdit_adatallomany_nev.text(), self.lineEdit_adatallomany_projekt.text(),
                                              self.lineEdit_adatallomany_titkositasi_kod.text(), self.lineEdit_adatallomany_tulajdonos.text(),
                                              self.lineEdit_adatallomany_verzioszam.text()])
            self.ui.setupUi(self.window)
            self.window.show()


class UiRekordleirasGeneralas(object):
    def __init__(self, fajl_import_args):
        self.fajl_import_args = fajl_import_args
        self.rekordleiras_controller = RekordleirasController()
        self.mezo_controller = MezoController()
        self.kivalasztott_fajl_url = fajl_import_args[2]
        self.tipus = None

    def setupUi(self, UiRekordleirasGeneralas):
        UiRekordleirasGeneralas.setObjectName("UiRekordleirasGeneralas")
        UiRekordleirasGeneralas.resize(550, 200)
        UiRekordleirasGeneralas.setMinimumSize(550, 200)
        UiRekordleirasGeneralas.setMaximumSize(550, 200)
        self.window = UiRekordleirasGeneralas
        self.centralwidget = QtWidgets.QWidget(self.window)
        self.centralwidget.setObjectName("centralwidget")

        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(0, 0, 550, 200))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.frame.setFont(font)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")

        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(40, 40, 240, 20))
        self.label.setObjectName("label")
        self.label.setFont(font)

        self.lineEdit_rekordleiras_nev = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_rekordleiras_nev.setGeometry(QtCore.QRect(300, 40, 200, 20))
        self.lineEdit_rekordleiras_nev.setObjectName("lineEdit_rekordleiras_nev")
        self.lineEdit_rekordleiras_nev.setFont(font)

        self.pushButton_Generalas = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Generalas.setGeometry(QtCore.QRect(150, 120, 100, 30))
        self.pushButton_Generalas.setObjectName("pushButton_Mentes")
        self.pushButton_Generalas.setFont(font)
        self.pushButton_Generalas.clicked.connect(lambda: self.rekordleirasGeneralas())

        self.pushButton_Megse = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Megse.setGeometry(QtCore.QRect(350, 120, 100, 30))
        self.pushButton_Megse.setObjectName("pushButton_Megse")
        self.pushButton_Megse.setFont(font)
        self.pushButton_Megse.clicked.connect(lambda: self.closeUiRekordleirasGeneralas())

        self.window.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(self.window)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 600, 20))
        self.menubar.setObjectName("menubar")
        self.window.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(self.window)
        self.statusbar.setObjectName("statusbar")
        self.window.setStatusBar(self.statusbar)
        self.retranslateUi(self.window)
        QtCore.QMetaObject.connectSlotsByName(self.window)

    def retranslateUi(self, UiRekordleirasGeneralas):
        _translate = QtCore.QCoreApplication.translate
        UiRekordleirasGeneralas.setWindowTitle(_translate("UiRekordleirasGeneralas", "Rekordleírás generálás"))
        self.label.setText(_translate("UiRekordleirasGeneralas", "Add meg a rekordleírás nevét:"))
        self.pushButton_Generalas.setText(_translate("UiFajlImport", "Generálás"))
        self.pushButton_Megse.setText(_translate("UiFajlImport", "Mégse"))

    def rekordleirasGeneralas(self):
        if self.kivalasztott_fajl_url.endswith(".csv"):
            self.tipus = "CSV"
        elif self.kivalasztott_fajl_url.endswith(".sas7bdat"):
            self.tipus = "SAS"
        elif self.kivalasztott_fajl_url.endswith(".xlsx"):
            self.tipus = "Excel"

        if self.lineEdit_rekordleiras_nev.text() is None or self.lineEdit_rekordleiras_nev.text() == "":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", "Ahhoz, hogy rekordleírás generálódjon meg kell adni annak nevét!")
            return

        message, rekordleiras_id = self.rekordleiras_controller.rekordleiras_generalas(self.lineEdit_rekordleiras_nev.text(), self.tipus)
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)

        message = self.mezo_controller.mezo_generalas(self.fajl_import_args[3], rekordleiras_id)
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)

        self.window.close()
        self.window = QtWidgets.QMainWindow()
        self.ui = UiFajlImport(self.fajl_import_args[0], self.fajl_import_args[1], self.fajl_import_args[2],
                               self.fajl_import_args[3],
                               self.fajl_import_args[4], self.fajl_import_args[5], self.fajl_import_args[6],
                               self.fajl_import_args[7],
                               self.fajl_import_args[8], self.fajl_import_args[9], self.lineEdit_rekordleiras_nev.text())
        self.ui.setupUi(self.window)
        self.window.show()

    def closeUiRekordleirasGeneralas(self):
        self.window.close()
        self.window = QtWidgets.QMainWindow()
        self.ui = UiFajlImport(self.fajl_import_args[0], self.fajl_import_args[1], self.fajl_import_args[2], self.fajl_import_args[3],
                               self.fajl_import_args[4], self.fajl_import_args[5], self.fajl_import_args[6], self.fajl_import_args[7],
                               self.fajl_import_args[8], self.fajl_import_args[9])
        self.ui.setupUi(self.window)
        self.window.show()

