import pandas as pd

from FajlImport.FajlImportService import FajlImportService


class FajlImportController(object):
    def __init__(self):
        self.fajl_import_service = FajlImportService()

    # betölti az adatállományt
    def adatallomanyt_betolt(self, url):
        if url.endswith(".csv"):
            return pd.read_csv(url)

        elif url.endswith(".sas7bdat"):
            return pd.read_sas(url)

        elif url.endswith(".xlsx"):
            return pd.read_excel(url)

        return "sikertelen"

    def adatallomanyt_torol(self, adatallomany_nev):
        return self.fajl_import_service.delete(adatallomany_nev)


    # elmenti az adatállományt az adatbázisba
    def fajl_mentese(self, adatallomany_neve, df):
        if isinstance(df, pd.DataFrame) and isinstance(adatallomany_neve, str):
            message = self.fajl_import_service.createTableFromFajl(adatallomany_neve, df)

            if message == "sikeres":
                return self.fajl_import_service.fillUpTable(adatallomany_neve, df)
            else:
                return message

        return "sikertelen"

    def get_fajl_rekordszam(self, adatallomany_neve):
        rekordszam, message = self.fajl_import_service.get_fajl_rekordszam(adatallomany_neve)
        return rekordszam, message

    def get_fajl_mezoszam(self, adatallomany_neve):
        mezoszam, message = self.fajl_import_service.get_fajl_mezoszam(adatallomany_neve)
        return mezoszam, message

    def rekord_list(self, adatallomany_neve):
        rekord_list, message = self.fajl_import_service.rekord_list(adatallomany_neve)
        return rekord_list, message

    def get_file_header_names(self, adatallomany_neve):
        header_names, message = self.fajl_import_service.get_file_header_names(adatallomany_neve)
        return header_names, message

    def create_rekord(self, selected_adatallomany_nev, rekord_ertekek):
        message = self.fajl_import_service.create_rekord(selected_adatallomany_nev, rekord_ertekek)
        return message

    def update_rekord(self, selected_adatallomany_nev, selected_rekord_id, rekord_ertekek):
        message = self.fajl_import_service.update_rekord(selected_adatallomany_nev, selected_rekord_id, rekord_ertekek)
        return message

    def delete_rekord(self, selected_adatallomany_nev, selected_rekord_id):
        return self.fajl_import_service.delete_rekord(selected_adatallomany_nev, selected_rekord_id)

    def rekord_getOneById(self, selected_adatallomany_nev, selected_rekord_id):
        rekord, message = self.fajl_import_service.getOneById(selected_adatallomany_nev, selected_rekord_id)
        return rekord, message
