import pymysql


class FajlImportRepository(object):
    def createTableFromFajl(self, adatallomany_neve, columns_data_types):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="adatallomanyok", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        message = "sikeres"
        letezik = False

        try:
            cursor.execute("SHOW TABLES LIKE '{}'".format(adatallomany_neve))
            letezik = cursor.fetchone() is not None
        except Exception as e:
            message = "Fájl importálás sikertelen. Próbáld újra! " + repr(e)

        if letezik:
            return "Már létezik a tábla!"

        #ha nem létezik, létrehozás
        sql_query = f"""CREATE TABLE {adatallomany_neve} ({self.createTableColumns(columns_data_types)})"""

        try:
            cursor.execute(sql_query)
        except Exception as e:
            message = "Fájl importálás sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message

    def fillUpTable(self, adatallomany_neve, column_names, df_values):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="adatallomanyok", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        message = "sikeres"

        values = self.convertValues(column_names)

        df_columns = ""

        for column in column_names:
            if self.is_mysql_keyword(column):
                df_columns += f"`{column}`, "
            else:
                df_columns += column + ", "

        df_columns = df_columns.rstrip(", ")

        sql_query = f"""INSERT INTO {adatallomany_neve} ({df_columns}) VALUES ({values})"""

        try:
            cursor.executemany(sql_query, df_values)
        except Exception as e:
            message = "Fájl importálás sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message

    def delete(self, nev):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="adatallomanyok", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        sql_query = f"""DROP TABLE {nev}"""
        message = "sikeres"

        try:
            cursor.execute(sql_query)
        except Exception as e:
            message = "Törlés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message


    def get_fajl_rekordszam(self, adatallomany_neve):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="adatallomanyok", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        sql_query = f"""SELECT COUNT(*) FROM {adatallomany_neve}"""
        message = "sikeres"
        rekordszam = None

        try:
            cursor.execute(sql_query)
            rekordszam = cursor.fetchone()[0]
        except Exception as e:
            message = "Lekérdezés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.close()

        return rekordszam, message

    def get_fajl_mezoszam(self, adatallomany_neve):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="adatallomanyok", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        sql_query = f"""SELECT COUNT(*)
                        FROM information_schema.columns
                        WHERE table_schema = 'adatallomany' AND table_name = '{adatallomany_neve}'"""

        sql_query = f"""SHOW COLUMNS FROM {adatallomany_neve}"""

        message = "sikeres"
        mezoszam = None

        try:
            cursor.execute(sql_query)
            mezoszam = len(cursor.fetchall())
        except Exception as e:
            message = "Lekérdezés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.close()

        return mezoszam, message

    def rekord_list(self, adatallomany_neve):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="adatallomanyok", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        sql_query = f"""SELECT * FROM {adatallomany_neve}"""

        message = "sikeres"
        rekord_list = None

        try:
            cursor.execute(sql_query)
            rekord_list = cursor.fetchall()
        except Exception as e:
            message = "Lekérdezés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.close()

        return rekord_list, message

    def get_file_header_names(self, adatallomany_neve):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="adatallomanyok", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        sql_query = f"""SELECT column_name
                        FROM information_schema.columns
                        WHERE table_schema = 'adatallomanyok'
                        AND table_name = '{adatallomany_neve}'
                        ORDER BY ordinal_position"""

        message = "sikeres"
        header_names = None

        try:
            cursor.execute(sql_query)
            header_names = cursor.fetchall()
        except Exception as e:
            message = "Lekérdezés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.close()

        return header_names, message

    def create_rekord(self, selected_adatallomany_nev, rekord_ertekek):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="adatallomanyok", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        header_names, message = self.get_file_header_names(selected_adatallomany_nev)
        sql_header_names_string = ""

        for i in range(1, len(header_names)):
            if self.is_mysql_keyword(str(header_names[i][0])):
                sql_header_names_string += "`" + str(header_names[i][0]) + "`, "
            else:
                sql_header_names_string += str(header_names[i][0]) + ", "

        sql_header_names_string = sql_header_names_string.rstrip(", ")

        sql_header_values_string = ""

        for i in range(1, len(header_names)):
            sql_header_values_string += "%s, "

        sql_header_values_string = sql_header_values_string.rstrip(", ")

        sql_query = f"""INSERT INTO {selected_adatallomany_nev} 
                        ({sql_header_names_string}) 
                        VALUES ({sql_header_values_string})"""

        message = "sikeres"

        try:
            cursor.execute(sql_query, rekord_ertekek)
        except Exception as e:
            message = "Létrehozás sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message

    def update_rekord(self, selected_adatallomany_nev, selected_rekord_id, rekord_ertekek):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="adatallomanyok", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        header_names, message = self.get_file_header_names(selected_adatallomany_nev)
        sql_set_string = ""

        for i in range(1, len(header_names)):
            if self.is_mysql_keyword(str(header_names[i][0])):
                sql_set_string += "`" + str(header_names[i][0]) + "` = %s, "
            else:
                sql_set_string += str(header_names[i][0]) + " = %s, "

        sql_set_string = sql_set_string.rstrip(", ")

        sql_query = f"""UPDATE {selected_adatallomany_nev} 
                        SET {sql_set_string}
                        WHERE id = %s"""

        rekord_ertekek.append(selected_rekord_id)
        message = "sikeres"

        try:
            cursor.execute(sql_query, rekord_ertekek)
        except Exception as e:
            message = "Módosítás sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message

    def delete_rekord(self, selected_adatallomany_nev, selected_rekord_id):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="adatallomanyok", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        sql_query = f"""DELETE FROM {selected_adatallomany_nev} WHERE id = %s"""

        message = "sikeres"

        try:
            cursor.execute(sql_query, selected_rekord_id)
        except Exception as e:
            message = "Törlés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message


    def getOneById(self, selected_adatallomany_nev, selected_rekord_id):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="adatallomanyok", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        sql_query = f"""SELECT * FROM {selected_adatallomany_nev} WHERE id = %s"""

        message = "sikeres"
        result = None

        try:
            cursor.execute(sql_query, (selected_rekord_id,))
            result = cursor.fetchone()
        except Exception as e:
            message = "Lekérdezés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.close()

        return result, message


    def createTableColumns(self, columns_data_types):
        result = ""

        for column_name, column_data_type in columns_data_types.items():
            if self.is_mysql_keyword(column_name):
                result += f"`{column_name}` {column_data_type}, "
            else:
                result += column_name + " " + column_data_type + ","

        result = result.rstrip(", ")

        return result

    def convertValues(self, df_columns):
        values = ""

        for i in range(0, len(df_columns) - 1):
            values += "%s, "
        values += "%s"

        return values


    def is_mysql_keyword(self, column_name):
        foglalt_szavak = ["ADD", "ALL", "ALTER", "ANALYZE", "AND", "AS", "ASC", "ASENSITIVE", "BEFORE", "BETWEEN",
                          "BIGINT", "BINARY", "BLOB", "BOTH", "BY", "CALL", "CASCADE", "CASE", "CHANGE", "CHAR",
                          "CHARACTER", "CHECK", "COLLATE", "COLUMN", "CONDITION", "CONSTRAINT", "CONTINUE",
                          "CONVERT",
                          "CREATE", "CROSS", "CURRENT_DATE", "CURRENT_TIME", "CURRENT_TIMESTAMP", "CURRENT_USER",
                          "CURSOR",
                          "DATABASE", "DATABASES", "DAY_HOUR", "DAY_MICROSECOND", "DAY_MINUTE", "DAY_SECOND", "DEC",
                          "DECIMAL",
                          "DECLARE", "DEFAULT", "DELAYED", "DELETE", "DESC", "DESCRIBE", "DETERMINISTIC",
                          "DISTINCT", "DISTINCTROW",
                          "DIV", "DOUBLE", "DROP", "DUAL", "EACH", "ELSE", "ELSEIF", "ENCLOSED", "ESCAPED",
                          "EXISTS", "EXIT", "EXPLAIN",
                          "FALSE", "FETCH", "FLOAT", "FLOAT4", "FLOAT8", "FOR", "FORCE", "FOREIGN", "FROM",
                          "FULLTEXT", "GRANT",
                          "GROUP", "HAVING", "HIGH_PRIORITY", "HOUR_MICROSECOND", "HOUR_MINUTE", "HOUR_SECOND",
                          "IF", "IGNORE", "IN",
                          "INDEX", "INFILE", "INNER", "INOUT", "INSENSITIVE", "INSERT", "INT", "INT1", "INT2",
                          "INT3", "INT4", "INT8",
                          "INTEGER", "INTERVAL", "INTO", "IS", "ITERATE", "JOIN", "KEY", "KEYS", "KILL", "LABEL",
                          "LEADING", "LEAVE",
                          "LEFT", "LIKE", "LIMIT", "LINEAR", "LINES", "LOAD", "LOCALTIME", "LOCALTIMESTAMP", "LOCK",
                          "LONG", "LONGBLOB",
                          "LONGTEXT", "LOOP", "LOW_PRIORITY", "MATCH", "MEDIUMBLOB", "MEDIUMINT", "MEDIUMTEXT",
                          "MIDDLEINT", "MINUTE_MICROSECOND",
                          "MINUTE_SECOND", "MOD", "MODIFIES", "NATURAL", "NOT", "NO_WRITE_TO_BINLOG", "NULL",
                          "NUMERIC", "ON", "OPTIMIZE",
                          "OPTION", "OPTIONALLY", "OR", "ORDER", "OUT", "OUTER", "OUTFILE", "PRECISION", "PRIMARY",
                          "PROCEDURE", "PURGE",
                          "RAID0", "READ", "READS", "REAL", "REFERENCES", "REGEXP", "RELEASE", "RENAME", "REPEAT",
                          "REPLACE", "REQUIRE",
                          "RESTRICT", "RETURN", "REVOKE", "RIGHT", "RLIKE", "SCHEMA", "SCHEMAS",
                          "SECOND_MICROSECOND", "SELECT", "SENSITIVE",
                          "SEPARATOR", "SET", "SHOW", "SMALLINT", "SPATIAL", "SPECIFIC", "SQL", "SQLEXCEPTION",
                          "SQLSTATE", "SQLWARNING",
                          "SQL_BIG_RESULT", "SQL_CALC_FOUND_ROWS", "SQL_SMALL_RESULT", "SSL", "STARTING",
                          "STRAIGHT_JOIN", "TABLE", "TERMINATED",
                          "THEN", "TINYBLOB", "TINYINT", "TINYTEXT", "TO", "TRAILING", "TRIGGER", "TRUE", "UNDO",
                          "UNION", "UNIQUE", "UNLOCK",
                          "UNSIGNED", "UPDATE", "USAGE", "USE", "USING", "UTC_DATE", "UTC_TIME", "UTC_TIMESTAMP",
                          "VALUES", "VARBINARY", "VARCHAR",
                          "VARCHARACTER", "VARYING", "WHEN", "WHERE", "WHILE", "WITH", "WRITE", "X509", "XOR",
                          "YEAR_MONTH", "ZEROFILL"]

        return column_name.upper() in foglalt_szavak


