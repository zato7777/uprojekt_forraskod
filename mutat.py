import pandas as pd
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout, QTableWidget, QTableWidgetItem, QRadioButton
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
import os
import numpy
import import2
from import2 import ablak
from import2 import main
from PyQt5 import QtCore
import glos

import sys
import pandas as pd
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidget, QTableWidgetItem, QDialog, QLabel, QMessageBox
from pathlib import Path


class TableDisplay(QMainWindow):
    def __init__(self, dataframe):
        super().__init__()
        
        self.dataframe = dataframe
        self.setWindowTitle("Mutatók")
        self.setGeometry(100, 100, 1500, 600)
        
        self.table_widget = QTableWidget(self)
        self.table_widget.setGeometry(50, 50, 1300, 400)
        
        button=QPushButton("Exportálás", self, clicked=self.exportToExcel)
        button.setGeometry(10, 10, 70, 30)
        
        self.rb1=QRadioButton("CSV", self)
        self.rb1.setGeometry(85, 10, 50, 30)

        self.rb2=QRadioButton("Excel", self)
        self.rb2.setGeometry(140, 10, 65, 30)

        
        button3=QPushButton('Új sor', self, clicked=self.ujsor)
        button3.setGeometry(200, 10, 100, 30)

        button2=QPushButton("Mentés", self, clicked=self.gloss)
        button2.setGeometry(310, 10, 100, 30)

       
        self.counter=0
        self.nyomva=False
        
        
        
        self.setup_table()
       

    
    def ujsor(self):
            
            self.nyomva=True
            self.setup_table()
    

    def exportToExcel(self):
        oszlopnevek = []

        for i in range(self.table_widget.model().columnCount()):
            oszlopnevek.append(self.table_widget.horizontalHeaderItem(i).text())
        df = pd.DataFrame(columns=oszlopnevek)
 
        for row in range(self.table_widget.rowCount()):
            for col in range(self.table_widget.columnCount()):
                df.at[row, oszlopnevek[col]] = self.table_widget.item(row, col).text()

        if self.rb2.isChecked():
            df.to_excel('Mutatók_nómenklatúrák.xlsx', index=False)
            print('Excel fájl exportálva')
        elif self.rb1.isChecked():
            df.to_csv('Mutatók_nómenklatúrák.csv', encoding="ISO-8859-2", index=False)
            print('CSV fájl exportálva')
        else:
            figy=QMessageBox(self)
            figy.setWindowTitle('Figyelmeztetés')
            figy.setText("Válasszon az Excel és a CSV közt!")
            figy.exec()
        
          
    
    def setup_table(self):
        if self.nyomva==True:
            self.counter+=1
            print(self.counter)
        
        
        
        
        self.table_widget.setRowCount(self.dataframe.shape[0]+self.counter)
        self.table_widget.setColumnCount(self.dataframe.shape[1])
      
        self.table_widget.setHorizontalHeaderLabels(self.dataframe.columns)
       
        for i in range(self.dataframe.shape[0]):
            for j in range(self.dataframe.shape[1]):
                item = QTableWidgetItem(str(self.dataframe.iloc[i, j]))
                self.table_widget.setItem(i, j, item)

                if j < 2:
                    item.setFlags(item.flags() & ~QtCore.Qt.ItemIsEditable)
            
              
                   
        self.table_widget.resizeColumnsToContents()
        self.table_widget.resizeRowsToContents()
    
    def gloss(self):
        oszlopnevek = []

        for i in range(self.table_widget.model().columnCount()):
            oszlopnevek.append(self.table_widget.horizontalHeaderItem(i).text())
        df = pd.DataFrame(columns=oszlopnevek)
 
        for row in range(self.table_widget.rowCount()):
            for col in range(self.table_widget.columnCount()):
                df.at[row, oszlopnevek[col]] = self.table_widget.item(row, col).text()
        df.to_csv('glo.csv', sep=',', encoding="ISO-8859-2", index=False)
        filename='glos.csv'
        fp=str(os.getcwd()+'\glo.csv')
    
        self.file_path=filename
        print(self.file_path)
        print(fp)
        fp=str(fp.replace("\\", "/"))
        print(fp)   
        f=open('gl.txt', 'w')
        f.write(fp)
        
         
      
     

def main():
    
    app = QApplication(sys.argv)
    with open('utso.txt', 'r') as file:
        idk = file.read()
    global df
    if idk:
        df=pd.read_csv(idk)
        a1=[]
        a2=[]
        for x in df.columns:
            a1.append(x)
            
        for x in df.iloc[2]:
            
                if x=='Yes' or x=='yes' or x=='True' or x=='true' or x=='No' or x=='no' or x=='False' or x=='false':
                    x=bool(x)     
                a2.append(str(type(x)).replace("<class", "").replace(">", ""))

        glos=dict(zip(a1, a2))
        print(glos)
        dfglos=pd.DataFrame(columns=['Név', 'Nyomtatási cimke', 'Hossz', 'Adattípus', 'Mutató', 'Leírás', 'Mutatócsoport',
                                 'Utolsó módosítás', 'Érvényesség kezdete', 'Érvényesség vége', 'Tulaj', 'Szervezet'])
        i=1
        for x in glos:
            dfglos.loc[i, 'Név']=x
            dfglos.loc[i, 'Nyomtatási cimke']=" "
            dfglos.loc[i, 'Hossz']=" "
            dfglos.loc[i, 'Adattípus']=glos[x]
            if glos[x]==" 'str'":
                dfglos.loc[i, 'Mutató']='Nómenklatúra'
            else:
                dfglos.loc[i, 'Mutató']='Mutató'
            dfglos.loc[i, 'Leírás']=" "
            dfglos.loc[i, 'Mutatócsoport']=" "
            dfglos.loc[i, 'Utolsó módosítás']=" "
            dfglos.loc[i, 'Érvényesség kezdete']=" "
            dfglos.loc[i, 'Érvényesség vége']=" "
            dfglos.loc[i, 'Tulaj']=" "
            dfglos.loc[i, 'Szervezet']=" "
            i=i+1

        data2=dfglos

        

        
        df = pd.DataFrame(data2)
       
        
        

        window =TableDisplay(df)
        window.show()
        
        sys.exit(app.exec_())
        

if __name__ == "__main__":
    main()