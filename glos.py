import pandas as pd
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout, QTableWidget, QTableWidgetItem, QRadioButton
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
import os
import numpy
from import2 import main
from PyQt5 import QtCore

import sys
import pandas as pd
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidget, QTableWidgetItem, QDialog, QLabel, QMessageBox


class aa(QMainWindow):
    def __init__(self, dataframe):
        super().__init__()
        
        self.dataframe = dataframe
        self.setWindowTitle("Mutatók")
        self.setGeometry(100, 100, 500, 400)
        
        self.table_widget = QTableWidget(self)
        self.table_widget.setGeometry(50, 50, 400, 300)
        
        button=QPushButton("Exportálás", self, clicked=self.exportToExcel)
        button.setGeometry(10, 10, 70, 30)
        
        self.rb1=QRadioButton("CSV", self)
        self.rb1.setGeometry(85, 10, 50, 30)

        self.rb2=QRadioButton("Excel", self)
        self.rb2.setGeometry(140, 10, 65, 30)

       

        
        self.setup_table()
        

    def exportToExcel(self):
        oszlopnevek = []

        for i in range(self.table_widget.model().columnCount()):
            oszlopnevek.append(self.table_widget.horizontalHeaderItem(i).text())
        df = pd.DataFrame(columns=oszlopnevek)
 
        for row in range(self.table_widget.rowCount()):
            for col in range(self.table_widget.columnCount()):
                df.at[row, oszlopnevek[col]] = self.table_widget.item(row, col).text()

        if self.rb2.isChecked():
            df.to_excel('glossary.xlsx', index=False)
            print('Excel fájl exportálva')
        elif self.rb1.isChecked():
            df.to_csv('glossary.csv', index=False)
            print('CSV fájl exportálva')
        else:
            figy=QMessageBox(self)
            figy.setWindowTitle('Figyelmeztetés')
            figy.setText("Válasszon az Excel és a CSV közt!")
            figy.exec()
        
    
        
    
    def setup_table(self):
        self.table_widget.setRowCount(self.dataframe.shape[0])
        self.table_widget.setColumnCount(self.dataframe.shape[1])
      
        self.table_widget.setHorizontalHeaderLabels(self.dataframe.columns)
       
        for i in range(self.dataframe.shape[0]):
            for j in range(self.dataframe.shape[1]):
                item = QTableWidgetItem(str(self.dataframe.iloc[i, j]))
                self.table_widget.setItem(i, j, item)

                if j < 2:
                    item.setFlags(item.flags() & ~QtCore.Qt.ItemIsEditable)
            
              
                   
        self.table_widget.resizeColumnsToContents()
        self.table_widget.resizeRowsToContents()

def main():
    app=QApplication(sys.argv)

    
    data = pd.read_csv('glo.csv', sep=",", encoding="ISO-8859-2")
    d2=data.drop(['Nyomtatási cimke', 'Hossz', 'Mutató', 'Mutatócsoport', 'Utolsó módosítás', 'Érvényesség kezdete', 'Érvényesség vége'], axis=1)
    print(d2)
    df=pd.DataFrame(d2)
    window =aa(df)
    window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()