from business_glossary.DbModel.MutatoDbModel import MutatoDbModel
from business_glossary.Repository.MutatoRepository import MutatoRepository
from Adatszotar.Service.IService import IService
from Adatszotar.Service.ServiceBase import ServiceBase
from business_glossary.ViewModel.MutatoViewModel import MutatoViewModel


class MutatoService(ServiceBase, IService):
    def __init__(self):
        super().__init__(MutatoRepository(), MutatoService)

    def assembleViewModel(self, db_model):
        message = "sikeres"
        return MutatoViewModel(db_model[0], db_model[1], db_model[2], db_model[3], db_model[4],
                               db_model[5], db_model[6], db_model[7], db_model[8], db_model[9], db_model[10]), message

    def assembleDbModel(self, view_model):
        return MutatoDbModel(view_model.mutato_csoport, view_model.nev, view_model.cimke, view_model.leiras,
                             view_model.hossz, view_model.tipus,
                             view_model.utolso_modositas, view_model.ervenyesseg_kezdete, view_model.ervenyesseg_vege,
                             view_model.tulajdonos, view_model.szervezet)
