from business_glossary.DbModel.NomenklaturaDbModel import NomenklaturaDbModel
from business_glossary.Repository.NomenklaturaRepository import NomenklaturaRepository
from Adatszotar.Service.IService import IService
from Adatszotar.Service.ServiceBase import ServiceBase
from business_glossary.ViewModel.NomenklaturaViewModel import NomenklaturaViewModel


class NomenklaturaService(ServiceBase, IService):
    def __init__(self):
        super().__init__(NomenklaturaRepository(), NomenklaturaService)

    def assembleViewModel(self, db_model):
        message = "sikeres"
        return NomenklaturaViewModel(db_model[0], db_model[1], db_model[2], db_model[3], db_model[4],
                                     db_model[5], db_model[6], db_model[7], db_model[8], db_model[9]), message

    def assembleDbModel(self, view_model):
        return NomenklaturaDbModel(view_model.nev, view_model.cimke, view_model.leiras,
                                   view_model.hossz, view_model.tipus, view_model.utolso_modositas,
                                   view_model.ervenyesseg_kezdete, view_model.ervenyesseg_vege, view_model.tulajdonos,
                                   view_model.szervezet)
