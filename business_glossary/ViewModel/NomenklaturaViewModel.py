class NomenklaturaViewModel(object):
    def __init__(self, nev, cimke, leiras, hossz, tipus, utolso_modositas, ervenyesseg_kezdete, ervenyesseg_vege, tulajdonos, szervezet):
        self.nev = nev
        self.cimke = cimke
        self.leiras = leiras
        self.hossz = hossz
        self.tipus = tipus
        self.utolso_modositas = utolso_modositas
        self.ervenyesseg_kezdete = ervenyesseg_kezdete
        self.ervenyesseg_vege = ervenyesseg_vege
        self.tulajdonos = tulajdonos
        self.szervezet = szervezet
