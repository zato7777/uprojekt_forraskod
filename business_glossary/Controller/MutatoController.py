from business_glossary.Service.MutatoService import MutatoService


class MutatoController(object):
    def __init__(self):
        self.mutato_service = MutatoService()

    def mutato_list(self):
        mutato_list, message = self.mutato_service.readAll()
        return mutato_list, message

