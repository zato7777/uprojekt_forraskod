from business_glossary.Service.NomenklaturaService import NomenklaturaService


class NomenklaturaController(object):
    def __init__(self):
        self.nomenklatura_service = NomenklaturaService()

    def nomenklatura_list(self):
        nomenklatura_list, message = self.nomenklatura_service.readAll()
        return nomenklatura_list, message

