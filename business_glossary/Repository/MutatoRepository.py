import pymysql

from Adatszotar.Repository.RepositoryBase import RepositoryBase


class MutatoRepository(RepositoryBase):
    def __init__(self):
        super().__init__("mutato")


    def create(self, mutato):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        sql_query = f"""INSERT INTO {self.table_name} 
        (mutato_csoport, nev, cimke, leiras, hossz, tipus, utolso_modositas, ervenyesseg_kezdete, ervenyesseg_vege, tulajdonos, szervezet) 
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
        data = (mutato.mutato_csoport, mutato.nev, mutato.cimke, mutato.leiras, mutato.hossz,
                mutato.tipus, mutato.utolso_modositas, mutato.ervenyesseg_kezdete, mutato.ervenyesseg_vege,
                mutato.tulajdonos, mutato.szervezet)
        message = "sikeres"

        try:
            cursor.execute(sql_query, data)
        except Exception as e:
            message = "Mutató létrehozása sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message

    def update(self, mutato):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        sql_update_query = f"""UPDATE {self.table_name}
                SET mutato_csoport = %s, cimke = %s, leiras = %s, hossz = %s, tipus = %s, utolso_modositas = %s,
                 ervenyesseg_kezdete = %s, ervenyesseg_vege = %s, tulajdonos = %s, szervezet = %s
                WHERE nev = %s"""
        data = (mutato.mutato_csoport, mutato.cimke, mutato.leiras, mutato.hossz,
                mutato.tipus, mutato.utolso_modositas, mutato.ervenyesseg_kezdete, mutato.ervenyesseg_vege,
                mutato.tulajdonos, mutato.szervezet, mutato.nev)
        message = "sikeres"

        try:
            cursor.execute(sql_update_query, data)
        except Exception as e:
            message = "Mutató módosítása sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message


