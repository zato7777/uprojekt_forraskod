import pymysql

from Adatszotar.Repository.RepositoryBase import RepositoryBase


class NomenklaturaRepository(RepositoryBase):
    def __init__(self):
        super().__init__("nomenklatura")

    def create(self, nomenklatura):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="main", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        sql_query = f"""INSERT INTO {self.table_name} 
        (nev, cimke, leiras, hossz, tipus, utolso_modositas, ervenyesseg_kezdete, ervenyesseg_vege, tulajdonos, szervezet) 
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
        data = (nomenklatura.nev, nomenklatura.cimke, nomenklatura.leiras, nomenklatura.hossz,
                nomenklatura.tipus, nomenklatura.utolso_modositas, nomenklatura.ervenyesseg_kezdete,
                nomenklatura.ervenyesseg_vege, nomenklatura.tulajdonos, nomenklatura.szervezet)
        message = "sikeres"

        try:
            cursor.execute(sql_query, data)
        except Exception as e:
            message = "Nómenklatúra létrehozása sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message

    def update(self, nomenklatura):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="main", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        sql_update_query = f"""UPDATE {self.table_name}
                SET cimke = %s, leiras = %s, hossz = %s, tipus = %s, utolso_modositas = %s, ervenyesseg_kezdete = %s,
                 ervenyesseg_vege = %s, tulajdonos = %s, szervezet = %s
                WHERE nev = %s"""
        data = (nomenklatura.cimke, nomenklatura.leiras, nomenklatura.hossz,
                nomenklatura.tipus, nomenklatura.utolso_modositas, nomenklatura.ervenyesseg_kezdete,
                nomenklatura.ervenyesseg_vege, nomenklatura.tulajdonos, nomenklatura.szervezet, nomenklatura.nev)
        message = "sikeres"

        try:
            cursor.execute(sql_update_query, data)
        except Exception as e:
            message = "Nómenklatúra módosítása sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message