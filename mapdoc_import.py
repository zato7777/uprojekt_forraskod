from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt, QObject
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout, QMainWindow


class UiMapdocImport(QMainWindow):

    def setupUi(self, UiMapdocImport):
        UiMapdocImport.setObjectName("UiMapdocImport")
        UiMapdocImport.setMinimumSize(600, 250)
        UiMapdocImport.setMaximumSize(600, 250)
        self.window = UiMapdocImport
        self.centralwidget = QtWidgets.QWidget(UiMapdocImport)
        self.centralwidget.setObjectName("centralwidget")

        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(0, 40, 600, 200))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.frame.setFont(font)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")

        self.label = QtWidgets.QLabel(self.frame)
        self.label.setGeometry(QtCore.QRect(30, 40, 170, 30))
        self.label.setObjectName("label")

        self.lineAdd_MapdocAdatallomany = QtWidgets.QLineEdit(self.frame)
        self.lineAdd_MapdocAdatallomany.setGeometry(QtCore.QRect(220, 40, 180, 30))
        self.lineAdd_MapdocAdatallomany.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.lineAdd_MapdocAdatallomany.setReadOnly(True)
        self.lineAdd_MapdocAdatallomany.setObjectName("lineAdd_Adatallomany")

        self.pushButton_Tallozas = QtWidgets.QPushButton(self.frame)
        self.pushButton_Tallozas.setGeometry(QtCore.QRect(420, 40, 150, 30))
        self.eleresi_ut = ""
        self.pushButton_Tallozas.setObjectName("pushButton_Tallozas")
        self.pushButton_Tallozas.clicked.connect(lambda: self.adatallomanyEllenorzese())

        self.pushButton_Importalas = QtWidgets.QPushButton(self.frame)
        self.pushButton_Importalas.setGeometry(QtCore.QRect(180, 100, 100, 30))
        self.pushButton_Importalas.setObjectName("pushButton_Importalas")
        self.pushButton_Importalas.clicked.connect(lambda: self.openWindowImportalasEredmenye())

        self.pushButton_Megse = QtWidgets.QPushButton(self.frame)
        self.pushButton_Megse.setGeometry(QtCore.QRect(320, 100, 100, 30))
        self.pushButton_Megse.setObjectName("pushButton_Megse")
        self.pushButton_Megse.clicked.connect(lambda: UiMapdocImport.close())

        self.window.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(self.window)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 600, 30))
        self.menubar.setObjectName("menubar")
        self.window.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(self.window)
        self.statusbar.setObjectName("statusbar")
        self.window.setStatusBar(self.statusbar)
        self.retranslateUi(self.window)
        QtCore.QMetaObject.connectSlotsByName(self.window)

    def retranslateUi(self, UiMapdocImport):
        _translate = QtCore.QCoreApplication.translate
        UiMapdocImport.setWindowTitle(_translate("UiMapdocImport", "Mapdoc import"))
        self.label.setText(_translate("UiMapdocImport", "Mapdoc adatállomány:"))
        self.pushButton_Tallozas.setText(_translate("UiMapdocImport", "Tallózás"))
        self.pushButton_Importalas.setText(_translate("UiMapdocImport", "Importálás"))
        self.pushButton_Megse.setText(_translate("UiMapdocImport", "Mégse"))

    def adatallomanyEllenorzese(self):
        try:
            self.eleresi_ut = QtWidgets.QFileDialog.getOpenFileUrl(caption="Mapdoc adatállomány importálása",
                                                                                filter="Mapdoc adatállomány (*.sql)",
                                                                                initialFilter="Mapdoc adatállomány (*.sql)")
            self.lineAdd_MapdocAdatallomany.setText(self.eleresi_ut[0].fileName())
            self.mapdocAdatallomanytBetolt()
        except Exception as e:
            QtWidgets.QMessageBox.warning(self.window, "Mapdoc adatállomány importálása",
                                          "Hiba a mapdoc adatállomány importálása közben! Próbálja újra!")
            print(e)

    def openWindowImportalasEredmenye(self):
        pass

    def mapdocAdatallomanytBetolt(self):
        pass



