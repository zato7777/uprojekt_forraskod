from Adatszotar.Repository.AdatallomanyRepository import AdatallomanyRepository
from Adatszotar.Service.IService import IService
from Adatszotar.Service.RekordleirasService import RekordleirasService
from Adatszotar.Service.ServiceBase import ServiceBase
from Adatszotar.ViewModel.AdatallomanyViewModel import AdatallomanyViewModel
from Adatszotar.DbModel.AdatallomanyDbModel import AdatallomanyDbModel
from FajlImport.FajlImportService import FajlImportService


class AdatallomanyService(ServiceBase, IService):
    def __init__(self):
        super().__init__(AdatallomanyRepository(), AdatallomanyService)
        self.rekordleiras_service = RekordleirasService()
        self.fajl_import_service = FajlImportService()

    def assembleViewModel(self, db_model):
        #return AdatallomanyViewModel(db_model.id, db_model.nev, db_model.rekordszam, db_model.verzioszam,
        #                             db_model.projekt, db_model.tulajdonos, db_model.titkositasi_kod,
        #                             db_model.letrehozva, db_model.forras, self.rekordleiras_service.getOneById(db_model.rekordleiras_id))
        rekordleiras, message = self.rekordleiras_service.getOneById(db_model[9])
        return AdatallomanyViewModel(db_model[0], db_model[1], db_model[2], db_model[3], db_model[4],
                                     db_model[5], db_model[6], db_model[7],
                                     db_model[8], rekordleiras), message


    def assembleDbModel(self, view_model):
        return AdatallomanyDbModel(view_model.id, view_model.forras, view_model.letrehozva, view_model.nev,
                                   view_model.projekt, view_model.rekordszam, view_model.titkositasi_kod,
                                   view_model.tulajdonos, view_model.verzioszam, view_model.rekordleiras.id)

    def adatallomany_modositas(self, id):
        adatallomany, message = self.repository.getOneById(id)
        adatallomany = list(adatallomany)
        if message == "sikeres":
            adatallomany[5], message = self.fajl_import_service.get_fajl_rekordszam(adatallomany[3])
            if message == "sikeres":
                db_model = AdatallomanyDbModel(adatallomany[0], adatallomany[1], adatallomany[2], adatallomany[3],
                                    adatallomany[4], adatallomany[5], adatallomany[6], adatallomany[7],
                                    adatallomany[8], adatallomany[9])
                message = self.repository.update(db_model)
        return message


