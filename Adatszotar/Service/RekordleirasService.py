from Adatszotar.DbModel.RekordleirasDbModel import RekordleirasDbModel
from Adatszotar.Repository.RekordleirasRepository import RekordleirasRepository
from Adatszotar.Service.IService import IService
from Adatszotar.Service.ServiceBase import ServiceBase
from Adatszotar.ViewModel.RekordleirasViewModel import RekordleirasViewModel


class RekordleirasService(ServiceBase, IService):
    def __init__(self):
        super().__init__(RekordleirasRepository(), RekordleirasService)

    def assembleViewModel(self, db_model):
        message = "sikeres"
        return RekordleirasViewModel(db_model[0], db_model[1], db_model[2], db_model[3], db_model[4],
                                     db_model[5], db_model[6]), message

    def assembleDbModel(self, view_model):
        return RekordleirasDbModel(view_model.id, view_model.nev, view_model.tipus, view_model.cimke, view_model.leiras,
                                   view_model.utolso_modositas, view_model.letrehozva)

    def getOneByName(self, nev):
        db_model, message = self.repository.getOneByName(nev)
        view_model = None
        if message == "sikeres":
            view_model, message = self.service.assembleViewModel(self, db_model)
        return view_model, message

    def generalas(self, rekordleiras_view_model):
        message, rekordleiras_id = self.repository.generalas(self.service.assembleDbModel(self, rekordleiras_view_model))
        return message, rekordleiras_id