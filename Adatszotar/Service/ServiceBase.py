from abc import ABC

from Adatszotar.Service.IService import IService


class ServiceBase(IService, ABC):
    def __init__(self, repository, service):
        self.repository = repository
        self.service = service

    def getOneById(self, id):
        db_model, message = self.repository.getOneById(id)
        view_model = None
        if message == "sikeres":
            view_model, message = self.service.assembleViewModel(self, db_model)
        return view_model, message

    def readAll(self):
        db_models, message = self.repository.readAll()
        view_models = []

        if message == "sikeres":
            for db_model in db_models:
                view_model, message = self.service.assembleViewModel(self, db_model)
                view_models.append(view_model)

        return view_models, message

    def create(self, view_model):
        message = self.repository.create(self.service.assembleDbModel(self, view_model))
        return message

    def update(self, view_model):
        message = self.repository.update(self.service.assembleDbModel(self, view_model))
        return message

    def delete(self, id):
        return self.repository.delete(id)
