from abc import abstractmethod, ABC


class IService(ABC):
    @abstractmethod
    def assembleViewModel(self, db_model):
        pass

    @abstractmethod
    def assembleDbModel(self, view_model):
        pass

    @abstractmethod
    def getOneById(self, id):
        pass

    @abstractmethod
    def readAll(self):
        pass

    @abstractmethod
    def create(self, view_model):
        pass

    @abstractmethod
    def update(self, view_model):
        pass

    @abstractmethod
    def delete(self, id):
        pass
