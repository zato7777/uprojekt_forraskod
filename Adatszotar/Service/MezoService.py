from Adatszotar.DbModel.MezoDbModel import MezoDbModel
from Adatszotar.Repository.MezoRepository import MezoRepository
from Adatszotar.Service.IService import IService
from Adatszotar.Service.RekordleirasService import RekordleirasService
from Adatszotar.Service.ServiceBase import ServiceBase
from Adatszotar.ViewModel.MezoViewModel import MezoViewModel


class MezoService(ServiceBase, IService):
    def __init__(self):
        super().__init__(MezoRepository(), MezoService)
        self.rekordleiras_service = RekordleirasService()

    def assembleViewModel(self, db_model):
        rekordleiras, message = self.rekordleiras_service.getOneById(db_model[5])
        return MezoViewModel(db_model[0], db_model[1], db_model[2], db_model[3], db_model[4],
                             rekordleiras), message

    def assembleDbModel(self, view_model):
        return MezoDbModel(view_model.id, view_model.nev, view_model.adattipus, view_model.leiras,
                                   view_model.business_glossary_nev, view_model.rekordleiras.id)

    def readAllMezoToRekordleiras(self, id):
        db_models, message = self.repository.readAllMezoToRekordleiras(id)
        view_models = []

        if message == "sikeres":
            for db_model in db_models:
                view_model, message = self.assembleViewModel(db_model)
                view_models.append(view_model)

        return view_models, message
