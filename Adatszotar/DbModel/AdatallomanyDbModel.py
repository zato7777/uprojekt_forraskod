class AdatallomanyDbModel(object):
    def __init__(self, id, forras, letrehozva, nev, projekt, rekordszam, titkositasi_kod, tulajdonos, verzioszam, rekordleiras_id):
        self.id = id
        self.forras = forras
        self.letrehozva = letrehozva
        self.nev = nev
        self.projekt = projekt
        self.rekordszam = rekordszam
        self.titkositasi_kod = titkositasi_kod
        self.tulajdonos = tulajdonos
        self.verzioszam = verzioszam
        self.rekordleiras_id = rekordleiras_id
