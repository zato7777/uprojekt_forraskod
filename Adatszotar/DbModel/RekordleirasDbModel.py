class RekordleirasDbModel(object):
    def __init__(self, id, nev, tipus, cimke, leiras, utolso_modositas, letrehozva):
        self.id = id
        self.nev = nev
        self.tipus = tipus
        self.cimke = cimke
        self.leiras = leiras
        self.utolso_modositas = utolso_modositas
        self.letrehozva = letrehozva
