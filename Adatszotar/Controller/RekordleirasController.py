import datetime

from Adatszotar.Service.RekordleirasService import RekordleirasService
from Adatszotar.ViewModel.RekordleirasViewModel import RekordleirasViewModel


class RekordleirasController(object):
    def __init__(self):
        self.rekordleiras_service = RekordleirasService()

    def rekordleiras_list(self):
        rekordleiras_list, message = self.rekordleiras_service.readAll()
        return rekordleiras_list, message

    def rekordleiras_hozzaadas(self, nev, tipus, cimke, leiras):
        current_datetime = datetime.datetime.now()
        rekordleiras_view_model = RekordleirasViewModel(0, nev, tipus, cimke, leiras, current_datetime, current_datetime)
        message = self.rekordleiras_service.create(rekordleiras_view_model)
        return message

    def rekordleiras_generalas(self, nev, tipus):
        current_datetime = datetime.datetime.now()
        rekordleiras_view_model = RekordleirasViewModel(0, nev, tipus, "", "", current_datetime, current_datetime)
        message, rekordleiras_id = self.rekordleiras_service.generalas(rekordleiras_view_model)
        return message, rekordleiras_id

    def rekordleiras_modositas(self, id, nev, tipus, cimke, leiras):
        current_datetime = datetime.datetime.now()
        rekordleiras, message = self.rekordleiras_service.getOneById(id)
        if message == "sikeres":
            rekordleiras_view_model = RekordleirasViewModel(id, nev, tipus, cimke, leiras, current_datetime,
                                                            rekordleiras.letrehozva)
            message = self.rekordleiras_service.update(rekordleiras_view_model)
        return message

    def rekordleiras_getOneById(self, id):
        return self.rekordleiras_service.getOneById(id)

    def rekordleiras_delete(self, id):
        return self.rekordleiras_service.delete(id)
