from Adatszotar.Service.AdatallomanyService import AdatallomanyService
from Adatszotar.Service.RekordleirasService import RekordleirasService
from Adatszotar.ViewModel.AdatallomanyViewModel import AdatallomanyViewModel


class AdatallomanyController(object):
    def __init__(self):
        self.adatallomany_service = AdatallomanyService()
        self.rekordleiras_service = RekordleirasService()

    def adatallomanyt_letrehoz(self, forras, letrehozva, nev, projekt, rekordszam, titkositasi_kod, tulajdonos, verzioszam, rekordleiras_nev):
        rekordleiras, message = self.rekordleiras_service.getOneByName(rekordleiras_nev)
        if message == "sikeres":
            adatallomany_view_model = AdatallomanyViewModel(0, forras, letrehozva, nev, projekt, rekordszam, titkositasi_kod, tulajdonos, verzioszam, rekordleiras)
            message = self.adatallomany_service.create(adatallomany_view_model)
        return message

    def adatszotar_list(self):
        adatallomany_list, message = self.adatallomany_service.readAll()
        return adatallomany_list, message

    def adatszotar_modositas(self, id):
        return self.adatallomany_service.adatallomany_modositas(id)

    def adatallomany_delete(self, id):
        return self.adatallomany_service.delete(id)

