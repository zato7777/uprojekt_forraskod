from Adatszotar.Service.MezoService import MezoService
from Adatszotar.Service.RekordleirasService import RekordleirasService
from Adatszotar.ViewModel.MezoViewModel import MezoViewModel


class MezoController(object):
    def __init__(self):
        self.mezo_service = MezoService()
        self.rekordleiras_service = RekordleirasService()

    def mezokezeles(self, id):
        mezo_list_to_rekordleiras, message = self.mezo_service.readAllMezoToRekordleiras(id)
        return mezo_list_to_rekordleiras, message

    def mezo_hozzaadas(self, nev, adattipus, leiras, business_glossary_nev, rekordleiras_id):
        rekordleiras, message = self.rekordleiras_service.getOneById(rekordleiras_id)
        if message == "sikeres":
            mezo_list_to_rekordleiras, message = self.mezo_service.readAllMezoToRekordleiras(rekordleiras_id)
            if message == "sikeres":
                for mezo in mezo_list_to_rekordleiras:
                    if nev == mezo.nev:
                        message = "Hiba! Nem lehet két azonos nevű mezőt azonos rekordleíráshoz rendelni."
                        return message
                mezo_view_model = MezoViewModel(0, nev, adattipus, leiras, business_glossary_nev, rekordleiras)
                message = self.mezo_service.create(mezo_view_model)
        return message

    def mezo_modositas(self, id, nev, adattipus, leiras, business_glossary_nev, rekordleiras_id):
        rekordleiras, message = self.rekordleiras_service.getOneById(rekordleiras_id)
        if message == "sikeres":
            mezo_list_to_rekordleiras, message = self.mezo_service.readAllMezoToRekordleiras(rekordleiras_id)
            if message == "sikeres":
                for mezo in mezo_list_to_rekordleiras:
                    if id != mezo.id and nev == mezo.nev:
                        message = "Hiba! Nem lehet két azonos nevű mezőt azonos rekordleíráshoz rendelni."
                        return message
                mezo_view_model = MezoViewModel(id, nev, adattipus, leiras, business_glossary_nev, rekordleiras)
                message = self.mezo_service.update(mezo_view_model)
        return message

    def mezo_getOneById(self, id):
        return self.mezo_service.getOneById(id)

    def mezo_delete(self, id):
        return self.mezo_service.delete(id)

    def mezo_generalas(self, df, rekordleiras_id):
        column_info = {}

        for oszlop_nev, oszlop_adattipus in zip(df.columns, df.dtypes):
            if str(oszlop_adattipus) == 'object':
                mysql_adattipus = 'string'
            elif 'int' in str(oszlop_adattipus):
                mysql_adattipus = 'int'
            elif 'float' in str(oszlop_adattipus):
                mysql_adattipus = 'float'
            else:
                mysql_adattipus = 'string'

            column_info[oszlop_nev] = mysql_adattipus

        rekordleiras, message = self.rekordleiras_service.getOneById(rekordleiras_id)

        for oszlop_nev, oszlop_adattipus in column_info.items():
            mezo_view_model = MezoViewModel(0, oszlop_nev, oszlop_adattipus, "", "", rekordleiras)
            message = self.mezo_service.create(mezo_view_model)
            if message != "sikeres":
                return message

        return message
