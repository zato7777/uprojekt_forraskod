from Adatszotar.Controller.AdatallomanyController import AdatallomanyController
from Adatszotar.Controller.MezoController import MezoController
from Adatszotar.Controller.RekordleirasController import RekordleirasController
from PyQt5 import QtCore, QtGui, QtWidgets


from FajlImport.FajlImportController import FajlImportController
from business_glossary.Controller.MutatoController import MutatoController
from business_glossary.Controller.NomenklaturaController import NomenklaturaController


class UiDataDictionary(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.adatallomany_controller = AdatallomanyController()
        self.selected_adatallomany_id = None
        self.selected_adatallomany_nev = None
        self.adatallomany_rekordleirasa = None
        self.fajl_import_controller = FajlImportController()

    def setupUi(self, UiDataDictionary):
        adatallomany_list, message = self.adatallomany_controller.adatszotar_list()
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            adatallomanyok_szama = len(adatallomany_list)

            UiDataDictionary.setObjectName("UiDataDictionary")
            UiDataDictionary.resize(1600, 800)
            UiDataDictionary.setMinimumSize(1600, 800)
            UiDataDictionary.setMaximumSize(1600, 800)
            self.window = UiDataDictionary
            self.centralwidget = QtWidgets.QWidget(self.window)
            self.centralwidget.setObjectName("centralwidget")

            self.frame = QtWidgets.QFrame(self.centralwidget)
            self.frame.setGeometry(QtCore.QRect(10, 10, 1500, 800))
            font = QtGui.QFont()
            font.setPointSize(12)
            self.frame.setFont(font)
            self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
            self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
            self.frame.setObjectName("frame")

            self.tableWidget = QtWidgets.QTableWidget(self.frame)
            self.tableWidget.setGeometry(QtCore.QRect(10, 100, 1400, 600))
            self.tableWidget.setObjectName("tableWidget")
            self.tableWidget.horizontalHeader().setStretchLastSection(True)
            self.tableWidget.setColumnCount(9)
            self.tableWidget.setRowCount(adatallomanyok_szama)

            self.pushButton_adatok = QtWidgets.QPushButton(self.frame)
            self.pushButton_adatok.setGeometry(QtCore.QRect(20, 40, 200, 30))
            self.pushButton_adatok.setObjectName("pushButton_adatok")
            self.pushButton_rekordleiras = QtWidgets.QPushButton(self.frame)
            self.pushButton_rekordleiras.setGeometry(QtCore.QRect(250, 40, 200, 30))
            self.pushButton_rekordleiras.setObjectName("pushButton_rekordleiras")
            self.pushButton_torles = QtWidgets.QPushButton(self.frame)
            self.pushButton_torles.setGeometry(QtCore.QRect(480, 40, 200, 30))
            self.pushButton_torles.setObjectName("pushButton_torles")

            UiDataDictionary.setCentralWidget(self.centralwidget)
            self.menubar = QtWidgets.QMenuBar(UiDataDictionary)
            self.menubar.setGeometry(QtCore.QRect(0, 0, 900, 21))
            self.menubar.setObjectName("menubar")
            UiDataDictionary.setMenuBar(self.menubar)
            self.statusbar = QtWidgets.QStatusBar(UiDataDictionary)
            self.statusbar.setObjectName("statusbar")
            UiDataDictionary.setStatusBar(self.statusbar)

            #fuggoleges headers
            for i in range(0, adatallomanyok_szama):
                item = QtWidgets.QTableWidgetItem()
                self.tableWidget.setVerticalHeaderItem(i, item)

            #vizszintes headers
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(0, item)
            self.tableWidget.setColumnWidth(0, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(1, item)
            self.tableWidget.setColumnWidth(1, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(2, item)
            self.tableWidget.setColumnWidth(2, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(3, item)
            self.tableWidget.setColumnWidth(3, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(4, item)
            self.tableWidget.setColumnWidth(4, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(5, item)
            self.tableWidget.setColumnWidth(5, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(6, item)
            self.tableWidget.setColumnWidth(6, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(7, item)
            self.tableWidget.setColumnWidth(7, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(8, item)
            self.tableWidget.setColumnWidth(8, 150)

            #innentol a rekordok kilistazva
            for i in range(0, adatallomanyok_szama):
                for j in range(0, 9):
                    item = QtWidgets.QTableWidgetItem()
                    self.tableWidget.setItem(i, j, item)

            self.pushButton_adatok.clicked.connect(self.open_adatok)
            self.pushButton_rekordleiras.clicked.connect(self.open_rekordleiras)
            self.pushButton_torles.clicked.connect(self.open_delete)

            self.retranslateUi(UiDataDictionary)
            QtCore.QMetaObject.connectSlotsByName(UiDataDictionary)

            self.tableWidget.setSelectionBehavior(QtWidgets.QTableWidget.SelectRows)
            self.tableWidget.selectionModel().selectionChanged.connect(
                self.on_selection_changed
            )

            self.on_selection_changed()

    def on_selection_changed(self):
        self.pushButton_adatok.setEnabled(
            bool(self.tableWidget.selectionModel().selectedRows())
        )
        self.pushButton_rekordleiras.setEnabled(
            bool(self.tableWidget.selectionModel().selectedRows())
        )
        self.pushButton_torles.setEnabled(
            bool(self.tableWidget.selectionModel().selectedRows())
        )

    def retranslateUi(self, UiDataDictionary):
        _translate = QtCore.QCoreApplication.translate
        UiDataDictionary.setWindowTitle(_translate("UiDataDictionary", "Adatszótár"))

        adatallomany_list, message = self.adatallomany_controller.adatszotar_list()
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            adatallomanyok_szama = len(adatallomany_list)
            for i in range(0, adatallomanyok_szama):
                item = self.tableWidget.verticalHeaderItem(i)
                item.setText(_translate("UiDataDictionary", str(i+1)))

            item = self.tableWidget.horizontalHeaderItem(0)
            item.setText(_translate("UiDataDictionary", "Adatállomány neve"))
            item = self.tableWidget.horizontalHeaderItem(1)
            item.setText(_translate("UiDataDictionary", "Rekordszám"))
            item = self.tableWidget.horizontalHeaderItem(2)
            item.setText(_translate("UiDataDictionary", "Rekordleírása"))
            item = self.tableWidget.horizontalHeaderItem(3)
            item.setText(_translate("UiDataDictionary", "Verziószáma"))
            item = self.tableWidget.horizontalHeaderItem(4)
            item.setText(_translate("UiDataDictionary", "Projekt"))
            item = self.tableWidget.horizontalHeaderItem(5)
            item.setText(_translate("UiDataDictionary", "Tulajdonos"))
            item = self.tableWidget.horizontalHeaderItem(6)
            item.setText(_translate("UiDataDictionary", "Titkosítási kód"))
            item = self.tableWidget.horizontalHeaderItem(7)
            item.setText(_translate("UiDataDictionary", "Létrehozva"))
            item = self.tableWidget.horizontalHeaderItem(8)
            item.setText(_translate("UiDataDictionary", "Forrás"))

            __sortingEnabled = self.tableWidget.isSortingEnabled()
            self.tableWidget.setSortingEnabled(False)

            for i in range(0, adatallomanyok_szama):
                item = self.tableWidget.item(i, 0)
                item.setText(_translate("UiDataDictionary", str(adatallomany_list[i].nev)))
                item = self.tableWidget.item(i, 1)
                item.setText(_translate("UiDataDictionary", str(adatallomany_list[i].rekordszam)))
                item = self.tableWidget.item(i, 2)
                item.setText(_translate("UiDataDictionary", str(adatallomany_list[i].rekordleiras.nev)))
                item = self.tableWidget.item(i, 3)
                item.setText(_translate("UiDataDictionary", str(adatallomany_list[i].verzioszam)))
                item = self.tableWidget.item(i, 4)
                item.setText(_translate("UiDataDictionary", str(adatallomany_list[i].projekt)))
                item = self.tableWidget.item(i, 5)
                item.setText(_translate("UiDataDictionary", str(adatallomany_list[i].tulajdonos)))
                item = self.tableWidget.item(i, 6)
                item.setText(_translate("UiDataDictionary", str(adatallomany_list[i].titkositasi_kod)))
                item = self.tableWidget.item(i, 7)
                item.setText(_translate("UiDataDictionary", str(adatallomany_list[i].letrehozva)))
                item = self.tableWidget.item(i, 8)
                item.setText(_translate("UiDataDictionary", str(adatallomany_list[i].forras)))

            self.tableWidget.setSortingEnabled(__sortingEnabled)
            self.pushButton_adatok.setText(_translate("UiDataDictionary", "Adatok megtekintése"))
            self.pushButton_rekordleiras.setText(_translate("UiDataDictionary", "Rekordleírások"))
            self.pushButton_torles.setText(_translate("UiDataDictionary", "Törlés"))

    def open_adatok(self):
        row = None
        indexes = self.tableWidget.selectionModel().selectedRows()
        if len(indexes) == 1:
            row = indexes[0].row()

        adatallomany_list, message = self.adatallomany_controller.adatszotar_list()

        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            adatallomanyok_szama = len(adatallomany_list)
            for i in range(0, adatallomanyok_szama):
                if i == row:
                    self.selected_adatallomany_id = adatallomany_list[i].id
                    self.selected_adatallomany_nev = adatallomany_list[i].nev
            if self.selected_adatallomany_id is not None and self.selected_adatallomany_nev is not None:
                self.ui = UiAdatallomanyAdatok(self.selected_adatallomany_id, self.selected_adatallomany_nev)
                if message != "sikeres":
                    QtWidgets.QMessageBox.warning(self.window, "Hiba", message)

            self.window.close()
            self.window = QtWidgets.QMainWindow()
            self.ui.setupUi(self.window)
            self.window.show()

    def open_rekordleiras(self):
        self.window.close()
        row = None
        indexes = self.tableWidget.selectionModel().selectedRows()
        if len(indexes) == 1:
            row = indexes[0].row()
        adatallomany_list, message = self.adatallomany_controller.adatszotar_list()
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            for i in range(0, len(adatallomany_list)):
                if i == row:
                    self.selected_adatallomany_id = adatallomany_list[i].id
                    self.adatallomany_rekordleirasa = adatallomany_list[i].rekordleiras

            self.window = QtWidgets.QMainWindow()
            if self.selected_adatallomany_id is not None:
                self.ui = UiRekordleiras(self.adatallomany_rekordleirasa)
            else:
                pass
            self.ui.setupUi(self.window)
            self.window.show()

    def open_delete(self):
        result = QtWidgets.QMessageBox.question(None,
                                                "Törlés megerősítése...",
                                                "Biztos ki akarod törölni a kiválaszott sort?",
                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)

        if result == QtWidgets.QMessageBox.Yes:
            row = None
            indexes = self.tableWidget.selectionModel().selectedRows()
            if len(indexes) == 1:
                row = indexes[0].row()

            adatallomany_list, message = self.adatallomany_controller.adatszotar_list()

            if message != "sikeres":
                (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
            else:
                adatallomanyok_szama = len(adatallomany_list)
                for i in range(0, adatallomanyok_szama):
                    if i == row:
                        self.selected_adatallomany_id = adatallomany_list[i].id
                        self.selected_adatallomany_nev = adatallomany_list[i].nev
                if self.selected_adatallomany_id is not None:
                    message = self.adatallomany_controller.adatallomany_delete(self.selected_adatallomany_id)
                    if message != "sikeres":
                        QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
                else:
                    pass

                message = self.fajl_import_controller.adatallomanyt_torol(self.selected_adatallomany_nev)

                if message != "sikeres":
                    QtWidgets.QMessageBox.warning(self.window, "Hiba", message)

                self.window.close()
                self.window = QtWidgets.QMainWindow()
                self.ui = UiDataDictionary()
                self.ui.setupUi(self.window)
                self.window.show()

class UiAdatallomanyAdatok(object):
    def __init__(self, selected_adatallomany_id, selected_adatallomany_nev):
        self.selected_adatallomany_id = selected_adatallomany_id
        self.selected_adatallomany_nev = selected_adatallomany_nev
        self.adatallomany_controller = AdatallomanyController()
        self.fajl_import_controller = FajlImportController()
        self.mezok_szama, message = self.fajl_import_controller.get_fajl_mezoszam(self.selected_adatallomany_nev)
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
            self.window.close()
            self.window = QtWidgets.QMainWindow()
            self.ui = UiDataDictionary()
            self.ui.setupUi(self.window)
            self.window.show()
        self.rekordok_szama = 0
        self.selected_rekord_id = None

    def setupUi(self, UiAdatallomanyAdatok):
        UiAdatallomanyAdatok.setObjectName("UiAdatallomanyAdatok")
        UiAdatallomanyAdatok.resize(200 + (self.mezok_szama - 1) * 150, 800)
        UiAdatallomanyAdatok.setMinimumSize(200 + (self.mezok_szama - 1) * 150, 800)
        UiAdatallomanyAdatok.setMaximumSize(200 + (self.mezok_szama - 1) * 150, 800)
        self.window = UiAdatallomanyAdatok
        self.centralwidget = QtWidgets.QWidget(self.window)
        self.centralwidget.setObjectName("centralwidget")

        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(10, 10, 210 + (self.mezok_szama - 1) * 140, 800))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.frame.setFont(font)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")


        self.rekordok_szama, message = self.fajl_import_controller.get_fajl_rekordszam(self.selected_adatallomany_nev)

        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
            self.window.close()
            self.window = QtWidgets.QMainWindow()
            self.ui = UiDataDictionary()
            self.ui.setupUi(self.window)
            self.window.show()

        self.tableWidget = QtWidgets.QTableWidget(self.frame)
        self.tableWidget.setGeometry(QtCore.QRect(10, 100, 200 + (self.mezok_szama - 1) * 140, 600))
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.setColumnCount(self.mezok_szama - 1)
        self.tableWidget.setRowCount(self.rekordok_szama)


        self.pushButton_hozzaadas = QtWidgets.QPushButton(self.frame)
        self.pushButton_hozzaadas.setGeometry(QtCore.QRect(20, 40, 200, 30))
        self.pushButton_hozzaadas.setObjectName("pushButton_hozzaadas")
        self.pushButton_modositas = QtWidgets.QPushButton(self.frame)
        self.pushButton_modositas.setGeometry(QtCore.QRect(250, 40, 200, 30))
        self.pushButton_modositas.setObjectName("pushButton_modositas")
        self.pushButton_torles = QtWidgets.QPushButton(self.frame)
        self.pushButton_torles.setGeometry(QtCore.QRect(480, 40, 200, 30))
        self.pushButton_torles.setObjectName("pushButton_torles")

        UiAdatallomanyAdatok.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(UiAdatallomanyAdatok)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 900, 21))
        self.menubar.setObjectName("menubar")
        UiAdatallomanyAdatok.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(UiAdatallomanyAdatok)
        self.statusbar.setObjectName("statusbar")
        UiAdatallomanyAdatok.setStatusBar(self.statusbar)

        # fuggoleges headers
        for i in range(0, self.rekordok_szama):
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setVerticalHeaderItem(i, item)

        # vizszintes headers
        for i in range(0, self.mezok_szama - 1):
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(i, item)
            self.tableWidget.setColumnWidth(i, 140)

        # innentol a rekordok kilistazva
        for i in range(0, self.rekordok_szama):
            for j in range(0, self.mezok_szama - 1):
                item = QtWidgets.QTableWidgetItem()
                self.tableWidget.setItem(i, j, item)


        self.pushButton_hozzaadas.clicked.connect(self.open_hozzaadas)
        self.pushButton_modositas.clicked.connect(self.open_modositas)
        self.pushButton_torles.clicked.connect(self.open_delete)

        self.retranslateUi(UiAdatallomanyAdatok)
        QtCore.QMetaObject.connectSlotsByName(UiAdatallomanyAdatok)

        self.tableWidget.setSelectionBehavior(QtWidgets.QTableWidget.SelectRows)
        self.tableWidget.selectionModel().selectionChanged.connect(
            self.on_selection_changed
        )

        self.on_selection_changed()

    def on_selection_changed(self):
        self.pushButton_modositas.setEnabled(
            bool(self.tableWidget.selectionModel().selectedRows())
        )
        self.pushButton_torles.setEnabled(
            bool(self.tableWidget.selectionModel().selectedRows())
        )


    def retranslateUi(self, UiAdatallomanyAdatok):
        _translate = QtCore.QCoreApplication.translate
        UiAdatallomanyAdatok.setWindowTitle(_translate("UiAdatallomanyAdatok", "Adatok megtekintése"))

        rekord_list, message = self.fajl_import_controller.rekord_list(self.selected_adatallomany_nev)
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            for i in range(0, self.rekordok_szama):
                item = self.tableWidget.verticalHeaderItem(i)
                item.setText(_translate("UiAdatallomanyAdatok", str(i + 1)))

            header_names, message = self.fajl_import_controller.get_file_header_names(self.selected_adatallomany_nev)

            for i in range(0, self.mezok_szama - 1):
                item = self.tableWidget.horizontalHeaderItem(i)
                item.setText(_translate("UiAdatallomanyAdatok", header_names[i + 1]))

            __sortingEnabled = self.tableWidget.isSortingEnabled()
            self.tableWidget.setSortingEnabled(False)

            for i in range(0, self.rekordok_szama):
                for j in range(1, self.mezok_szama):
                    item = self.tableWidget.item(i, j - 1)
                    item.setText(_translate("UiAdatallomanyAdatok", str(rekord_list[i][j])))



        self.pushButton_hozzaadas.setText(_translate("UiAdatallomanyAdatok", "Rekord hozzáadása"))
        self.pushButton_modositas.setText(_translate("UiAdatallomanyAdatok", "Rekord módosítása"))
        self.pushButton_torles.setText(_translate("UiAdatallomanyAdatok", "Törlés"))

    def open_hozzaadas(self):
        self.window.close()
        self.window = QtWidgets.QMainWindow()
        self.ui = UiRekordHozzaadas(self.selected_adatallomany_id, self.selected_adatallomany_nev)
        self.ui.setupUi(self.window)
        self.window.show()

    def open_modositas(self):
        self.window.close()
        row = None
        indexes = self.tableWidget.selectionModel().selectedRows()
        if len(indexes) == 1:
            row = indexes[0].row()
        rekord_list, message = self.fajl_import_controller.rekord_list(self.selected_adatallomany_nev)
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            for i in range(0, self.rekordok_szama):
                if i == row:
                    self.selected_rekord_id = rekord_list[i][0]

            self.window = QtWidgets.QMainWindow()
            if self.selected_rekord_id is not None:
                self.ui = UiRekordModositas(self.selected_adatallomany_id, self.selected_adatallomany_nev, self.selected_rekord_id)
            else:
                pass
            self.ui.setupUi(self.window)
            self.window.show()

    def open_delete(self):
        result = QtWidgets.QMessageBox.question(None,
                                                "Törlés megerősítése...",
                                                "Biztos ki akarod törölni a kiválaszott sort?",
                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)

        if result == QtWidgets.QMessageBox.Yes:
            row = None
            indexes = self.tableWidget.selectionModel().selectedRows()
            if len(indexes) == 1:
                row = indexes[0].row()
            rekord_list, message = self.fajl_import_controller.rekord_list(self.selected_adatallomany_nev)
            if message != "sikeres":
                (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
            else:
                for i in range(0, self.rekordok_szama):
                    if i == row:
                        self.selected_rekord_id = rekord_list[i][0]

                if self.selected_rekord_id is not None:
                    message = self.fajl_import_controller.delete_rekord(self.selected_adatallomany_nev, self.selected_rekord_id)
                    if message != "sikeres":
                        (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))

                message = self.adatallomany_controller.adatszotar_modositas(self.selected_adatallomany_id)
                if message != "sikeres":
                    (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
                self.window.close()
                self.window = QtWidgets.QMainWindow()
                self.ui = UiAdatallomanyAdatok(self.selected_adatallomany_id, self.selected_adatallomany_nev)
                self.ui.setupUi(self.window)
                self.window.show()

class UiRekordHozzaadas(object):
    def __init__(self, selected_adatallomany_id, selected_adatallomany_nev):
        self.selected_adatallomany_id = selected_adatallomany_id
        self.selected_adatallomany_nev = selected_adatallomany_nev
        self.fajl_import_controller = FajlImportController()
        self.mezok_szama, message = self.fajl_import_controller.get_fajl_mezoszam(self.selected_adatallomany_nev)
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        self.lineEdit_rekord_list = []
        self.label_list = []
        self.adatallomany_controller = AdatallomanyController()

    def setupUi(self, UiRekordHozzaadas):
        UiRekordHozzaadas.setObjectName("UiRekordHozzaadas")
        UiRekordHozzaadas.resize(600, 200 + (self.mezok_szama - 1) * 50)
        UiRekordHozzaadas.setMinimumSize(600, 200 + (self.mezok_szama - 1) * 50)
        UiRekordHozzaadas.setMaximumSize(600, 200 + (self.mezok_szama - 1) * 50)
        self.window = UiRekordHozzaadas
        self.centralwidget = QtWidgets.QWidget(self.window)
        self.centralwidget.setObjectName("centralwidget")

        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(10, 10, 500, 50 + (self.mezok_szama - 1) * 50))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.tabWidget.setFont(font)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_attributum = QtWidgets.QWidget()
        self.tab_attributum.setObjectName("tab_attributum")

        for i in range(1, self.mezok_szama):
            self.lineEdit_rekord_list.append(QtWidgets.QLineEdit(self.tab_attributum))
            self.lineEdit_rekord_list[i - 1].setGeometry(QtCore.QRect(170, 30 + (i - 1) * 50, 170, 20))
            objectName = "lineEdit_rekord_list" + str(i)
            self.lineEdit_rekord_list[i - 1].setObjectName(objectName)
            self.label_list.append(QtWidgets.QLabel(self.tab_attributum))
            self.label_list[i - 1].setGeometry(QtCore.QRect(10, 30 + (i - 1) * 50, 150, 20))
            self.label_list[i - 1].setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
            objectName = "label_list" + str(i)
            self.label_list[i - 1].setObjectName(objectName)

        self.tabWidget.addTab(self.tab_attributum, "")

        self.pushButton_Megse = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Megse.setGeometry(QtCore.QRect(310, 100 + (self.mezok_szama - 1) * 50, 90, 40))
        self.pushButton_Megse.setFont(font)
        self.pushButton_Megse.clicked.connect(lambda: self.closeRekordHozzaadas())
        self.pushButton_Megse.setDefault(True)
        self.pushButton_Megse.setObjectName("pushButton_Megse")
        self.pushButton_Hozzaad = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Hozzaad.setGeometry(QtCore.QRect(210, 100 + (self.mezok_szama - 1) * 50, 90, 40))
        self.pushButton_Hozzaad.setFont(font)
        self.pushButton_Hozzaad.clicked.connect(lambda: self.rekord_hozzaad())
        self.pushButton_Hozzaad.setObjectName("pushButton_Hozzaad")
        UiRekordHozzaadas.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(UiRekordHozzaadas)
        self.statusbar.setObjectName("statusbar")
        UiRekordHozzaadas.setStatusBar(self.statusbar)

        self.retranslateUi(UiRekordHozzaadas)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(UiRekordHozzaadas)

    def retranslateUi(self, UiRekordHozzaadas):
        _translate = QtCore.QCoreApplication.translate
        UiRekordHozzaadas.setWindowTitle(_translate("UiRekordHozzaadas", "Rekord hozzáadás"))

        header_names, message = self.fajl_import_controller.get_file_header_names(self.selected_adatallomany_nev)
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)

        for i in range(1, self.mezok_szama):
            self.label_list[i - 1].setText(_translate("UiRekordHozzaadas", header_names[i] + ":"))

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_attributum),
                                  _translate("UiRekordHozzaadas", "Attribútumok"))
        self.pushButton_Megse.setText(_translate("UiRekordHozzaadas", "Mégse"))
        self.pushButton_Hozzaad.setText(_translate("UiRekordHozzaadas", "Hozzáad"))

    def rekord_hozzaad(self):
        rekord_ertekek = []

        for i in range(0, self.mezok_szama - 1):
            rekord_ertekek.append(self.lineEdit_rekord_list[i].text())

        message = self.fajl_import_controller.create_rekord(self.selected_adatallomany_nev, rekord_ertekek)


        if message == "sikeres":
            self.adatallomany_controller.adatszotar_modositas(self.selected_adatallomany_id)
            self.window.close()
            self.window = QtWidgets.QMainWindow()
            self.ui = UiAdatallomanyAdatok(self.selected_adatallomany_id, self.selected_adatallomany_nev)
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)

    def closeRekordHozzaadas(self):
        self.window.close()
        self.window = QtWidgets.QMainWindow()
        self.ui = UiAdatallomanyAdatok(self.selected_adatallomany_id, self.selected_adatallomany_nev)
        self.ui.setupUi(self.window)
        self.window.show()


class UiRekordModositas(object):
    def __init__(self, selected_adatallomany_id, selected_adatallomany_nev, selected_rekord_id):
        self.selected_adatallomany_id = selected_adatallomany_id
        self.selected_adatallomany_nev = selected_adatallomany_nev
        self.selected_rekord_id = selected_rekord_id
        self.fajl_import_controller = FajlImportController()
        self.mezok_szama, message = self.fajl_import_controller.get_fajl_mezoszam(self.selected_adatallomany_nev)
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        self.lineEdit_rekord_list = []
        self.label_list = []
        self.adatallomany_controller = AdatallomanyController()

    def setupUi(self, UiRekordModositas):
        UiRekordModositas.setObjectName("UiRekordModositas")
        UiRekordModositas.resize(600, 200 + (self.mezok_szama - 1) * 50)
        UiRekordModositas.setMinimumSize(600, 200 + (self.mezok_szama - 1) * 50)
        UiRekordModositas.setMaximumSize(600, 200 + (self.mezok_szama - 1) * 50)
        self.window = UiRekordModositas
        self.centralwidget = QtWidgets.QWidget(self.window)
        self.centralwidget.setObjectName("centralwidget")

        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(10, 10, 500, 50 + (self.mezok_szama - 1) * 50))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.tabWidget.setFont(font)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_attributum = QtWidgets.QWidget()
        self.tab_attributum.setObjectName("tab_attributum")

        for i in range(1, self.mezok_szama):
            self.lineEdit_rekord_list.append(QtWidgets.QLineEdit(self.tab_attributum))
            self.lineEdit_rekord_list[i - 1].setGeometry(QtCore.QRect(170, 30 + 50 * (i - 1), 170, 20))
            objectName = "lineEdit_rekord_list" + str(i)
            self.lineEdit_rekord_list[i - 1].setObjectName(objectName)
            self.label_list.append(QtWidgets.QLabel(self.tab_attributum))
            self.label_list[i - 1].setGeometry(QtCore.QRect(10, 30 + 50 * (i - 1), 150, 20))
            self.label_list[i - 1].setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
            objectName = "label_list" + str(i)
            self.label_list[i - 1].setObjectName(objectName)

        self.tabWidget.addTab(self.tab_attributum, "")

        self.pushButton_Megse = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Megse.setGeometry(QtCore.QRect(310, 100 + (self.mezok_szama - 1) * 50, 90, 40))
        self.pushButton_Megse.setFont(font)
        self.pushButton_Megse.clicked.connect(lambda: self.closeRekordModositas())
        self.pushButton_Megse.setDefault(True)
        self.pushButton_Megse.setObjectName("pushButton_Megse")
        self.pushButton_Modositas = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Modositas.setGeometry(QtCore.QRect(210, 100 + (self.mezok_szama - 1) * 50, 90, 40))
        self.pushButton_Modositas.setFont(font)
        self.pushButton_Modositas.clicked.connect(lambda: self.rekord_modositas())
        self.pushButton_Modositas.setObjectName("pushButton_Hozzaad")
        UiRekordModositas.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(UiRekordModositas)
        self.statusbar.setObjectName("statusbar")
        UiRekordModositas.setStatusBar(self.statusbar)

        self.retranslateUi(UiRekordModositas)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(UiRekordModositas)

    def retranslateUi(self, UiRekordModositas):
        selected_rekord, message = self.fajl_import_controller.rekord_getOneById(self.selected_adatallomany_nev, self.selected_rekord_id)
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
            self.window.close()
            self.window = QtWidgets.QMainWindow()
            self.ui = UiAdatallomanyAdatok(self.selected_adatallomany_id, self.selected_adatallomany_nev)
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            _translate = QtCore.QCoreApplication.translate
            UiRekordModositas.setWindowTitle(_translate("UiRekordModositas", "Rekord módosítása"))

            header_names, message = self.fajl_import_controller.get_file_header_names(self.selected_adatallomany_nev)
            if message != "sikeres":
                QtWidgets.QMessageBox.warning(self.window, "Hiba", message)

            for i in range(1, self.mezok_szama):
                self.label_list[i - 1].setText(_translate("UiRekordModositas", header_names[i] + ":"))
                item = self.lineEdit_rekord_list[i - 1]
                item.setText(_translate("UiRekordModositas", str(selected_rekord[i])))

            self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_attributum),
                                      _translate("UiRekordModositas", "Attribútumok"))
            self.pushButton_Megse.setText(_translate("UiRekordModositas", "Mégse"))
            self.pushButton_Modositas.setText(_translate("UiRekordModositas", "Módosítás"))

    def rekord_modositas(self):
        rekord_ertekek = []

        for i in range(0, self.mezok_szama - 1):
            rekord_ertekek.append(self.lineEdit_rekord_list[i].text())

        message = self.fajl_import_controller.update_rekord(self.selected_adatallomany_nev, self.selected_rekord_id, rekord_ertekek)
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))

        message = self.adatallomany_controller.adatszotar_modositas(self.selected_adatallomany_id)
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))

        self.window.close()
        self.window = QtWidgets.QMainWindow()
        self.ui = UiAdatallomanyAdatok(self.selected_adatallomany_id, self.selected_adatallomany_nev)
        self.ui.setupUi(self.window)
        self.window.show()

    def closeRekordModositas(self):
        self.window.close()
        self.window = QtWidgets.QMainWindow()
        self.ui = UiAdatallomanyAdatok(self.selected_adatallomany_id, self.selected_adatallomany_nev)
        self.ui.setupUi(self.window)
        self.window.show()


class UiRekordleiras(object):
    def __init__(self, selected_rekordleiras):
        self.rekordleiras_controller = RekordleirasController()
        self.selected_rekordleiras_id = None
        self.selected_rekordleiras = selected_rekordleiras

    def setupUi(self, UiRekordleiras):
        rekordleiras_list, message = self.rekordleiras_controller.rekordleiras_list()
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            rekordleirasok_szama = len(rekordleiras_list)

            UiRekordleiras.setObjectName("UiRekordleiras")
            UiRekordleiras.resize(1100, 600)
            UiRekordleiras.setMinimumSize(1100, 600)
            UiRekordleiras.setMaximumSize(1100, 600)
            self.window = UiRekordleiras
            self.centralwidget = QtWidgets.QWidget(self.window)
            self.centralwidget.setObjectName("centralwidget")

            self.frame = QtWidgets.QFrame(self.centralwidget)
            self.frame.setGeometry(QtCore.QRect(10, 10, 1000, 580))
            font = QtGui.QFont()
            font.setPointSize(12)
            self.frame.setFont(font)
            self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
            self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
            self.frame.setObjectName("frame")

            self.tableWidget = QtWidgets.QTableWidget(self.frame)
            self.tableWidget.setGeometry(QtCore.QRect(10, 100, 950, 400))
            self.tableWidget.setObjectName("tableWidget")
            self.tableWidget.horizontalHeader().setStretchLastSection(True)
            self.tableWidget.setColumnCount(6)
            self.tableWidget.setRowCount(rekordleirasok_szama)

            self.pushButton_hozzaadas = QtWidgets.QPushButton(self.frame)
            self.pushButton_hozzaadas.setGeometry(QtCore.QRect(20, 40, 200, 30))
            self.pushButton_hozzaadas.setObjectName("pushButton_hozzaadas")
            self.pushButton_modositas = QtWidgets.QPushButton(self.frame)
            self.pushButton_modositas.setGeometry(QtCore.QRect(250, 40, 200, 30))
            self.pushButton_modositas.setObjectName("pushButton_modositas")
            self.pushButton_torles = QtWidgets.QPushButton(self.frame)
            self.pushButton_torles.setGeometry(QtCore.QRect(480, 40, 200, 30))
            self.pushButton_torles.setObjectName("pushButton_torles")
            self.pushButton_mezokezeles = QtWidgets.QPushButton(self.frame)
            self.pushButton_mezokezeles.setGeometry(QtCore.QRect(710, 40, 200, 30))
            self.pushButton_mezokezeles.setObjectName("pushButton_mezokezeles")

            UiRekordleiras.setCentralWidget(self.centralwidget)
            self.menubar = QtWidgets.QMenuBar(UiRekordleiras)
            self.menubar.setGeometry(QtCore.QRect(0, 0, 900, 21))
            self.menubar.setObjectName("menubar")
            UiRekordleiras.setMenuBar(self.menubar)
            self.statusbar = QtWidgets.QStatusBar(UiRekordleiras)
            self.statusbar.setObjectName("statusbar")
            UiRekordleiras.setStatusBar(self.statusbar)

            for i in range(0, rekordleirasok_szama):
                item = QtWidgets.QTableWidgetItem()
                self.tableWidget.setVerticalHeaderItem(i, item)

            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(0, item)
            self.tableWidget.setColumnWidth(0, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(1, item)
            self.tableWidget.setColumnWidth(1, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(2, item)
            self.tableWidget.setColumnWidth(2, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(3, item)
            self.tableWidget.setColumnWidth(3, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(4, item)
            self.tableWidget.setColumnWidth(4, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(5, item)
            self.tableWidget.setColumnWidth(5, 150)

            for i in range(0, rekordleirasok_szama):
                for j in range(0, 6):
                    item = QtWidgets.QTableWidgetItem()
                    self.tableWidget.setItem(i, j, item)

            self.pushButton_hozzaadas.clicked.connect(self.open_hozzaad)
            self.pushButton_modositas.clicked.connect(self.open_modosit)
            self.pushButton_torles.clicked.connect(self.open_delete)
            self.pushButton_mezokezeles.clicked.connect(self.open_mezokezeles)

            self.retranslateUi(UiRekordleiras)
            QtCore.QMetaObject.connectSlotsByName(UiRekordleiras)

            self.tableWidget.setSelectionBehavior(QtWidgets.QTableWidget.SelectRows)
            self.tableWidget.selectionModel().selectionChanged.connect(
                self.on_selection_changed
            )

            self.on_selection_changed()

    def on_selection_changed(self):
        self.pushButton_modositas.setEnabled(
            bool(self.tableWidget.selectionModel().selectedRows())
        )
        self.pushButton_torles.setEnabled(
            bool(self.tableWidget.selectionModel().selectedRows())
        )
        self.pushButton_mezokezeles.setEnabled(
            bool(self.tableWidget.selectionModel().selectedRows())
        )

    def retranslateUi(self, UiRekordleiras):
        _translate = QtCore.QCoreApplication.translate
        UiRekordleiras.setWindowTitle(_translate("UiRekordleiras", "Rekordleírás"))

        rekordleiras_list, message = self.rekordleiras_controller.rekordleiras_list()
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            rekordleirasok_szama = len(rekordleiras_list)
            for i in range(0, rekordleirasok_szama):
                item = self.tableWidget.verticalHeaderItem(i)
                item.setText(_translate("UiRekordleiras", str(i + 1)))

            item = self.tableWidget.horizontalHeaderItem(0)
            item.setText(_translate("UiRekordleiras", "Rekordleírás neve"))
            item = self.tableWidget.horizontalHeaderItem(1)
            item.setText(_translate("UiRekordleiras", "Típus"))
            item = self.tableWidget.horizontalHeaderItem(2)
            item.setText(_translate("UiRekordleiras", "Címke"))
            item = self.tableWidget.horizontalHeaderItem(3)
            item.setText(_translate("UiRekordleiras", "Leírás"))
            item = self.tableWidget.horizontalHeaderItem(4)
            item.setText(_translate("UiRekordleiras", "Utolsó módosítás"))
            item = self.tableWidget.horizontalHeaderItem(5)
            item.setText(_translate("UiRekordleiras", "Létrehozva"))

            __sortingEnabled = self.tableWidget.isSortingEnabled()
            self.tableWidget.setSortingEnabled(False)

            for i in range(0, rekordleirasok_szama):
                item = self.tableWidget.item(i, 0)
                item.setText(_translate("UiRekordleiras", str(rekordleiras_list[i].nev)))
                item = self.tableWidget.item(i, 1)
                item.setText(_translate("UiRekordleiras", str(rekordleiras_list[i].tipus)))
                item = self.tableWidget.item(i, 2)
                item.setText(_translate("UiRekordleiras", str(rekordleiras_list[i].cimke)))
                item = self.tableWidget.item(i, 3)
                item.setText(_translate("UiRekordleiras", str(rekordleiras_list[i].leiras)))
                item = self.tableWidget.item(i, 4)
                item.setText(_translate("UiRekordleiras", str(rekordleiras_list[i].utolso_modositas)))
                item = self.tableWidget.item(i, 5)
                item.setText(_translate("UiRekordleiras", str(rekordleiras_list[i].letrehozva)))

            self.tableWidget.setSortingEnabled(__sortingEnabled)
            self.pushButton_hozzaadas.setText(_translate("UiRekordleiras", "Rekordleiras hozzáadása"))
            self.pushButton_modositas.setText(_translate("UiRekordleiras", "Módosítás"))
            self.pushButton_torles.setText(_translate("UiRekordleiras", "Törlés"))
            self.pushButton_mezokezeles.setText(_translate("UiRekordleiras", "Mezőkezelés"))


            for i in range(0, rekordleirasok_szama):
                if rekordleiras_list[i].nev == self.selected_rekordleiras.nev:
                    self.tableWidget.selectRow(i)

    def open_hozzaad(self):
        self.window.close()
        self.window = QtWidgets.QMainWindow()
        self.ui = UiRekordleirasHozzaadas(self.selected_rekordleiras)
        self.ui.setupUi(self.window)
        self.window.show()

    def open_modosit(self):
        self.window.close()
        row = None
        indexes = self.tableWidget.selectionModel().selectedRows()
        if len(indexes) == 1:
            row = indexes[0].row()
        rekordleiras_list, message = self.rekordleiras_controller.rekordleiras_list()
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            rekordleirasok_szama = len(rekordleiras_list)
            for i in range(0, rekordleirasok_szama):
                if i == row:
                    self.selected_rekordleiras_id = rekordleiras_list[i].id

            self.window = QtWidgets.QMainWindow()
            if self.selected_rekordleiras_id is not None:
                self.ui = UiRekordleirasModositas(self.selected_rekordleiras, self.selected_rekordleiras_id)
            else:
                pass
            self.ui.setupUi(self.window)
            self.window.show()

    def open_delete(self):
        self.window.close()
        result = QtWidgets.QMessageBox.question(None,
                                                "Törlés megerősítése...",
                                                "Biztos ki akarod törölni a kiválaszott sort?",
                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)

        if result == QtWidgets.QMessageBox.Yes:
            row = None
            indexes = self.tableWidget.selectionModel().selectedRows()
            if len(indexes) == 1:
                row = indexes[0].row()
            rekordleiras_list, message = self.rekordleiras_controller.rekordleiras_list()
            if message != "sikeres":
                (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
            else:
                rekordleirasok_szama = len(rekordleiras_list)
                for i in range(0, rekordleirasok_szama):
                    if i == row:
                        self.selected_rekordleiras_id = rekordleiras_list[i].id
                if self.selected_rekordleiras_id is not None:
                    message = self.rekordleiras_controller.rekordleiras_delete(self.selected_rekordleiras_id)
                    if message != "sikeres":
                        (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
                else:
                    pass
                self.window.close()
                self.window = QtWidgets.QMainWindow()
                self.ui = UiRekordleiras(self.selected_rekordleiras)
                self.ui.setupUi(self.window)
                self.window.show()

    def open_mezokezeles(self):
        row = None
        indexes = self.tableWidget.selectionModel().selectedRows()
        if len(indexes) == 1:
            row = indexes[0].row()
        rekordleiras_list, message = self.rekordleiras_controller.rekordleiras_list()
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            rekordleirasok_szama = len(rekordleiras_list)
            for i in range(0, rekordleirasok_szama):
                if i == row:
                    self.selected_rekordleiras_id = rekordleiras_list[i].id

            self.window = QtWidgets.QMainWindow()
            if self.selected_rekordleiras_id is not None:
                self.ui = UiMezokezeles(self.selected_rekordleiras_id)
            else:
                pass
            self.ui.setupUi(self.window)
            self.window.show()

class UiRekordleirasHozzaadas(object):
    def __init__(self, selected_rekordleiras):
        super().__init__()
        self.selected_rekordleiras = selected_rekordleiras
        self.rekordleiras_controller = RekordleirasController()

    def setupUi(self, UiRekordleirasHozzaadas):
        UiRekordleirasHozzaadas.setObjectName("UiRekordleirasHozzaadas")
        UiRekordleirasHozzaadas.resize(600, 400)
        UiRekordleirasHozzaadas.setMinimumSize(600, 400)
        UiRekordleirasHozzaadas.setMaximumSize(600, 400)
        self.window = UiRekordleirasHozzaadas
        self.centralwidget = QtWidgets.QWidget(self.window)
        self.centralwidget.setObjectName("centralwidget")

        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(10, 10, 500, 270))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.tabWidget.setFont(font)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_attributum = QtWidgets.QWidget()
        self.tab_attributum.setObjectName("tab_attributum")
        self.lineEdit_rekord_neve = QtWidgets.QLineEdit(self.tab_attributum)
        self.lineEdit_rekord_neve.setGeometry(QtCore.QRect(230, 30, 200, 30))
        self.lineEdit_rekord_neve.setObjectName("lineEdit_rekord_neve")
        self.label = QtWidgets.QLabel(self.tab_attributum)
        self.label.setGeometry(QtCore.QRect(10, 30, 200, 30))
        self.label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.comboBox_tipus = QtWidgets.QComboBox(self.tab_attributum)
        self.comboBox_tipus.addItem("")
        self.comboBox_tipus.addItem("")
        self.comboBox_tipus.addItem("")
        self.comboBox_tipus.setGeometry(QtCore.QRect(230, 80, 200, 30))
        self.comboBox_tipus.setObjectName("comboBox_tipus")
        self.label_2 = QtWidgets.QLabel(self.tab_attributum)
        self.label_2.setGeometry(QtCore.QRect(10, 80, 200, 30))
        self.label_2.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.lineEdit_cimke = QtWidgets.QLineEdit(self.tab_attributum)
        self.lineEdit_cimke.setGeometry(QtCore.QRect(230, 130, 200, 30))
        self.lineEdit_cimke.setObjectName("lineEdit_cimke")
        self.label_3 = QtWidgets.QLabel(self.tab_attributum)
        self.label_3.setGeometry(QtCore.QRect(10, 130, 200, 30))
        self.label_3.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.lineEdit_leiras = QtWidgets.QLineEdit(self.tab_attributum)
        self.lineEdit_leiras.setGeometry(QtCore.QRect(230, 180, 200, 30))
        self.lineEdit_leiras.setObjectName("lineEdit_leiras")
        self.label_4 = QtWidgets.QLabel(self.tab_attributum)
        self.label_4.setGeometry(QtCore.QRect(10, 180, 200, 30))
        self.label_4.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName("label_4")
        self.tabWidget.addTab(self.tab_attributum, "")

        self.pushButton_Megse = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Megse.setGeometry(QtCore.QRect(330, 300, 150, 30))
        self.pushButton_Megse.setFont(font)
        self.pushButton_Megse.clicked.connect(lambda: self.closeRekordleirasHozzaadas())
        self.pushButton_Megse.setDefault(True)
        self.pushButton_Megse.setObjectName("pushButton_Megse")
        self.pushButton_Hozzaad = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Hozzaad.setGeometry(QtCore.QRect(150, 300, 150, 30))
        self.pushButton_Hozzaad.setFont(font)
        self.pushButton_Hozzaad.clicked.connect(self.rekordleiras_hozzaad)
        self.pushButton_Hozzaad.setObjectName("pushButton_Hozzaad")
        UiRekordleirasHozzaadas.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(UiRekordleirasHozzaadas)
        self.statusbar.setObjectName("statusbar")
        UiRekordleirasHozzaadas.setStatusBar(self.statusbar)

        self.retranslateUi(UiRekordleirasHozzaadas)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(UiRekordleirasHozzaadas)

    def retranslateUi(self, UiRekordleirasHozzaadas):
        _translate = QtCore.QCoreApplication.translate
        UiRekordleirasHozzaadas.setWindowTitle(_translate("UiRekordleirasHozzaadas", "Rekordleírás hozzáadás"))
        self.label.setText(_translate("UiRekordleirasHozzaadas", "Rekordleírás neve:"))
        self.comboBox_tipus.setItemText(0, _translate("UiRekordleirasHozzaadas", "SAS"))
        self.comboBox_tipus.setItemText(1, _translate("UiRekordleirasHozzaadas", "CSV"))
        self.comboBox_tipus.setItemText(2, _translate("UiRekordleirasHozzaadas", "Excel"))
        self.label_2.setText(_translate("UiRekordleirasHozzaadas", "Típus:"))
        self.label_3.setText(_translate("UiRekordleirasHozzaadas", "Címke:"))
        self.label_4.setText(_translate("UiRekordleirasHozzaadas", "Leírás:"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_attributum),
                                  _translate("UiRekordleirasHozzaadas", "Attribútumok"))
        self.pushButton_Megse.setText(_translate("UiRekordleirasHozzaadas", "Mégse"))
        self.pushButton_Hozzaad.setText(_translate("UiRekordleirasHozzaadas", "Hozzáad"))

    def rekordleiras_hozzaad(self):
        message = self.rekordleiras_controller.rekordleiras_hozzaadas(self.lineEdit_rekord_neve.text(), str(self.comboBox_tipus.currentText()),
                                                            self.lineEdit_cimke.text(), self.lineEdit_leiras.text())

        if message == "sikeres":
            self.window.close()
            self.window = QtWidgets.QMainWindow()
            self.ui = UiRekordleiras(self.selected_rekordleiras)
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)

    def closeRekordleirasHozzaadas(self):
        self.window.close()
        self.window = QtWidgets.QMainWindow()
        self.ui = UiRekordleiras(self.selected_rekordleiras)
        self.ui.setupUi(self.window)
        self.window.show()


class UiRekordleirasModositas(object):
    def __init__(self, selected_rekordleiras, selected_rekordleiras_id):
        super().__init__()
        self.rekordleiras_controller = RekordleirasController()
        self.selected_rekordleiras = selected_rekordleiras
        self.selected_rekordleiras_id = selected_rekordleiras_id

    def setupUi(self, UiRekordleirasModositas):
        UiRekordleirasModositas.setObjectName("UiRekordleirasModositas")
        UiRekordleirasModositas.resize(600, 400)
        UiRekordleirasModositas.setMinimumSize(600, 400)
        UiRekordleirasModositas.setMaximumSize(600, 400)
        self.window = UiRekordleirasModositas
        self.centralwidget = QtWidgets.QWidget(self.window)
        self.centralwidget.setObjectName("centralwidget")

        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(10, 10, 500, 270))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.tabWidget.setFont(font)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_attributum = QtWidgets.QWidget()
        self.tab_attributum.setObjectName("tab_attributum")
        self.lineEdit_rekord_neve = QtWidgets.QLineEdit(self.tab_attributum)
        self.lineEdit_rekord_neve.setGeometry(QtCore.QRect(230, 30, 200, 30))
        self.lineEdit_rekord_neve.setObjectName("lineEdit_rekord_neve")
        self.label = QtWidgets.QLabel(self.tab_attributum)
        self.label.setGeometry(QtCore.QRect(10, 30, 200, 30))
        self.label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.comboBox_tipus = QtWidgets.QComboBox(self.tab_attributum)
        self.comboBox_tipus.addItem("")
        self.comboBox_tipus.addItem("")
        self.comboBox_tipus.addItem("")
        self.comboBox_tipus.setGeometry(QtCore.QRect(230, 80, 200, 30))
        self.comboBox_tipus.setObjectName("comboBox_tipus")
        self.label_2 = QtWidgets.QLabel(self.tab_attributum)
        self.label_2.setGeometry(QtCore.QRect(10, 80, 200, 30))
        self.label_2.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.lineEdit_cimke = QtWidgets.QLineEdit(self.tab_attributum)
        self.lineEdit_cimke.setGeometry(QtCore.QRect(230, 130, 200, 30))
        self.lineEdit_cimke.setObjectName("lineEdit_cimke")
        self.label_3 = QtWidgets.QLabel(self.tab_attributum)
        self.label_3.setGeometry(QtCore.QRect(10, 130, 200, 30))
        self.label_3.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.lineEdit_leiras = QtWidgets.QLineEdit(self.tab_attributum)
        self.lineEdit_leiras.setGeometry(QtCore.QRect(230, 180, 200, 30))
        self.lineEdit_leiras.setObjectName("lineEdit_leiras")
        self.label_4 = QtWidgets.QLabel(self.tab_attributum)
        self.label_4.setGeometry(QtCore.QRect(10, 180, 200, 30))
        self.label_4.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName("label_4")
        self.tabWidget.addTab(self.tab_attributum, "")

        self.pushButton_Megse = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Megse.setGeometry(QtCore.QRect(330, 300, 150, 30))
        self.pushButton_Megse.setFont(font)
        self.pushButton_Megse.clicked.connect(lambda: self.closeRekordleirasModositas())
        self.pushButton_Megse.setDefault(True)
        self.pushButton_Megse.setObjectName("pushButton_Megse")
        self.pushButton_Mentes = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Mentes.setGeometry(QtCore.QRect(150, 300, 150, 30))
        self.pushButton_Mentes.setFont(font)
        self.pushButton_Mentes.setObjectName("pushButton_Mentes")
        self.pushButton_Mentes.clicked.connect(self.rekordleiras_modositas)
        UiRekordleirasModositas.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(UiRekordleirasModositas)
        self.statusbar.setObjectName("statusbar")
        UiRekordleirasModositas.setStatusBar(self.statusbar)

        self.retranslateUi(UiRekordleirasModositas)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(UiRekordleirasModositas)

    def retranslateUi(self, UiRekordleirasModositas):
        selected_rekordleiras, message = self.rekordleiras_controller.rekordleiras_getOneById(self.selected_rekordleiras_id)
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            _translate = QtCore.QCoreApplication.translate
            UiRekordleirasModositas.setWindowTitle(_translate("UiRekordleirasModositas", "Rekordleírás módosítása"))
            self.label.setText(_translate("UiRekordleirasModositas", "Rekordleírás neve:"))
            item = self.lineEdit_rekord_neve
            item.setText(_translate("UiRekordleirasModositas", selected_rekordleiras.nev))
            self.label_2.setText(_translate("UiRekordleirasModositas", "Típus:"))
            item = self.comboBox_tipus
            item.setItemText(0, _translate("UiRekordleirasModositas", "SAS"))
            item.setItemText(1, _translate("UiRekordleirasModositas", "CSV"))
            item.setItemText(2, _translate("UiRekordleirasModositas", "Excel"))
            item.setCurrentText(_translate("UiRekordleirasModositas", selected_rekordleiras.tipus))
            self.label_3.setText(_translate("UiRekordleirasModositas", "Címke:"))
            item = self.lineEdit_cimke
            item.setText(_translate("UiRekordleirasModositas", selected_rekordleiras.cimke))
            self.label_4.setText(_translate("UiRekordleirasModositas", "Leírás:"))
            item = self.lineEdit_leiras
            item.setText(_translate("UiRekordleirasModositas", selected_rekordleiras.leiras))
            self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_attributum),
                                      _translate("UiRekordleirasModositas", "Attribútumok"))
            self.pushButton_Megse.setText(_translate("UiRekordleirasModositas", "Mégse"))
            self.pushButton_Mentes.setText(_translate("UiRekordleirasModositas", "Mentés"))



    def rekordleiras_modositas(self):
        message = self.rekordleiras_controller.rekordleiras_modositas(self.selected_rekordleiras_id, self.lineEdit_rekord_neve.text(),
                                                            self.comboBox_tipus.currentText(),
                                                            self.lineEdit_cimke.text(), self.lineEdit_leiras.text())

        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        else:
            self.window.close()
            self.window = QtWidgets.QMainWindow()
            self.ui = UiRekordleiras(self.selected_rekordleiras)
            self.ui.setupUi(self.window)
            self.window.show()

    def closeRekordleirasModositas(self):
        self.window.close()
        self.window = QtWidgets.QMainWindow()
        self.ui = UiRekordleiras(self.selected_rekordleiras)
        self.ui.setupUi(self.window)
        self.window.show()

class UiMezokezeles(object):
    def __init__(self, rekordleiras_id):
        super().__init__()
        self.mezo_controller = MezoController()
        self.mutato_controller = MutatoController()
        self.nomenklatura_controller = NomenklaturaController()
        self.rekordleiras_id = rekordleiras_id
        self.selected_mezo_id = None
        self.mutato_nomenklatura_list, message = self.get_mutato_nomenklatura_list()

    def setupUi(self, UiMezokezeles):
        mezo_list_to_rekordleiras, message = self.mezo_controller.mezokezeles(self.rekordleiras_id)
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            mezok_szama = len(mezo_list_to_rekordleiras)

            UiMezokezeles.setObjectName("UiMezokezeles")
            UiMezokezeles.resize(1100, 600)
            UiMezokezeles.setMinimumSize(1100, 600)
            UiMezokezeles.setMaximumSize(1100, 600)
            self.window = UiMezokezeles
            self.centralwidget = QtWidgets.QWidget(self.window)
            self.centralwidget.setObjectName("centralwidget")

            self.frame = QtWidgets.QFrame(self.centralwidget)
            self.frame.setGeometry(QtCore.QRect(10, 10, 1000, 580))
            font = QtGui.QFont()
            font.setPointSize(12)
            self.frame.setFont(font)
            self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
            self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
            self.frame.setObjectName("frame")

            self.tableWidget = QtWidgets.QTableWidget(self.frame)
            self.tableWidget.setGeometry(QtCore.QRect(10, 100, 950, 400))
            self.tableWidget.setObjectName("tableWidget")
            self.tableWidget.horizontalHeader().setStretchLastSection(True)
            self.tableWidget.setColumnCount(6)
            self.tableWidget.setRowCount(mezok_szama)

            self.pushButton_mezo_hozzaadas = QtWidgets.QPushButton(self.frame)
            self.pushButton_mezo_hozzaadas.setGeometry(QtCore.QRect(20, 40, 200, 30))
            self.pushButton_mezo_hozzaadas.setFont(font)
            self.pushButton_mezo_hozzaadas.setObjectName("pushButton_mezo_hozzaadas")
            self.pushButton_mezo_modositas = QtWidgets.QPushButton(self.frame)
            self.pushButton_mezo_modositas.setGeometry(QtCore.QRect(250, 40, 200, 30))
            self.pushButton_mezo_modositas.setFont(font)
            self.pushButton_mezo_modositas.setObjectName("pushButton_mezo_modositas")
            self.pushButton_mezo_torlese = QtWidgets.QPushButton(self.frame)
            self.pushButton_mezo_torlese.setGeometry(QtCore.QRect(480, 40, 200, 30))
            self.pushButton_mezo_torlese.setFont(font)
            self.pushButton_mezo_torlese.setObjectName("pushButton_mezo_torlese")

            UiMezokezeles.setCentralWidget(self.centralwidget)
            self.menubar = QtWidgets.QMenuBar(UiMezokezeles)
            self.menubar.setGeometry(QtCore.QRect(0, 0, 443, 21))
            self.menubar.setObjectName("menubar")
            UiMezokezeles.setMenuBar(self.menubar)
            self.statusbar = QtWidgets.QStatusBar(UiMezokezeles)
            self.statusbar.setObjectName("statusbar")
            UiMezokezeles.setStatusBar(self.statusbar)

            for i in range(0, mezok_szama):
                item = QtWidgets.QTableWidgetItem()
                self.tableWidget.setVerticalHeaderItem(i, item)

            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(0, item)
            self.tableWidget.setColumnWidth(0, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(1, item)
            self.tableWidget.setColumnWidth(1, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(2, item)
            self.tableWidget.setColumnWidth(2, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(3, item)
            self.tableWidget.setColumnWidth(3, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(4, item)
            self.tableWidget.setColumnWidth(4, 150)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget.setHorizontalHeaderItem(5, item)
            self.tableWidget.setColumnWidth(5, 150)

            for i in range(0, mezok_szama):
                for j in range(0, 6):
                    item = QtWidgets.QTableWidgetItem()
                    self.tableWidget.setItem(i, j, item)

            self.pushButton_mezo_hozzaadas.clicked.connect(self.open_hozzaad)
            self.pushButton_mezo_modositas.clicked.connect(self.open_modosit)
            self.pushButton_mezo_torlese.clicked.connect(self.open_delete)

            self.retranslateUi(UiMezokezeles)
            QtCore.QMetaObject.connectSlotsByName(UiMezokezeles)

            self.tableWidget.setSelectionBehavior(QtWidgets.QTableWidget.SelectRows)
            self.tableWidget.selectionModel().selectionChanged.connect(
                self.on_selection_changed
            )

            self.on_selection_changed()

    def on_selection_changed(self):
        self.pushButton_mezo_modositas.setEnabled(
            bool(self.tableWidget.selectionModel().selectedRows())
        )
        self.pushButton_mezo_torlese.setEnabled(
            bool(self.tableWidget.selectionModel().selectedRows())
        )

    def retranslateUi(self, UiMezokezeles):
        _translate = QtCore.QCoreApplication.translate
        UiMezokezeles.setWindowTitle(_translate("UiMezokezeles", "Mezőkezelés"))

        mezo_list_to_rekordleiras, message = self.mezo_controller.mezokezeles(self.rekordleiras_id)
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            mezok_szama = len(mezo_list_to_rekordleiras)
            for i in range(0, mezok_szama):
                item = self.tableWidget.verticalHeaderItem(i)
                item.setText(_translate("UiMezokezeles", str(i + 1)))

            item = self.tableWidget.horizontalHeaderItem(0)
            item.setText(_translate("UiMezokezeles", "Mező neve"))
            item = self.tableWidget.horizontalHeaderItem(1)
            item.setText(_translate("UiMezokezeles", "Adattípus"))
            item = self.tableWidget.horizontalHeaderItem(2)
            item.setText(_translate("UiMezokezeles", "Leírás"))
            item = self.tableWidget.horizontalHeaderItem(3)
            item.setText(_translate("UiMezokezeles", "Business Glossary név"))
            item = self.tableWidget.horizontalHeaderItem(4)
            item.setText(_translate("UiMezokezeles", "Tulajdonos"))
            item = self.tableWidget.horizontalHeaderItem(5)
            item.setText(_translate("UiMezokezeles", "Szervezet"))

            __sortingEnabled = self.tableWidget.isSortingEnabled()
            self.tableWidget.setSortingEnabled(False)



            for i in range(0, mezok_szama):
                item = self.tableWidget.item(i, 0)
                item.setText(_translate("UiMezokezeles", str(mezo_list_to_rekordleiras[i].nev)))
                item = self.tableWidget.item(i, 1)
                item.setText(_translate("UiMezokezeles", str(mezo_list_to_rekordleiras[i].adattipus)))
                item = self.tableWidget.item(i, 2)
                item.setText(_translate("UiMezokezeles", str(mezo_list_to_rekordleiras[i].leiras)))
                item = self.tableWidget.item(i, 3)
                item.setText(_translate("UiMezokezeles", str(mezo_list_to_rekordleiras[i].business_glossary_nev)))
                item = self.tableWidget.item(i, 4)

                mutato_nomenklatura_tulajdonos = ""
                mutato_nomenklatura_szervezet = ""

                for mutato_nomenklatura in self.mutato_nomenklatura_list:
                    if mezo_list_to_rekordleiras[i].business_glossary_nev == mutato_nomenklatura.nev:
                        mutato_nomenklatura_tulajdonos = mutato_nomenklatura.tulajdonos
                        mutato_nomenklatura_szervezet = mutato_nomenklatura.szervezet

                if mutato_nomenklatura_tulajdonos is None:
                    mutato_nomenklatura_tulajdonos = ""

                if mutato_nomenklatura_szervezet is None:
                    mutato_nomenklatura_szervezet = ""

                item.setText(_translate("UiMezokezeles", str(mutato_nomenklatura_tulajdonos)))
                item = self.tableWidget.item(i, 5)
                item.setText(_translate("UiMezokezeles", str(mutato_nomenklatura_szervezet)))

            self.tableWidget.setSortingEnabled(__sortingEnabled)
            self.pushButton_mezo_hozzaadas.setText(_translate("UiMezokezeles", "Hozzáadás"))
            self.pushButton_mezo_modositas.setText(_translate("UiMezokezeles", "Módosítás"))
            self.pushButton_mezo_torlese.setText(_translate("UiMezokezeles", "Törlés"))

    def open_hozzaad(self):
        self.window.close()
        self.window = QtWidgets.QMainWindow()
        self.ui = UiMezoHozzaadas(self.rekordleiras_id)
        self.ui.setupUi(self.window)
        self.window.show()

    def open_modosit(self):
        self.window.close()
        row = None
        indexes = self.tableWidget.selectionModel().selectedRows()
        if len(indexes) == 1:
            row = indexes[0].row()
        mezo_list, message = self.mezo_controller.mezokezeles(self.rekordleiras_id)
        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            mezok_szama = len(mezo_list)
            for i in range(0, mezok_szama):
                if i == row:
                    self.selected_mezo_id = mezo_list[i].id

            self.window.close()
            self.window = QtWidgets.QMainWindow()
            if self.selected_mezo_id is not None:
                self.ui = UiMezoModositas(self.rekordleiras_id, self.selected_mezo_id)
            else:
                pass
            self.ui.setupUi(self.window)
            self.window.show()

    def open_delete(self):
        result = QtWidgets.QMessageBox.question(None,
                                                "Törlés megerősítése...",
                                                "Biztos ki akarod törölni a kiválaszott sort?",
                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)

        if result == QtWidgets.QMessageBox.Yes:
            row = None
            indexes = self.tableWidget.selectionModel().selectedRows()
            if len(indexes) == 1:
                row = indexes[0].row()
            mezo_list, message = self.mezo_controller.mezokezeles(self.rekordleiras_id)
            if message != "sikeres":
                (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
            else:
                mezok_szama = len(mezo_list)
                for i in range(0, mezok_szama):
                    if i == row:
                        self.selected_mezo_id = mezo_list[i].id
                if self.selected_mezo_id is not None:
                    message = self.mezo_controller.mezo_delete(self.selected_mezo_id)
                    if message != "sikeres":
                        (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
                else:
                    pass
                self.window.close()
                self.window = QtWidgets.QMainWindow()
                self.ui = UiMezokezeles(self.rekordleiras_id)
                self.ui.setupUi(self.window)
                self.window.show()

    def get_mutato_nomenklatura_list(self):
        mutato_nomenklatura_list = list()
        mutato_list, message = self.mutato_controller.mutato_list()

        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        else:
            nomenklatura_list, message = self.nomenklatura_controller.nomenklatura_list()

            if message != "sikeres":
                QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
            else:
                for mutato in mutato_list:
                    mutato_nomenklatura_list.append(mutato)
                for nomenklatura in nomenklatura_list:
                    mutato_nomenklatura_list.append(nomenklatura)

                """if len(mutato_nomenklatura_list) == 0:
                    message = "Hiba! Mező módosításhoz előbb létre kell hozni egy mutatót vagy egy nómenklatúrát."
                """

        return mutato_nomenklatura_list, message

class UiMezoHozzaadas(object):
    def __init__(self, rekordleiras_id):
        super().__init__()
        self.mezo_controller = MezoController()
        self.rekordleiras_id = rekordleiras_id
        self.mutato_controller = MutatoController()
        self.nomenklatura_controller = NomenklaturaController()

    def setupUi(self, UiMezoHozzaadas):
        UiMezoHozzaadas.setObjectName("UiMezoHozzaadas")
        UiMezoHozzaadas.resize(600, 400)
        UiMezoHozzaadas.setMinimumSize(600, 400)
        UiMezoHozzaadas.setMaximumSize(600, 400)
        self.window = UiMezoHozzaadas
        self.centralwidget = QtWidgets.QWidget(self.window)
        self.centralwidget.setObjectName("centralwidget")

        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(10, 10, 500, 270))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.tabWidget.setFont(font)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_attributum = QtWidgets.QWidget()
        self.tab_attributum.setObjectName("tab_attributum")
        self.lineEdit_mezo_neve = QtWidgets.QLineEdit(self.tab_attributum)
        self.lineEdit_mezo_neve.setGeometry(QtCore.QRect(230, 30, 200, 30))
        self.lineEdit_mezo_neve.setObjectName("lineEdit_mezo_neve")
        self.label = QtWidgets.QLabel(self.tab_attributum)
        self.label.setGeometry(QtCore.QRect(10, 30, 200, 30))
        self.label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.comboBox_adattipus = QtWidgets.QComboBox(self.tab_attributum)
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.setGeometry(QtCore.QRect(230, 80, 200, 30))
        self.comboBox_adattipus.setObjectName("lineEdit_adattipus")
        self.label_2 = QtWidgets.QLabel(self.tab_attributum)
        self.label_2.setGeometry(QtCore.QRect(10, 80, 200, 30))
        self.label_2.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.lineEdit_leiras = QtWidgets.QLineEdit(self.tab_attributum)
        self.lineEdit_leiras.setGeometry(QtCore.QRect(230, 130, 200, 30))
        self.lineEdit_leiras.setObjectName("lineEdit_leiras")
        self.label_3 = QtWidgets.QLabel(self.tab_attributum)
        self.label_3.setGeometry(QtCore.QRect(10, 130, 200, 30))
        self.label_3.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.comboBox_business_glossary_nev = QtWidgets.QComboBox(self.tab_attributum)

        self.comboBox_business_glossary_nev.addItem("")
        mutato_nomenklatura_list, message = self.get_mutato_nomenklatura_list()
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        else:
            for i in range(0, len(mutato_nomenklatura_list)):
                self.comboBox_business_glossary_nev.addItem("")

        self.comboBox_business_glossary_nev.setGeometry(QtCore.QRect(230, 180, 200, 30))
        self.comboBox_business_glossary_nev.setObjectName("comboBox_business_glossary_nev")
        self.label_4 = QtWidgets.QLabel(self.tab_attributum)
        self.label_4.setGeometry(QtCore.QRect(10, 180, 200, 30))
        self.label_4.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName("label_4")
        self.tabWidget.addTab(self.tab_attributum, "")

        self.pushButton_Megse = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Megse.setGeometry(QtCore.QRect(330, 300, 150, 30))
        self.pushButton_Megse.setFont(font)
        self.pushButton_Megse.clicked.connect(lambda: self.closeMezoHozzaadas())
        self.pushButton_Megse.setDefault(True)
        self.pushButton_Megse.setObjectName("pushButton_Megse")
        self.pushButton_Hozzaad = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Hozzaad.setGeometry(QtCore.QRect(150, 300, 150, 30))
        self.pushButton_Hozzaad.setFont(font)
        self.pushButton_Hozzaad.clicked.connect(self.mezo_hozzaad)
        self.pushButton_Hozzaad.setObjectName("pushButton_Hozzaad")
        UiMezoHozzaadas.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(UiMezoHozzaadas)
        self.statusbar.setObjectName("statusbar")
        UiMezoHozzaadas.setStatusBar(self.statusbar)

        self.retranslateUi(UiMezoHozzaadas)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(UiMezoHozzaadas)

    def retranslateUi(self, UiMezoHozzaadas):
        _translate = QtCore.QCoreApplication.translate
        UiMezoHozzaadas.setWindowTitle(_translate("UiMezoHozzaadas", "Mező hozzáadás"))
        self.label.setText(_translate("UiMezoHozzaadas", "Mező neve:"))
        self.label_2.setText(_translate("UiMezoHozzaadas", "Adattípus:"))
        self.comboBox_adattipus.setItemText(0, _translate("UiMezoHozzaadas", "byte"))
        self.comboBox_adattipus.setItemText(1, _translate("UiMezoHozzaadas", "short"))
        self.comboBox_adattipus.setItemText(2, _translate("UiMezoHozzaadas", "int"))
        self.comboBox_adattipus.setItemText(3, _translate("UiMezoHozzaadas", "long"))
        self.comboBox_adattipus.setItemText(4, _translate("UiMezoHozzaadas", "float"))
        self.comboBox_adattipus.setItemText(5, _translate("UiMezoHozzaadas", "double"))
        self.comboBox_adattipus.setItemText(6, _translate("UiMezoHozzaadas", "char"))
        self.comboBox_adattipus.setItemText(7, _translate("UiMezoHozzaadas", "string"))
        self.comboBox_adattipus.setItemText(8, _translate("UiMezoHozzaadas", "boolean"))
        self.comboBox_adattipus.setItemText(9, _translate("UiMezoHozzaadas", "object"))
        self.comboBox_adattipus.setItemText(10, _translate("UiMezoHozzaadas", "time"))
        self.comboBox_adattipus.setItemText(11, _translate("UiMezoHozzaadas", "date"))
        self.comboBox_adattipus.setItemText(12, _translate("UiMezoHozzaadas", "datetime"))
        self.comboBox_adattipus.setItemText(13, _translate("UiMezoHozzaadas", "blob"))
        self.comboBox_adattipus.setItemText(14, _translate("UiMezoHozzaadas", "enum"))
        self.comboBox_adattipus.setItemText(15, _translate("UiMezoHozzaadas", "set"))
        self.label_3.setText(_translate("UiMezoHozzaadas", "Leírás:"))
        self.label_4.setText(_translate("UiMezoHozzaadas", "Business Glossary név:"))

        self.comboBox_business_glossary_nev.setItemText(0, _translate("UiMezoHozzaadas", ""))
        mutato_nomenklatura_list, message = self.get_mutato_nomenklatura_list()
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        else:
            for i in range(0, len(mutato_nomenklatura_list)):
                self.comboBox_business_glossary_nev.setItemText(i + 1, _translate("UiMezoHozzaadas", mutato_nomenklatura_list[i].nev))

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_attributum),
                                  _translate("UiMezoHozzaadas", "Attribútumok"))
        self.pushButton_Megse.setText(_translate("UiMezoHozzaadas", "Mégse"))
        self.pushButton_Hozzaad.setText(_translate("UiMezoHozzaadas", "Hozzáad"))

    def mezo_hozzaad(self):
        message = self.mezo_controller.mezo_hozzaadas(self.lineEdit_mezo_neve.text(), self.comboBox_adattipus.currentText(),
                                                      self.lineEdit_leiras.text(), self.comboBox_business_glossary_nev.currentText(),
                                                      self.rekordleiras_id)

        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        else:
            self.window.close()
            self.window = QtWidgets.QMainWindow()
            self.ui = UiMezokezeles(self.rekordleiras_id)
            self.ui.setupUi(self.window)
            self.window.show()

    def get_mutato_nomenklatura_list(self):
        mutato_nomenklatura_list = list()
        mutato_list, message = self.mutato_controller.mutato_list()

        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        else:
            nomenklatura_list, message = self.nomenklatura_controller.nomenklatura_list()

            if message != "sikeres":
                QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
            else:
                for mutato in mutato_list:
                    mutato_nomenklatura_list.append(mutato)
                for nomenklatura in nomenklatura_list:
                    mutato_nomenklatura_list.append(nomenklatura)

                """if len(mutato_nomenklatura_list) == 0:
                    message = "Hiba! Mező létrehozásához előbb létre kell hozni egy mutatót vagy egy nómenklatúrát."
                """

        return mutato_nomenklatura_list, message

    def closeMezoHozzaadas(self):
        self.window.close()
        self.window = QtWidgets.QMainWindow()
        self.ui = UiMezokezeles(self.rekordleiras_id)
        self.ui.setupUi(self.window)
        self.window.show()


class UiMezoModositas(object):
    def __init__(self, rekordleiras_id, selected_mezo_id):
        super().__init__()
        self.mezo_controller = MezoController()
        self.rekordleiras_id = rekordleiras_id
        self.selected_mezo_id = selected_mezo_id
        self.mutato_controller = MutatoController()
        self.nomenklatura_controller = NomenklaturaController()

    def setupUi(self, UiMezoModosit):
        UiMezoModosit.setObjectName("UiMezoModosit")
        UiMezoModosit.resize(600, 400)
        UiMezoModosit.setMinimumSize(600, 400)
        UiMezoModosit.setMaximumSize(600, 400)
        self.window = UiMezoModosit
        self.centralwidget = QtWidgets.QWidget(self.window)
        self.centralwidget.setObjectName("centralwidget")

        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(10, 10, 500, 270))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.tabWidget.setFont(font)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_attributum = QtWidgets.QWidget()
        self.tab_attributum.setObjectName("tab_attributum")
        self.lineEdit_mezo_neve = QtWidgets.QLineEdit(self.tab_attributum)
        self.lineEdit_mezo_neve.setGeometry(QtCore.QRect(230, 30, 200, 30))
        self.lineEdit_mezo_neve.setObjectName("lineEdit_mezo_neve")
        self.label = QtWidgets.QLabel(self.tab_attributum)
        self.label.setGeometry(QtCore.QRect(10, 30, 200, 30))
        self.label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.comboBox_adattipus = QtWidgets.QComboBox(self.tab_attributum)
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.addItem("")
        self.comboBox_adattipus.setGeometry(QtCore.QRect(230, 80, 200, 30))
        self.comboBox_adattipus.setObjectName("lineEdit_adattipus")
        self.label_2 = QtWidgets.QLabel(self.tab_attributum)
        self.label_2.setGeometry(QtCore.QRect(10, 80, 200, 30))
        self.label_2.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.lineEdit_leiras = QtWidgets.QLineEdit(self.tab_attributum)
        self.lineEdit_leiras.setGeometry(QtCore.QRect(230, 130, 200, 30))
        self.lineEdit_leiras.setObjectName("lineEdit_leiras")
        self.label_3 = QtWidgets.QLabel(self.tab_attributum)
        self.label_3.setGeometry(QtCore.QRect(10, 130, 200, 30))
        self.label_3.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.comboBox_business_glossary_nev = QtWidgets.QComboBox(self.tab_attributum)

        self.comboBox_business_glossary_nev.addItem("")
        mutato_nomenklatura_list, message = self.get_mutato_nomenklatura_list()
        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        else:
            for i in range(0, len(mutato_nomenklatura_list)):
                self.comboBox_business_glossary_nev.addItem("")

        self.comboBox_business_glossary_nev.setGeometry(QtCore.QRect(230, 180, 200, 30))
        self.comboBox_business_glossary_nev.setObjectName("comboBox_business_glossary_nev")
        self.label_4 = QtWidgets.QLabel(self.tab_attributum)
        self.label_4.setGeometry(QtCore.QRect(10, 180, 200, 30))
        self.label_4.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName("label_4")
        self.tabWidget.addTab(self.tab_attributum, "")

        self.pushButton_Megse = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Megse.setGeometry(QtCore.QRect(330, 300, 150, 30))
        self.pushButton_Megse.setFont(font)
        self.pushButton_Megse.clicked.connect(lambda: self.closeMezoModositas())
        self.pushButton_Megse.setDefault(True)
        self.pushButton_Megse.setObjectName("pushButton_Megse")
        self.pushButton_Mentes = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Mentes.setGeometry(QtCore.QRect(150, 300, 150, 30))
        self.pushButton_Mentes.setFont(font)
        self.pushButton_Mentes.clicked.connect(self.mezo_modositas)
        self.pushButton_Mentes.setObjectName("pushButton_Hozzaad")
        UiMezoModosit.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(UiMezoModosit)
        self.statusbar.setObjectName("statusbar")
        UiMezoModosit.setStatusBar(self.statusbar)

        self.retranslateUi(UiMezoModosit)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(UiMezoModosit)

    def retranslateUi(self, UiMezoModosit):
        selected_mezo, message = self.mezo_controller.mezo_getOneById(self.selected_mezo_id)

        if message != "sikeres":
            (QtWidgets.QMessageBox.warning(self.window, "Hiba", message))
        else:
            _translate = QtCore.QCoreApplication.translate
            UiMezoModosit.setWindowTitle(_translate("UiMezoModosit", "Mező módosítása"))
            self.label.setText(_translate("UiMezoModosit", "Mező neve:"))
            item = self.lineEdit_mezo_neve
            item.setText(_translate("UiMezoModosit", selected_mezo.nev))
            self.label_2.setText(_translate("UiMezoModosit", "Adattípus:"))
            item = self.comboBox_adattipus
            item.setItemText(0, _translate("UiMezoModosit", "byte"))
            item.setItemText(1, _translate("UiMezoModosit", "short"))
            item.setItemText(2, _translate("UiMezoModosit", "int"))
            item.setItemText(3, _translate("UiMezoModosit", "long"))
            item.setItemText(4, _translate("UiMezoModosit", "float"))
            item.setItemText(5, _translate("UiMezoModosit", "double"))
            item.setItemText(6, _translate("UiMezoModosit", "char"))
            item.setItemText(7, _translate("UiMezoModosit", "string"))
            item.setItemText(8, _translate("UiMezoModosit", "boolean"))
            item.setItemText(9, _translate("UiMezoModosit", "object"))
            item.setItemText(10, _translate("UiMezoModosit", "time"))
            item.setItemText(11, _translate("UiMezoModosit", "date"))
            item.setItemText(12, _translate("UiMezoModosit", "datetime"))
            item.setItemText(13, _translate("UiMezoModosit", "blob"))
            item.setItemText(14, _translate("UiMezoModosit", "enum"))
            item.setItemText(15, _translate("UiMezoModosit", "set"))
            item.setCurrentText(_translate("UiMezoModosit", selected_mezo.adattipus))
            self.label_3.setText(_translate("UiMezoModosit", "Leírás:"))
            item = self.lineEdit_leiras
            item.setText(_translate("UiMezoModosit", selected_mezo.leiras))
            self.label_4.setText(_translate("UiMezoModosit", "Business Glossary név:"))

            item = self.comboBox_business_glossary_nev
            item.setItemText(0, _translate("UiMezoModositas", ""))
            mutato_nomenklatura_list, message = self.get_mutato_nomenklatura_list()
            if message != "sikeres":
                QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
            else:
                for i in range(0, len(mutato_nomenklatura_list)):
                    item.setItemText(i + 1, _translate("UiMezoModositas", mutato_nomenklatura_list[i].nev))

            item.setCurrentText(_translate("UiMezoModosit", selected_mezo.business_glossary_nev))

            self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_attributum),
                                      _translate("UiMezoModosit", "Attribútumok"))
            self.pushButton_Megse.setText(_translate("UiMezoModosit", "Mégse"))
            self.pushButton_Mentes.setText(_translate("UiMezoModosit", "Mentés"))


    def mezo_modositas(self):
        message = self.mezo_controller.mezo_modositas(self.selected_mezo_id, self.lineEdit_mezo_neve.text(),
                                            self.comboBox_adattipus.currentText(), self.lineEdit_leiras.text(),
                                            self.comboBox_business_glossary_nev.currentText(), self.rekordleiras_id)

        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        else:
            self.window.close()
            self.window = QtWidgets.QMainWindow()
            self.ui = UiMezokezeles(self.rekordleiras_id)
            self.ui.setupUi(self.window)
            self.window.show()

    def get_mutato_nomenklatura_list(self):
        mutato_nomenklatura_list = list()
        mutato_list, message = self.mutato_controller.mutato_list()

        if message != "sikeres":
            QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
        else:
            nomenklatura_list, message = self.nomenklatura_controller.nomenklatura_list()

            if message != "sikeres":
                QtWidgets.QMessageBox.warning(self.window, "Hiba", message)
            else:
                for mutato in mutato_list:
                    mutato_nomenklatura_list.append(mutato)
                for nomenklatura in nomenklatura_list:
                    mutato_nomenklatura_list.append(nomenklatura)

                """if len(mutato_nomenklatura_list) == 0:
                    message = "Hiba! Mező módosításhoz előbb létre kell hozni egy mutatót vagy egy nómenklatúrát."
                """

        return mutato_nomenklatura_list, message

    def closeMezoModositas(self):
        self.window.close()
        self.window = QtWidgets.QMainWindow()
        self.ui = UiMezokezeles(self.rekordleiras_id)
        self.ui.setupUi(self.window)
        self.window.show()
