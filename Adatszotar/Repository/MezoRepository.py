import sqlite3
import pymysql

from Adatszotar.Repository.RepositoryBase import RepositoryBase


class MezoRepository(RepositoryBase):
    def __init__(self):
        super().__init__("mezo")

    def create(self, mezo):
        # connection = sqlite3.connect('datagov.db', timeout=30)
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x", database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        sql_query = f"""INSERT INTO {self.table_name} 
        (nev, adattipus, leiras, business_glossary_nev, rekordleiras_id) 
        VALUES (%s, %s, %s, %s, %s)"""
        data = (mezo.nev, mezo.adattipus, mezo.leiras, mezo.business_glossary_nev, mezo.rekordleiras_id)
        message = "sikeres"

        try:
            cursor.execute(sql_query, data)
        except Exception as e:
            message = "Mező létrehozása sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message

    def update(self, mezo):
        # connection = sqlite3.connect('datagov.db', timeout=30)
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x", database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        sql_update_query = f"""UPDATE {self.table_name} 
                SET nev = %s, adattipus = %s, leiras = %s, business_glossary_nev = %s, rekordleiras_id = %s
                WHERE id = %s"""
        data = (mezo.nev, mezo.adattipus, mezo.leiras, mezo.business_glossary_nev,
                mezo.rekordleiras_id, mezo.id)
        message = "sikeres"

        try:
            cursor.execute(sql_update_query, data)
        except Exception as e:
            message = "Mező módosítása sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message

    def readAllMezoToRekordleiras(self, id):
        # connection = sqlite3.connect('datagov.db', timeout=30)
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x", database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        sql_query = f"""SELECT * FROM {self.table_name} WHERE rekordleiras_id = %s"""
        message = "sikeres"
        result = None

        try:
            cursor.execute(sql_query, (id,))
            result = cursor.fetchall()
        except Exception as e:
            message = "Mező lekérdezés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return result, message
        # return result
