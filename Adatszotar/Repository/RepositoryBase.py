import sqlite3
# import pandas as pd
# from sqlalchemy import create_engine
import pymysql


class RepositoryBase(object):
    def __init__(self, table_name):
        self.table_name = table_name
        # self.disk_engine = create_engine(f'sqlite:///datagov.db')

    def getOneById(self, id):
        # sql = pd.read_sql(f"""SELECT * FROM {self.table_name} WHERE id = {id}""", self.disk_engine)
        # return sql.iloc[0]

        # connection = sqlite3.connect('datagov.db', timeout=30)
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x", database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        sql_query = f"""SELECT * FROM {self.table_name} WHERE id = %s"""
        message = "sikeres"
        result = None

        try:
            cursor.execute(sql_query, (id,))
            result = cursor.fetchone()
        except Exception as e:
            message = "Lekérdezés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.close()

        return result, message
        # return result

    def readAll(self):
        # connection = sqlite3.connect('datagov.db', timeout=30)
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x", database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        message = "sikeres"
        result = None

        try:
            cursor.execute(f"""SELECT * FROM {self.table_name}""")
            result = cursor.fetchall()
        except Exception as e:
            message = "Lekérdezés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.close()

        return result, message
        # return result

    def delete(self, id):
        #connection = sqlite3.connect('datagov.db', timeout=30)
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x", database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        sql_query = f"""DELETE FROM {self.table_name} WHERE id = %s"""
        message = "sikeres"

        try:
            cursor.execute(sql_query, (id,))
        except Exception as e:
            message = "Törlés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message
