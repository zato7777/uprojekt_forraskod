import datetime
import sqlite3
import pymysql

from Adatszotar.Repository.RepositoryBase import RepositoryBase


class RekordleirasRepository(RepositoryBase):
    def __init__(self):
        super().__init__("rekordleiras")

    def create(self, rekordleiras):
        # connection = sqlite3.connect('datagov.db', timeout=30)
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x", database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        sql_query = f"""INSERT INTO {self.table_name} 
        (nev, tipus, cimke, leiras, utolso_modositas, letrehozva) 
        VALUES (%s, %s, %s, %s, %s, %s)"""
        data = (rekordleiras.nev, rekordleiras.tipus, rekordleiras.cimke, rekordleiras.leiras, rekordleiras.utolso_modositas, rekordleiras.letrehozva)
        message = "sikeres"

        try:
            cursor.execute(sql_query, data)
        except Exception as e:
            message = "Rekordleírás létrehozása sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message

    def update(self, rekordleiras):
        # connection = sqlite3.connect('datagov.db', timeout=30)
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x", database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        sql_update_query = f"""UPDATE {self.table_name} 
                SET nev = %s, tipus = %s, cimke = %s, leiras = %s, utolso_modositas = %s, letrehozva = %s
                WHERE id = %s"""
        data = (rekordleiras.nev, rekordleiras.tipus, rekordleiras.cimke, rekordleiras.leiras,
                rekordleiras.utolso_modositas, rekordleiras.letrehozva, rekordleiras.id)
        message = "sikeres"

        try:
            cursor.execute(sql_update_query, data)
        except Exception as e:
            message = "Rekordleírás módosítása sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message

    def getOneByName(self, nev):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x",
                                     database="main", ssl={"flag_to_enable_tls": True})
        cursor = connection.cursor()

        sql_query = f"""SELECT * FROM {self.table_name} WHERE nev = %s"""
        message = "sikeres"
        result = None

        try:
            cursor.execute(sql_query, (nev,))
            result = cursor.fetchone()
        except Exception as e:
            message = "Lekérdezés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.close()

        return result, message

    def generalas(self, rekordleiras):
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x", database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        sql_query = f"""INSERT INTO {self.table_name} 
        (nev, tipus, cimke, leiras, utolso_modositas, letrehozva) 
        VALUES (%s, %s, %s, %s, %s, %s)"""
        data = (rekordleiras.nev, rekordleiras.tipus, rekordleiras.cimke, rekordleiras.leiras, rekordleiras.utolso_modositas, rekordleiras.letrehozva)
        message = "sikeres"

        try:
            cursor.execute(sql_query, data)
        except Exception as e:
            message = "Rekordleírás létrehozása sikertelen. Próbáld újra! " + repr(e)
        finally:
            connection.commit()

        sql_query = f"""SELECT id FROM {self.table_name} WHERE nev = %s"""
        message = "sikeres"
        result = None

        try:
            cursor.execute(sql_query, (rekordleiras.nev,))
            result = cursor.fetchone()
        except Exception as e:
            message = "Lekérdezés sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.close()

        return message, result
