import sqlite3
import pymysql

from Adatszotar.Repository.RepositoryBase import RepositoryBase


class AdatallomanyRepository(RepositoryBase):
    def __init__(self):
        super().__init__("adatallomany")

    def create(self, adatallomany):
        # connection = sqlite3.connect('datagov.db', timeout=30)
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x", database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        sql_query = f"""INSERT INTO {self.table_name} 
        (rekordleiras_id, nev, forras, rekordszam, verzioszam, projekt, tulajdonos, titkositasi_kod, letrehozva) 
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"""
        data = (adatallomany.rekordleiras_id, adatallomany.nev, adatallomany.forras, adatallomany.rekordszam, adatallomany.verzioszam,
                adatallomany.projekt, adatallomany.tulajdonos, adatallomany.titkositasi_kod, adatallomany.letrehozva)
        message = "sikeres"

        try:
            cursor.execute(sql_query, data)
        except Exception as e:
            message = "Adatállomány létrehozása sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message

    def update(self, adatallomany):
        # connection = sqlite3.connect('datagov.db', timeout=30)
        connection = pymysql.connect(host="up-mysql.mysql.database.azure.com", user="rg", passwd="yDa6z7D#x", database="main", ssl={"flag_to_enable_tls":True})
        cursor = connection.cursor()

        sql_update_query = f"""UPDATE {self.table_name} 
                SET rekordleiras_id = %s, nev = %s, forras = %s, rekordszam = %s, verzioszam = %s, projekt = %s, tulajdonos = %s, titkositasi_kod = %s, letrehozva = %s
                WHERE id = %s"""
        data = (adatallomany.rekordleiras_id, adatallomany.nev, adatallomany.forras, adatallomany.rekordszam,
                adatallomany.verzioszam, adatallomany.projekt, adatallomany.tulajdonos, adatallomany.titkositasi_kod,
                adatallomany.letrehozva, adatallomany.id)
        message = "sikeres"

        try:
            cursor.execute(sql_update_query, data)
        except Exception as e:
            message = "Adatállomány módosítása sikertelen. Próbáld újra! " + repr(e)
        finally:
            cursor.close()
            connection.commit()
            connection.close()

        return message

