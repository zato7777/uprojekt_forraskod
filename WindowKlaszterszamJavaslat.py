import sys
import pandas as pd
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QDialog, QApplication, QPushButton, QVBoxLayout, QLabel
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import MinMaxScaler
import WindowKlaszteranalizis
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

class Ui_KlaszterszamJavaslat(QDialog):

    data = pd.DataFrame()

    def __init__(self, parent=None):
        super(Ui_KlaszterszamJavaslat, self).__init__(parent)
        self.window = self
        self.setWindowTitle("Klaszterszám javaslat")
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.button = QPushButton('OK')
        self.button.clicked.connect(lambda: self.close())
        self.label_1 = QLabel("Tipp: Válasszon egy olyan klaszterszámot ahol függvény alapján alacsony az eltérés-négyzetösszeg.")
        self.label_2 = QLabel("Tipp: Könyök-módszer ábra: egy optimális pont lehet egy képzeletbeli könyök (könyök-függvény vonal) hajlatában.")
        self.abratKeszit()
        layout = QVBoxLayout()
        layout.addWidget(self.canvas)
        layout.addWidget(self.label_1)
        layout.addWidget(self.label_2)
        layout.addWidget(self.button)
        self.setLayout(layout)

    def abratKeszit(self):
        try:
            df = Ui_KlaszterszamJavaslat.data.copy()
            self.figure.clear()
            ax = self.figure.add_subplot(111)
            scaler = MinMaxScaler()
            scaler.fit(df)
            df = scaler.transform(df)
            sse = []
            limit = int((df.shape[0] // 2) ** 0.5)
            k_rng = range(1, limit+3)
            for k in k_rng:
                km = KMeans(n_clusters=k)
                km.fit(df)
                sse.append(km.inertia_)
            plt.xlabel('Klaszterszám')
            plt.ylabel('Eltérés-négyzetösszeg')
            plt.title("Könyök-módszer ábra: optimális klaszterszám keresése")
            plt.grid(True)
            ax.plot(k_rng, sse, marker = 'o')
            self.canvas.draw()
        except Exception as e:
            QtWidgets.QMessageBox.warning(self.window, "Klaszterszám javaslat","Hiba a klaszterszám keresése közben! Lehet, hogy egy változó nem számértékű!")
            print(e)


