import os
import dashboard
from app_modules import *
from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dashboard(object):
    def setupUi(self, Ui_Dashboard):
        Ui_Dashboard.setObjectName("Ui_Dashboard")
        Ui_Dashboard.resize(600, 270)

        font = QtGui.QFont()
        font.setPointSize(11)

        Ui_Dashboard.setWindowTitle("Dashboard")
        self.centralwidget = QtWidgets.QWidget(Ui_Dashboard)
        self.centralwidget.setObjectName("centralwidget")

        self.groupBox = QtWidgets.QGroupBox("A dashboard paraméterei", self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(30, 10, 540, 240))
        self.groupBox.setFont(font)

        # sourcedata
        self.label_sourcedata = QtWidgets.QLabel("Bemeneti adatállomány:", self.groupBox)
        self.label_sourcedata.setGeometry(QtCore.QRect(10, 40, 200, 21))
        self.label_sourcedata.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.comboBox_sourcedata = QtWidgets.QComboBox(self.groupBox)
        self.comboBox_sourcedata.setGeometry(QtCore.QRect(250, 40, 250, 25))
        sourcedata_defaultstring = "Válassz egy adatállományt"
        self.comboBox_sourcedata.addItem(sourcedata_defaultstring)
        all_folder_file = os.listdir('./SourceData')
        for element in all_folder_file:
            self.comboBox_sourcedata.addItem(element)

        # company name
        self.label_companyname = QtWidgets.QLabel("A dashboard címe:", self.groupBox)
        self.label_companyname.setGeometry(QtCore.QRect(10, 75, 200, 21))
        self.label_companyname.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.lineeditbox_companyname = QLineEdit(self.groupBox)
        self.lineeditbox_companyname.move(250, 75)
        self.lineeditbox_companyname.resize(250, 22)

        # style
        # self.label_style = QtWidgets.QLabel("A dashboard stílusa:", self.groupBox)
        # self.label_style.setGeometry(QtCore.QRect(10, 110, 200, 18))
        # self.label_style.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        # self.comboBox_style = QtWidgets.QComboBox(self.groupBox)
        # self.comboBox_style.setGeometry(QtCore.QRect(250, 110, 250, 23))
        # self.comboBox_style.setObjectName("comboBox_style")
        # self.comboBox_style.addItems(["Egyszerű", "Sötét", "Színes"])

        # run and close buttons
        self.pushButton_megse = QtWidgets.QPushButton("Mégse", self.centralwidget)
        self.pushButton_megse.setGeometry(QtCore.QRect(460, 200, 91, 31))

        self.pushButton_megse.setFont(font)
        self.pushButton_megse.setDefault(True)
        self.pushButton_megse.clicked.connect(lambda: Ui_Dashboard.close())

        self.pushButton_futtat = QtWidgets.QPushButton("Futtatás", self.centralwidget)
        self.pushButton_futtat.setGeometry(QtCore.QRect(360, 200, 91, 31))
        self.pushButton_futtat.setFont(font)
        self.pushButton_futtat.setDefault(True)
        self.pushButton_futtat.clicked.connect(lambda: dashboard.run(self.comboBox_sourcedata.currentText(),
                                                                        self.lineeditbox_companyname.text(),
                                                                        #self.comboBox_style.currentText()
                                                                        style="Egyszerű"
                                                                     ))
        self.pushButton_futtat.setObjectName("pushButton_futtat")

        Ui_Dashboard.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(Ui_Dashboard)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 591, 21))
        self.menubar.setObjectName("menubar")
        Ui_Dashboard.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(Ui_Dashboard)
        self.statusbar.setObjectName("statusbar")
        Ui_Dashboard.setStatusBar(self.statusbar)

        QtCore.QMetaObject.connectSlotsByName(Ui_Dashboard)