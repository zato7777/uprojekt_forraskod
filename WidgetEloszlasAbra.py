import pandas as pd
from PyQt5.QtWidgets import *
from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.figure import Figure
from matplotlib.ticker import MaxNLocator

class WidgetAbra(QWidget):

    data = pd.DataFrame()
    
    def __init__(self, parent = None):
        QWidget.__init__(self, parent)
        self.canvas = FigureCanvas(Figure())
        self.vertical_layout = QVBoxLayout()
        self.vertical_layout.addWidget(self.canvas)
        self.data = WidgetAbra.data
        self.canvas.axes = self.canvas.figure.add_subplot(111)
        self.counts = self.data['klaszter'].value_counts()
        self.index = []
        for i in self.counts.index:
            self.index.append(i)
        self.values = []
        for v in self.counts.values:
            self.values.append(v)
        self.bar = self.canvas.axes.bar(self.counts.index, self.counts.values, label=self.index)
        self.canvas.axes.xaxis.set_major_locator(MaxNLocator(integer=True))
        self.canvas.axes.bar_label(self.bar)
        self.canvas.axes.set_xlabel("Klaszter")
        self.canvas.axes.set_ylabel("Egyedszám")
        self.canvas.axes.set_title("A klaszterek egyedszámai")
        self.canvas.draw()
        self.setLayout(self.vertical_layout)
