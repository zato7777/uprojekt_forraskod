from app_modules import *
from PyQt5.QtGui import QIntValidator
from tabulate import tabulate
import WindowEredmenyek
import WindowKlaszterszamJavaslat
import WidgetEloszlasAbra
from dataimport import TableModel
from WindowStatisztika import *
from WindowAdatok import *
import sklearn.cluster as cluster
from WindowKlaszterszamJavaslat import *
from WindowEredmenyek import *
import pandas as pd
from IPython.display import display
from PyQt5 import QtCore, QtGui, QtWidgets
from prettytable import *


class Ui_WindowKlaszteranalizis(object):

    df_global = pd.DataFrame()
    klaszterszam = 2

    def openWindowAdatok(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_WindowAdatok()
        self.ui.setupUi(self.window)
        self.ui.lineEdit_EleresiUt.setText(self.lineEdit_Adatallomany.text())
        self.ui.model = TableModel(Ui_WindowKlaszteranalizis.df_global)
        self.ui.tableView_Adatok.setModel(self.ui.model)
        self.window.show()

    def openWindowStatisztika(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_WindowStatisztika()
        self.ui.setupUi(self.window)
        self.desc = Ui_WindowKlaszteranalizis.df_global.describe().applymap(lambda x: f"{x:0.3f}")
        self.desc = self.desc.rename(index={'count': 'egyedszám','mean': 'átlag','std': 'szórás','min': 'minimum','25%': 'Q1','50%': 'medián','75%': 'Q2','max': 'maximum'})
        self.ui.model = TableModel(self.desc)
        self.ui.tableView_Statisztika.setModel(self.ui.model)
        self.window.show()

    def openWindowKlaszterszamJavaslat(self):
        df = self.dataframeKeszito()
        WindowKlaszterszamJavaslat.Ui_KlaszterszamJavaslat.data = df
        self.window = Ui_KlaszterszamJavaslat()
        self.window.show()

    def openWindowEredmenyek(self):
        try:
            df = self.dataframeKeszito()
            dfcol = df.copy()
            dfg = Ui_WindowKlaszteranalizis.df_global.copy()

            # Standardizálás (MinMaxScaler-el)
            scaler = MinMaxScaler()
            scaler.fit(df)
            df = scaler.transform(df)
            # K-közép klaszterezés
            km = cluster.KMeans(n_clusters=self.klaszterszam)
            km.fit(df)
            identified_clusters = km.fit_predict(df)
            klaszter = identified_clusters
            dfg.insert(0, "klaszter", klaszter)
            # Adatok továbbadása az eloszlás ábrához
            WidgetEloszlasAbra.WidgetAbra.data = dfg
            WidgetEloszlasAbra.WidgetAbra.cluster_centers = km.cluster_centers_
            self.window = QtWidgets.QMainWindow()
            self.ui = Ui_WindowEredmenyek()
            self.ui.setupUi(self.window)
            WindowEredmenyek.Ui_WindowEredmenyek.df_export = dfg
            df = pd.DataFrame(df)
            dfstat = dfg.copy() # klaszterenkénti leíró statisztika halmaza
            for i in range(len(dfcol.columns)):
                szoveg = dfcol.columns[i] + "_z"
                dfg[szoveg] = df[i]
            # Dialógus ablak tartalma
            # keletkezett klaszterszámok és rendezésük
            klaszterek = []
            for i in dfg.klaszter.unique():
                klaszterek.append(i)
            klaszterek.sort()
            # alapinformációk
            self.ui.plainTextEditDialogus.appendPlainText("*******************************************")
            self.ui.plainTextEditDialogus.appendPlainText("* K-KÖZÉP MÓDSZER ELEMZÉS ALAPINFORMÁCIÓI *")
            self.ui.plainTextEditDialogus.appendPlainText("*******************************************")
            self.ui.plainTextEditDialogus.appendPlainText("")
            self.ui.plainTextEditDialogus.appendPlainText(" Egyedek száma összesen: " + str(df.shape[0]))
            szoveg = ""
            for i in dfcol.columns:
                szoveg+=i + "  "
            self.ui.plainTextEditDialogus.appendPlainText(" Klaszterváltozók alapján: " + str(szoveg))
            self.ui.plainTextEditDialogus.appendPlainText(" Klaszterek száma: " + str(len(dfg.klaszter.unique())))
            for i in klaszterek:
                self.ui.plainTextEditDialogus.appendPlainText(" " + str(i) + ". klaszter egyedszáma: " + str(dfg.loc[dfg.klaszter == i].shape[0]))
            self.ui.plainTextEditDialogus.appendPlainText(" Klaszterezés minősége: " + str(np.round(km.inertia_, 3)))
            self.ui.plainTextEditDialogus.appendPlainText("")
            self.ui.plainTextEditDialogus.appendPlainText("")
            self.ui.plainTextEditDialogus.appendPlainText("")
            # klaszterközéppontok
            klasztervaltozok_z = [i + "_z" for i in dfcol.columns]
            klasztervaltozok = [i for i in dfcol.columns]
            dfcc_z = pd.DataFrame()
            for i in range(len(km.cluster_centers_)):
                dfcc_z[i] = km.cluster_centers_[i]
            dfcc_z.index = klasztervaltozok_z
            dfcc_z = dfcc_z.applymap(lambda x: f"{x:0.3f}")
            dfcc_z = tabulate(dfcc_z, headers='keys', tablefmt='psql', stralign='center')
            dfcc = pd.DataFrame()
            for i in range(len(km.cluster_centers_)):
                dfcc[i] = scaler.inverse_transform(km.cluster_centers_)[i]
            dfcc.index = klasztervaltozok
            dfcc = dfcc.applymap(lambda x: f"{x:0.3f}")
            dfcc = tabulate(dfcc, headers='keys', tablefmt='psql', stralign='center')
            self.ui.plainTextEditDialogus.appendPlainText("*******************************************")
            self.ui.plainTextEditDialogus.appendPlainText("*     KLASZTER KÖZÉPPONTOK (CENTROIDOK)   *")
            self.ui.plainTextEditDialogus.appendPlainText("*******************************************")
            self.ui.plainTextEditDialogus.appendPlainText("")
            self.ui.plainTextEditDialogus.appendPlainText(" Klaszter középpontok (centroidok) standardizálva: ")
            self.ui.plainTextEditDialogus.appendPlainText("")
            self.ui.plainTextEditDialogus.appendPlainText(dfcc_z)
            self.ui.plainTextEditDialogus.appendPlainText("")
            self.ui.plainTextEditDialogus.appendPlainText(" Klaszter középpontok (centroidok): ")
            self.ui.plainTextEditDialogus.appendPlainText("")
            self.ui.plainTextEditDialogus.appendPlainText(dfcc)
            self.ui.plainTextEditDialogus.appendPlainText("")
            self.ui.plainTextEditDialogus.appendPlainText("")
            self.ui.plainTextEditDialogus.appendPlainText("")
            # elemzések klaszterenként
            self.ui.plainTextEditDialogus.appendPlainText("*******************************************")
            self.ui.plainTextEditDialogus.appendPlainText("*         ELEMZÉSEK KLASZTERENKÉNT        *")
            self.ui.plainTextEditDialogus.appendPlainText("*******************************************")
            self.ui.plainTextEditDialogus.appendPlainText("")
            klaszterek.sort()
            for i in klaszterek:
                if self.checkBoxKlaszterTagok.isChecked():
                    self.ui.plainTextEditDialogus.appendPlainText("  A(z) " + str(i) + ". klaszter tagjai és leíró statisztikája: ")
                else:
                    self.ui.plainTextEditDialogus.appendPlainText("  A(z) " + str(i) + ". klaszter leíró statisztikája: ")
                self.ui.plainTextEditDialogus.appendPlainText("")
                desc = dfstat.loc[dfstat.klaszter == i].describe().applymap(lambda x: f"{x:0.3f}")
                desc = desc.rename(index={'count': 'egyedszám','mean': 'átlag','std': 'szórás','min': 'minimum','25%': 'Q1','50%': 'medián','75%': 'Q2','max': 'maximum'})
                desc['klaszter'] = i
                if self.checkBoxKlaszterTagok.isChecked(): dfgs = tabulate(dfg.loc[dfg.klaszter == i], headers='keys', tablefmt='psql', stralign='center')
                descs = tabulate(desc, headers='keys', tablefmt='psql', stralign='center')
                if self.checkBoxKlaszterTagok.isChecked(): self.ui.plainTextEditDialogus.appendPlainText(dfgs)
                self.ui.plainTextEditDialogus.appendPlainText("")
                self.ui.plainTextEditDialogus.appendPlainText(descs)
                self.ui.plainTextEditDialogus.appendPlainText("")
                self.ui.plainTextEditDialogus.appendPlainText("")
                self.ui.plainTextEditDialogus.appendPlainText("")
            self.ui.plainTextEditDialogus.appendPlainText("*******************************************")
            self.ui.plainTextEditDialogus.appendPlainText("*    GÖRGESSEN FELJEBB AZ EREDMÉNYEKHEZ   *")
            self.ui.plainTextEditDialogus.appendPlainText("*******************************************")
            # tábla feltöltése
            self.ui.model = TableModel(dfg)
            self.ui.tableViewKlaszteradatok.setModel(self.ui.model)
            self.window.show()
        except Exception as e:
            QtWidgets.QMessageBox.warning(self.window, "Eredmények", "Hiba az eredmény betöltése közben! Ellenőrizze a paraméterek helyességét!")
            print(e)

    def klaszterszamotElment(self, text):
        try:
            self.lineEdit_Klaszterszam.setText(text)
            Ui_WindowKlaszteranalizis.klaszterszam = int(text)
        except Exception as e:
            Ui_WindowKlaszteranalizis.klaszterszam = 2
            print(e)

    def adatallomanytEllenoriz(self):
        try:
            self.adatallomany_eleresi_ut = QtWidgets.QFileDialog.getOpenFileUrl(caption="Adatállomány importálása", filter="CSV fájl (*.csv)", initialFilter="CSV fájl (*.csv)")
            self.lineEdit_Adatallomany.setText(self.adatallomany_eleresi_ut[0].fileName())
            self.adatallomanytBetolt()
        except Exception as e:
            QtWidgets.QMessageBox.warning(self.window, "Adatállomány importálása", "Hiba az adatállomány importálása közben! Próbálja újra!")
            self.valtozokatTorol()
            self.letiltGombokat()
            print(e)

    def adatallomanytBetolt(self):
        try:
            Ui_WindowKlaszteranalizis.df_global = pd.read_csv(self.adatallomany_eleresi_ut[0].toString())
            self.valtozokatTorol()
            self.listWidgetAdatValtozok.addItems([col for col in Ui_WindowKlaszteranalizis.df_global.describe()])
            self.engedelyezGombokat()
        except Exception as e:
            QtWidgets.QMessageBox.warning(self.window, "Adatállomány betöltése", "Hiba az adatállomány betöltése közben! Próbálja újra!")
            self.valtozokatTorol()
            self.letiltGombokat()
            print(e)

    def hozzaadValtozot(self):
        row = self.listWidgetAdatValtozok.currentRow()
        rowItem = self.listWidgetAdatValtozok.takeItem(row)
        self.listWidgetKlasztervaltozok.addItem(rowItem)

    def elveszValtozot(self):
        row = self.listWidgetKlasztervaltozok.currentRow()
        rowItem = self.listWidgetKlasztervaltozok.takeItem(row)
        self.listWidgetAdatValtozok.addItem(rowItem)

    def letiltGombokat(self):
        self.pushButton_AdatokMutatasa.setDisabled(True)
        self.pushButton_LeiroStatisztika.setDisabled(True)
        self.pushButton_Kereses.setDisabled(True)
        self.pushButton_OK.setDisabled(True)
        self.lineEdit_Klaszterszam.setDisabled(True)
        self.pushButtonHozzaad.setDisabled(True)
        self.pushButtonElvesz.setDisabled(True)

    def engedelyezGombokat(self):
        self.pushButton_AdatokMutatasa.setDisabled(False)
        self.pushButton_LeiroStatisztika.setDisabled(False)
        self.pushButton_Kereses.setDisabled(False)
        self.pushButton_OK.setDisabled(False)
        self.lineEdit_Klaszterszam.setDisabled(False)
        self.pushButtonHozzaad.setDisabled(False)
        self.pushButtonElvesz.setDisabled(False)

    def valtozokatTorol(self):
        self.listWidgetAdatValtozok.clear()
        self.listWidgetKlasztervaltozok.clear()

    def dataframeKeszito(self):
        df = Ui_WindowKlaszteranalizis.df_global.copy()
        items = []
        for index in range(self.listWidgetKlasztervaltozok.count()):
            items.append(self.listWidgetKlasztervaltozok.item(index).text())
        df = df[items]
        return df

    def setupUi(self, WindowKlaszteranalizis):
        WindowKlaszteranalizis.setObjectName("WindowKlaszteranalizis")
        WindowKlaszteranalizis.resize(663, 445)
        self.window = WindowKlaszteranalizis
        self.centralwidget = QtWidgets.QWidget(self.window)
        self.centralwidget.setObjectName("centralwidget")
        self.lineEdit_Adatallomany = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_Adatallomany.setGeometry(QtCore.QRect(170, 40, 161, 21))
        self.lineEdit_Adatallomany.setCursor(QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.lineEdit_Adatallomany.setReadOnly(True)
        self.lineEdit_Adatallomany.setObjectName("lineEdit_Adatallomany")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(80, 40, 71, 20))
        self.label.setObjectName("label")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(80, 320, 171, 20))
        self.label_4.setObjectName("label_4")
        self.pushButton_Tallozas = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Tallozas.setGeometry(QtCore.QRect(350, 40, 111, 21))
        self.adatallomany_eleresi_ut = ""
        self.pushButton_Tallozas.setObjectName("pushButton_Tallozas")
        self.pushButton_Tallozas.clicked.connect(lambda: self.adatallomanytEllenoriz())
        self.lineEdit_Klaszterszam = QtWidgets.QLineEdit(self.centralwidget)
        self.onlyInt = QIntValidator()
        self.onlyInt.setRange(2, 99)
        self.lineEdit_Klaszterszam.setValidator(self.onlyInt)
        self.lineEdit_Klaszterszam.setGeometry(QtCore.QRect(210, 320, 31, 21))
        self.lineEdit_Klaszterszam.setObjectName("lineEdit_Klaszterszam")
        self.klaszterszamotElment(self.lineEdit_Klaszterszam.text())
        self.lineEdit_Klaszterszam.textChanged[str].connect(lambda: self.klaszterszamotElment(self.lineEdit_Klaszterszam.text()))
        self.pushButton_Kereses = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Kereses.setGeometry(QtCore.QRect(250, 320, 171, 21))
        self.pushButton_Kereses.setObjectName("pushButton_Kereses")
        self.pushButton_Kereses.clicked.connect(lambda: self.openWindowKlaszterszamJavaslat())
        self.pushButton_OK = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_OK.setGeometry(QtCore.QRect(80, 370, 301, 23))
        self.pushButton_OK.setObjectName("pushButton_OK")
        self.pushButton_OK.clicked.connect(lambda: self.openWindowEredmenyek())
        self.pushButton_Megse = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Megse.setGeometry(QtCore.QRect(390, 370, 101, 23))
        self.pushButton_Megse.setObjectName("pushButton_Megse")
        self.pushButton_Megse.clicked.connect(lambda: WindowKlaszteranalizis.close())
        self.pushButton_Sugo = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Sugo.setGeometry(QtCore.QRect(500, 370, 101, 23))
        self.pushButton_Sugo.setObjectName("pushButton_Sugo")
        self.pushButton_LeiroStatisztika = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_LeiroStatisztika.setGeometry(QtCore.QRect(350, 70, 241, 23))
        self.pushButton_LeiroStatisztika.setObjectName("pushButton_LeiroStatisztika")
        self.pushButton_LeiroStatisztika.clicked.connect(lambda: self.openWindowStatisztika())
        self.pushButton_AdatokMutatasa = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_AdatokMutatasa.setGeometry(QtCore.QRect(480, 40, 111, 21))
        self.pushButton_AdatokMutatasa.setObjectName("pushButton_AdatokMutatasa")
        self.pushButton_AdatokMutatasa.clicked.connect(lambda: self.openWindowAdatok())
        self.listWidgetKlasztervaltozok = QtWidgets.QListWidget(self.centralwidget)
        self.listWidgetKlasztervaltozok.setGeometry(QtCore.QRect(390, 140, 211, 131))
        self.listWidgetKlasztervaltozok.setObjectName("listWidgetKlasztervaltozok")
        self.listWidgetAdatValtozok = QtWidgets.QListWidget(self.centralwidget)
        self.listWidgetAdatValtozok.setGeometry(QtCore.QRect(80, 140, 211, 131))
        self.listWidgetAdatValtozok.setObjectName("listWidgetAdatValtozok")
        self.pushButtonHozzaad = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonHozzaad.setGeometry(QtCore.QRect(300, 170, 75, 23))
        self.pushButtonHozzaad.setObjectName("pushButtonHozzaad")
        self.pushButtonHozzaad.clicked.connect(lambda: self.hozzaadValtozot())
        self.pushButtonElvesz = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonElvesz.setGeometry(QtCore.QRect(300, 200, 75, 23))
        self.pushButtonElvesz.setObjectName("pushButtonElvesz")
        self.pushButtonElvesz.clicked.connect(lambda: self.elveszValtozot())
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(130, 120, 131, 16))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(460, 120, 91, 16))
        self.label_3.setObjectName("label_3")
        self.checkBoxKlaszterTagok = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxKlaszterTagok.setGeometry(QtCore.QRect(450, 320, 181, 17))
        self.checkBoxKlaszterTagok.setChecked(False)
        self.checkBoxKlaszterTagok.setObjectName("checkBoxKlaszterTagok")
        self.window.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(self.window)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 663, 21))
        self.menubar.setObjectName("menubar")
        self.menuK_k_z_p_m_dszer = QtWidgets.QMenu(self.menubar)
        self.menuK_k_z_p_m_dszer.setObjectName("menuK_k_z_p_m_dszer")
        self.window.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(self.window)
        self.statusbar.setObjectName("statusbar")
        self.window.setStatusBar(self.statusbar)
        self.menubar.addAction(self.menuK_k_z_p_m_dszer.menuAction())
        self.retranslateUi(self.window)
        QtCore.QMetaObject.connectSlotsByName(self.window)
        self.letiltGombokat()

    def retranslateUi(self, WindowKlaszteranalizis):
        _translate = QtCore.QCoreApplication.translate
        WindowKlaszteranalizis.setWindowTitle(_translate("WindowKlaszteranalizis", "Klaszteranalízis"))
        self.label.setText(_translate("WindowKlaszteranalizis", "Adatállomány:"))
        self.label_4.setText(_translate("WindowKlaszteranalizis", "Klaszterszám megadása:"))
        self.pushButton_Tallozas.setText(_translate("WindowKlaszteranalizis", "Tallózás"))
        self.lineEdit_Klaszterszam.setText(_translate("WindowKlaszteranalizis", "2"))
        self.lineEdit_Klaszterszam.setPlaceholderText(_translate("WindowKlaszteranalizis", "2"))
        self.pushButton_Kereses.setText(_translate("WindowKlaszteranalizis", "Klaszterszám javaslat"))
        self.pushButton_OK.setText(_translate("WindowKlaszteranalizis", "Futtatás"))
        self.pushButton_Megse.setText(_translate("WindowKlaszteranalizis", "Mégse"))
        self.pushButton_Sugo.setText(_translate("WindowKlaszteranalizis", "Súgó"))
        self.pushButton_LeiroStatisztika.setText(_translate("WindowKlaszteranalizis", "Leíró statisztika"))
        self.pushButton_AdatokMutatasa.setText(_translate("WindowKlaszteranalizis", "Adatok mutatása"))
        self.pushButtonHozzaad.setText(_translate("WindowKlaszteranalizis", "Hozzáad"))
        self.pushButtonElvesz.setText(_translate("WindowKlaszteranalizis", "Elvesz"))
        self.label_2.setText(_translate("WindowKlaszteranalizis", "Adatállomány változói"))
        self.label_3.setText(_translate("WindowKlaszteranalizis", "Klaszterváltozók"))
        self.checkBoxKlaszterTagok.setText(_translate("WindowKlaszteranalizis", "Klaszterek tagjainak felsorolása"))
        self.menuK_k_z_p_m_dszer.setTitle(_translate("WindowKlaszteranalizis", "K-közép módszer"))

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    WindowKlaszteranalizis = QtWidgets.QMainWindow()
    ui = Ui_WindowKlaszteranalizis()
    ui.setupUi(WindowKlaszteranalizis)
    WindowKlaszteranalizis.show()
    sys.exit(app.exec_())
