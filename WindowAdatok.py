from PyQt5 import QtWidgets, QtCore

class Ui_WindowAdatok(object):
    def setupUi(self, WindowAdatok):
        WindowAdatok.setObjectName("WindowAdatok")
        WindowAdatok.resize(1190, 697)
        self.window = WindowAdatok
        self.pushButton_OK = QtWidgets.QPushButton(self.window)
        self.pushButton_OK.setGeometry(QtCore.QRect(10, 650, 1161, 41))
        self.pushButton_OK.setAutoFillBackground(False)
        self.pushButton_OK.setCheckable(False)
        self.pushButton_OK.setObjectName("pushButton_OK")
        self.pushButton_OK.clicked.connect(lambda: self.window.close())
        self.tableView_Adatok = QtWidgets.QTableView(self.window)
        self.tableView_Adatok.setGeometry(QtCore.QRect(10, 50, 1161, 591))
        self.tableView_Adatok.setAlternatingRowColors(True)
        self.tableView_Adatok.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.tableView_Adatok.setObjectName("tableView_Adatok")
        header = self.tableView_Adatok.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.lineEdit_EleresiUt = QtWidgets.QLineEdit(self.window)
        self.lineEdit_EleresiUt.setGeometry(QtCore.QRect(10, 19, 1161, 21))
        self.lineEdit_EleresiUt.setReadOnly(True)
        self.lineEdit_EleresiUt.setObjectName("lineEdit_EleresiUt")
        self.retranslateUi(self.window)
        QtCore.QMetaObject.connectSlotsByName(self.window)

    def retranslateUi(self, WindowAdatok):
        _translate = QtCore.QCoreApplication.translate
        WindowAdatok.setWindowTitle(_translate("WindowAdatok", "Adatok mutatása"))
        self.pushButton_OK.setText(_translate("WindowAdatok", "Bezárás"))

