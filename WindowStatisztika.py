from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_WindowStatisztika(object):

    def setupUi(self, WindowStatisztika):
        WindowStatisztika.setObjectName("WindowStatisztika")
        WindowStatisztika.resize(482, 372)
        self.window = WindowStatisztika
        self.centralwidget = QtWidgets.QWidget(self.window)
        self.centralwidget.setObjectName("centralwidget")
        self.tableView_Statisztika = QtWidgets.QTableView(self.centralwidget)
        self.tableView_Statisztika.setGeometry(QtCore.QRect(10, 10, 461, 301))
        self.tableView_Statisztika.setObjectName("tableView_Statisztika")
        self.header = self.tableView_Statisztika.horizontalHeader()
        self.header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.pushButton_OK = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_OK.setGeometry(QtCore.QRect(10, 320, 461, 31))
        self.pushButton_OK.setObjectName("pushButton_OK")
        self.pushButton_OK.clicked.connect(lambda: self.window.close())
        self.window.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(self.window)
        self.statusbar.setObjectName("statusbar")
        self.window.setStatusBar(self.statusbar)
        self.retranslateUi(self.window)
        QtCore.QMetaObject.connectSlotsByName(self.window)

    def retranslateUi(self, WindowStatisztika):
        _translate = QtCore.QCoreApplication.translate
        WindowStatisztika.setWindowTitle(_translate("WindowStatisztika", "Leíró statisztika"))
        self.pushButton_OK.setText(_translate("WindowStatisztika", "Bezárás"))

